package com.highlysucceed.kaluapp.vendor.android.java.firebase;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.util.Map;


/**
 * Created by BCTI 3 on 10/6/2017.
 */

public class FCMData implements Parcelable {

    public String title;
    public String body;
    public String type;
    public String image;
    public String event = "";
    public int referenceID;
    public int unit_id;
    public String displayType = "";

    public FCMData(Map<String, String> data) {
        title = data.get("title");
        body = data.get("body");
        type = data.get("type");
        image = data.get("thumbnail");
        event = data.get("event");
        displayType = data.get("display_type");

        String ref = data.get("reference_id");
        String[] ids = ref.split("\\|");
        Log.e("FCMData Map", "ids>>>" + ids);
        if(ids.length == 1){
            referenceID = getIntFromObject(ids[0]);
            unit_id = 0;
        }else{
            referenceID = getIntFromObject(ids[0]);
            unit_id = getIntFromObject(ids[1]);
        }

        Log.e("FCMData Map", "referenceID>>>" + referenceID);
        Log.e("FCMData Map", "unit_id>>>" + unit_id);
    }

    public FCMData(Bundle data) {
        title = data.getString("title");
        body = data.getString("body");
        type = data.getString("type");
        image = data.getString("thumbnail");
        event = data.getString("event");
        displayType = data.getString("display_type");

        String ref = data.getString("reference_id");
        String[] ids = ref.split("\\|");
        Log.e("FCMData Map", "ids>>>" + ids);
        if(ids.length == 1){
            referenceID = getIntFromObject(ids[0]);
            unit_id = 0;
        }else{
            referenceID = getIntFromObject(ids[0]);
            unit_id = getIntFromObject(ids[1]);
        }

        Log.e("FCMData Map", "referenceID>>>" + referenceID);
        Log.e("FCMData Map", "unit_id>>>" + unit_id);
    }

    private int getIntFromObject(Object object){
        if (object == null) {
            return 0;
        }
        String value = (String) object;
        if(value.equals("") || value.equals("null")){
            return 0;
        }
        return Integer.parseInt(value);
    }

    private int getIntFromObject(String object){
        if (object == null) {
            return 0;
        }
        if(object.equals("") || object.equals("null")){
            return 0;
        }
        return Integer.parseInt(object);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.body);
        dest.writeString(this.type);
        dest.writeString(this.image);
        dest.writeString(this.event);
        dest.writeInt(this.referenceID);
        dest.writeInt(this.unit_id);
        dest.writeString(this.displayType);
    }

    protected FCMData(Parcel in) {
        this.title = in.readString();
        this.body = in.readString();
        this.type = in.readString();
        this.image = in.readString();
        this.event = in.readString();
        this.referenceID = in.readInt();
        this.unit_id = in.readInt();
        this.displayType = in.readString();
    }

    public static final Creator<FCMData> CREATOR = new Creator<FCMData>() {
        @Override
        public FCMData createFromParcel(Parcel source) {
            return new FCMData(source);
        }

        @Override
        public FCMData[] newArray(int size) {
            return new FCMData[size];
        }
    };
}
