package com.highlysucceed.kaluapp.vendor.server.transformer;

import com.google.gson.annotations.SerializedName;
import com.highlysucceed.kaluapp.data.model.api.ErrorModel;

/**
 * Created by Labyalo on 8/3/2017.
 */

public class BaseTransformer {

    @SerializedName("msg")
    public String msg = "Internal Server Error";

    @SerializedName("status")
    public Boolean status = false;

    @SerializedName("status_code")
    public String status_code = "RETROFIT_FAILED";

    @SerializedName("token")
    public String token = "";

    @SerializedName("new_token")
    public String new_token = "";

    @SerializedName("has_morepages")
    public Boolean has_morepages = false;

    @SerializedName("hasRequirements")
    public boolean hasRequirements(){
        return false;
    }

    @SerializedName("errors")
    public ErrorModel error;

    public boolean checkEmpty(Object obj){
        return obj !=  null;
    }
}
