/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.highlysucceed.kaluapp.vendor.android.java.firebase;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.activity.LandingActivity;
import com.highlysucceed.kaluapp.android.activity.MainActivity;
import com.highlysucceed.kaluapp.data.preference.Data;
import com.highlysucceed.kaluapp.data.preference.UserData;
import com.highlysucceed.kaluapp.vendor.android.base.RouteManager;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;



public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "FMService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
//        Log.e(TAG, "FCMDatagetData" + remoteMessage.getData());
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "FCMDatagetData" + remoteMessage.getData());

            FCMData fcmData = new FCMData(remoteMessage.getData());
            if (remoteMessage.getNotification() != null) {
                fcmData.title = remoteMessage.getNotification().getTitle();
                fcmData.body = remoteMessage.getNotification().getBody();
                fcmData.image = remoteMessage.getNotification().getIcon();
                Log.e(TAG, "FCMDatagetNotification" + remoteMessage.getNotification().getBody());
            }

            Log.e(TAG, "FCMData Map" + fcmData.title);

            Data.setSharedPreferences(getBaseContext());
            showNotification(getDefaultIntent(fcmData), fcmData);

            if (UserData.isLogin()){
                Bundle bundle= new Bundle();
                Intent intent;
                switch (fcmData.type.toLowerCase()){
                    case "article":
                        bundle.putInt("article_id", fcmData.referenceID);
                        intent = RouteManager.Route.with(this)
                                .addActivityClass(MainActivity.class)
                                .addActivityTag("article")
                                .addFragmentTag("my_details")
                                .addFragmentBundle(bundle)
                                .getIntent();

                        showNotification(intent, fcmData);
                        break;

                    default:
                        showNotification(getDefaultIntent(fcmData), fcmData);
                }
            }else{
                showNotification(getDefaultIntent(fcmData), fcmData);
            }
        }
    }

    private Intent getDefaultIntent(FCMData fcmData) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("fcm_data", fcmData);
        return RouteManager.Route.with(getBaseContext())
                .addActivityClass(LandingActivity.class)
                .addActivityTag("splash")
                .addFragmentTag("splash")
                .addFragmentBundle(bundle)
                .getIntent(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
    }

    private void showNotification(Intent intent, FCMData fcmData){
        if(fcmData.displayType != null){
            switch (fcmData.displayType){
                case "big_image":
                    bigNotificationBuilder(intent, fcmData);
                    break;
                default:
                    defaultNotificationBuilder(intent, fcmData);
            }
        }else{
            defaultNotificationBuilder(intent, fcmData);
        }
    }

    private void defaultNotificationBuilder(Intent intent, FCMData fcmData) {
        Log.e("Message", ">>>" + fcmData.body);

        PendingIntent pendingIntent = PendingIntent.getActivity
                (this, fcmData.referenceID /* Request code */, intent,
                        PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getBaseContext())
                .setSmallIcon(R.drawable.logo)
                .setColor(ActivityCompat.getColor(getBaseContext(), R.color.colorPrimary))
                .setContentTitle(fcmData.title == null ? getString(R.string.app_name) : fcmData.title)
                .setContentText(fcmData.body)
                .setAutoCancel(true)
//                .setNumber(fcmData.counter_badge)
                .setDefaults(Notification.DEFAULT_ALL)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .setBigContentTitle(fcmData.title == null ? getString(R.string.app_name) : fcmData.title)
                        .bigText(fcmData.body))
                .setContentIntent(pendingIntent);

        Sound.playNotification(this);

        NotificationManager notificationManager =
                (NotificationManager) getBaseContext().getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(fcmData.referenceID /* ID of notification */, notificationBuilder.build());
    }

    private void bigNotificationBuilder(Intent intent, FCMData fcmData) {
        new generatePictureStyleNotification(intent, fcmData).execute();
    }

    public class generatePictureStyleNotification extends AsyncTask<String, Void, Bitmap> {

        private Intent intent;
        private FCMData fcmData;

        public generatePictureStyleNotification(Intent intent, FCMData fcmData) {
            super();
            this.intent = intent;
            this.fcmData = fcmData;
        }

        @Override
        protected Bitmap doInBackground(String... params) {

            InputStream in;
            try {
                URL url = new URL(this.fcmData.image);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                in = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(in);
                return myBitmap;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            PendingIntent pendingIntent = PendingIntent.getActivity
                    (getBaseContext(), fcmData.referenceID /* Request code */, intent,
                            PendingIntent.FLAG_CANCEL_CURRENT);

            Sound.playNotification(MyFirebaseMessagingService.this);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getBaseContext())
                    .setSmallIcon(R.drawable.logo)
                    .setColor(ActivityCompat.getColor(getBaseContext(), R.color.colorPrimary))
                    .setContentTitle(fcmData.title == null ? getString(R.string.app_name) : fcmData.title)
                    .setContentText(fcmData.body)
                    .setAutoCancel(true)
                    .setStyle(new NotificationCompat.BigPictureStyle()
                            .bigPicture(result)
                            .setSummaryText(fcmData.body)
                            .setBigContentTitle(fcmData.title == null ? getString(R.string.app_name) : fcmData.title))
                    .setSound(Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                            + "://" + MyFirebaseMessagingService.this.getPackageName() + "/raw/cat1"))
//                    .setNumber(fcmData.counter_badge)
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager =
                    (NotificationManager) getBaseContext().getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(fcmData.referenceID /* ID of notification */, notificationBuilder.build());
        }
    }
}
