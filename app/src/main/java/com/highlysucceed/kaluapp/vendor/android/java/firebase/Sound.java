package com.highlysucceed.kaluapp.vendor.android.java.firebase;

import android.content.Context;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;


/**
 * Created by BCTI 3 on 7/24/2017.
 */

public class Sound {
    private static Ringtone r;

    public static void playNotification(Context context){
        stopSound();
            Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            playSound(defaultSoundUri,context);
    }

    public static void playSound(Uri uri,Context context){
        r = RingtoneManager.getRingtone(context, uri);
        r.play();
    }

    public static void stopSound() {
        if (r != null) {
            if (r.isPlaying()) {
                r.stop();
            }
        }
    }
}
