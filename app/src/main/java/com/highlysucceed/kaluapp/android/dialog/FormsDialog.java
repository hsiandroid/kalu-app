package com.highlysucceed.kaluapp.android.dialog;

import android.widget.Button;
import android.widget.ImageView;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.activity.PersonalInfoActivity;
import com.highlysucceed.kaluapp.vendor.android.base.BaseDialog;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class FormsDialog extends BaseDialog {
	public static final String TAG = FormsDialog.class.getName().toString();
	PersonalInfoActivity activity;


	@BindView(R.id.continueBTN) 		Button continueBTN;
	@BindView(R.id.closeBTN) 			ImageView closeBTN;

	public static FormsDialog newInstance() {
		FormsDialog dialog = new FormsDialog();
		return dialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_forms;
	}

	@Override
	public void onViewReady() {
		activity = (PersonalInfoActivity) getContext();
		continueBTN.setText("Submit");
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}

	@OnClick(R.id.closeBTN)
	void closeBTN() {
		dismiss();
	}

	@OnClick(R.id.continueBTN)
	void continueBTN() {
		dismiss();
	}
}
