package com.highlysucceed.kaluapp.android.dialog;

import android.app.Dialog;
import android.content.Context;
import android.widget.EditText;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.vendor.android.base.BaseDialog;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class NotesDialog extends BaseDialog {
	public static final String TAG = NotesDialog.class.getName().toString();
	private Context context;
	private Callback callback;

	@BindView(R.id.edtAREA)   EditText edtAREA;

	public static NotesDialog newInstance(Context context, Callback callback) {
		NotesDialog dialog = new NotesDialog();
		dialog.context = context;
		dialog.callback = callback;
		return dialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_notes;
	}

	@Override
	public void onViewReady() {

	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}
	@OnClick(R.id.btnADD)
	void success(){
		if (!edtAREA.getText().toString().isEmpty()){
			callback.notesResult(true);
			dismiss();
		}
	}

	public interface Callback{
		void notesResult(boolean hasValue);
	}

}
