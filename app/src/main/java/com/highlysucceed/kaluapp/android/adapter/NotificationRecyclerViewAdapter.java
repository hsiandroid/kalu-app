package com.highlysucceed.kaluapp.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.data.model.api.NotifModel;
import com.highlysucceed.kaluapp.vendor.android.base.BaseRecylerViewAdapter;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class NotificationRecyclerViewAdapter extends BaseRecylerViewAdapter<NotificationRecyclerViewAdapter.ViewHolder, NotifModel> {

    private ClickListener clickListener;

    public NotificationRecyclerViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_notif));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.titleTXT.setText(holder.getItem().title);
        holder.descriptionTXT.setText(holder.getItem().description);
    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.titleTXT)     TextView titleTXT;
        @BindView(R.id.descriptionTXT)   TextView descriptionTXT;
        @BindView(R.id.adapterCON)  View adapterCON;

        public ViewHolder(View view) {
            super(view);
        }

        public NotifModel getItem() {
            return (NotifModel) super.getItem();
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener{
        void onItemClick(NotifModel testKitModel);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.adapterCON:
                if (clickListener != null){
                    clickListener.onItemClick((NotifModel) v.getTag());
                }
                break;
        }
    }


} 
