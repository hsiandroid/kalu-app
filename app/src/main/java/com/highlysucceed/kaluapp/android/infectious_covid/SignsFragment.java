package com.highlysucceed.kaluapp.android.infectious_covid;

import android.util.Log;
import android.widget.TextView;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.activity.PersonalInfoActivity;
import com.highlysucceed.kaluapp.data.model.api.InfoModel;
import com.highlysucceed.kaluapp.data.preference.UserData;
import com.highlysucceed.kaluapp.server.request.Auth;
import com.highlysucceed.kaluapp.server.request.Infectious;
import com.highlysucceed.kaluapp.vendor.android.base.BaseFragment;
import com.highlysucceed.kaluapp.vendor.server.request.APIRequest;
import com.highlysucceed.kaluapp.vendor.server.transformer.BaseTransformer;
import com.highlysucceed.kaluapp.vendor.server.transformer.CollectionTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class SignsFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {
    public static final String TAG = SignsFragment.class.getName().toString();

    @BindView(R.id.coughTXT)            TextView coughTXT;
    @BindView(R.id.feverTXT)           TextView feverTXT;
    @BindView(R.id.paintTXT)           TextView paintTXT;
    @BindView(R.id.weaknessTXT)        TextView weaknessTXT;
    @BindView(R.id.smellTXT)           TextView smellTXT;
    @BindView(R.id.tasteTXT)           TextView tasteTXT;
    @BindView(R.id.diarrheaTXT)        TextView diarrheaTXT;
    @BindView(R.id.breathingTXT)       TextView breathingTXT;
    @BindView(R.id.temperatureTXT)     TextView temperatureTXT;
    @BindView(R.id.note_observationsTXT)   TextView note_observationsTXT;
    @BindView(R.id.homeSRL)              SwipeRefreshLayout homeSRL;

    private APIRequest apiRequest;
    private PersonalInfoActivity activity;

    public static SignsFragment newInstance() {
        SignsFragment fragment = new SignsFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_signs;
    }

    @Override
    public void onViewReady() {
        activity = (PersonalInfoActivity) getContext();
        activity.setTitle("Signs & Symptoms");
        homeSRL.setOnRefreshListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        onRefresh();
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }


    @Subscribe
    public void onResponse(Infectious.LoginResponse response){
        CollectionTransformer<InfoModel> transformer = response.getData(CollectionTransformer.class);
        if(transformer.status) {

            if (!transformer.data.get(0).SIGNSANDSYMPTOMS.dryCough.equals("")) {
                coughTXT.setText(transformer.data.get(0).SIGNSANDSYMPTOMS.dryCough);
            }else {
                coughTXT.setText("--");
            }

            if (!transformer.data.get(0).SIGNSANDSYMPTOMS.fever.equals("")) {
                feverTXT.setText(transformer.data.get(0).SIGNSANDSYMPTOMS.fever);
            }else {
                feverTXT.setText("--");
            }

            if (!transformer.data.get(0).SIGNSANDSYMPTOMS.musclePain.equals("")) {
                paintTXT.setText(transformer.data.get(0).SIGNSANDSYMPTOMS.musclePain);
            }else {
                paintTXT.setText("--");
            }

            if (!transformer.data.get(0).SIGNSANDSYMPTOMS.weakness.equals("")) {
                weaknessTXT.setText(transformer.data.get(0).SIGNSANDSYMPTOMS.weakness);
            }else {
                weaknessTXT.setText("--");
            }

            if (!transformer.data.get(0).SIGNSANDSYMPTOMS.decreasedSenseOfSmell.equals("")) {
                smellTXT.setText(transformer.data.get(0).SIGNSANDSYMPTOMS.decreasedSenseOfSmell);
            }else {
                smellTXT.setText("--");
            }

            if (!transformer.data.get(0).SIGNSANDSYMPTOMS.decreasedSenseOfTaste.equals("")) {
                tasteTXT.setText(transformer.data.get(0).SIGNSANDSYMPTOMS.decreasedSenseOfTaste);
            }else {
                tasteTXT.setText("--");
            }

            if (!transformer.data.get(0).SIGNSANDSYMPTOMS.diarrhea.equals("")) {
                diarrheaTXT.setText(transformer.data.get(0).SIGNSANDSYMPTOMS.diarrhea);
            }else {
                diarrheaTXT.setText("--");
            }

            if (!transformer.data.get(0).SIGNSANDSYMPTOMS.difficultyOfBreathing.equals("")) {
                breathingTXT.setText(transformer.data.get(0).SIGNSANDSYMPTOMS.difficultyOfBreathing);
            }else {
                breathingTXT.setText("--");
            }

            if (!transformer.data.get(0).SIGNSANDSYMPTOMS.temperature.equals("")) {
                temperatureTXT.setText(transformer.data.get(0).SIGNSANDSYMPTOMS.temperature);
            }else {
                temperatureTXT.setText("--");
            }

            if (!transformer.data.get(0).SIGNSANDSYMPTOMS.noteObservations.equals("")) {
                note_observationsTXT.setText(transformer.data.get(0).SIGNSANDSYMPTOMS.noteObservations);
            }else {
                note_observationsTXT.setText("--");
            }


        }
    }

    @Override
    public void onRefresh() {
        refreshList();
    }
    public void refreshList(){
        apiRequest = Infectious.getDefault().getProfileDetails(activity, String.valueOf(UserData.getUserModel().userId), homeSRL);
        apiRequest.first();
    }
}
