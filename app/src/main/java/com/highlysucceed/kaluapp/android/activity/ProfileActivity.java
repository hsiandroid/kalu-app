package com.highlysucceed.kaluapp.android.activity;


import android.view.View;
import android.widget.ImageView;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.fragment.profile.DigitalIDFragment;
import com.highlysucceed.kaluapp.android.fragment.profile.MyQrCodeFragment;
import com.highlysucceed.kaluapp.android.fragment.profile.ProfileDetailsFragment;
import com.highlysucceed.kaluapp.android.fragment.profile.SettingsFragment;
import com.highlysucceed.kaluapp.android.fragment.profile.UpdateProfileFragment;
import com.highlysucceed.kaluapp.android.route.RouteActivity;

import butterknife.BindView;
import butterknife.OnClick;

public class ProfileActivity extends RouteActivity {
    public static final String TAG = ProfileActivity.class.getName().toString();

    public  ProfileActivity activity;
    @BindView(R.id.logo)               ImageView logo;
    @BindView(R.id.settingsBTN)          ImageView settingsBTN;


    @Override
    public int onLayoutSet() {
        return R.layout.activity_profile;
    }

    @Override
    public void onViewReady() {
     activity = (ProfileActivity) getContext();
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "update_profile":
                openUpdateProfileFragment();
                break;
            case "profiledetails":
                settingsBTN.setVisibility(View.VISIBLE);
                openProfileDetailsFragment();
                break;
            case "settings":
                settingsBTN.setVisibility(View.GONE);
                openSettingsFragment();
                break;
            case "digital":
                settingsBTN.setVisibility(View.GONE);
                openDigitalIdFragment();
                break;
            case "qrcode":
                settingsBTN.setVisibility(View.GONE);
                openQrCodeFragment();
                break;

        }
    }

    public void openUpdateProfileFragment(){ switchFragment(UpdateProfileFragment.newInstance()); }
    public void openSettingsFragment(){ switchFragment(SettingsFragment.newInstance()); }
    public void openProfileDetailsFragment(){ switchFragment(ProfileDetailsFragment.newInstance()); }
    public void openDigitalIdFragment(){ switchFragment(DigitalIDFragment.newInstance()); }
    public void openQrCodeFragment(){ switchFragment(MyQrCodeFragment.newInstance()); }

    @OnClick(R.id.settingsBTN)
    void settingsBTN(){
        activity.startProfileActivity("settings");
    }

}
