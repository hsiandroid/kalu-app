package com.highlysucceed.kaluapp.android.dialog;

import android.app.Dialog;
import android.content.Context;
import android.widget.EditText;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.vendor.android.base.BaseDialog;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ChangePasswordDialog extends BaseDialog {
	public static final String TAG = ChangePasswordDialog.class.getName().toString();
	private Context context;
	private Callback callback;

	@BindView(R.id.oldPassET)    EditText oldPassET;
	@BindView(R.id.newPassET)    EditText newPassET;
	@BindView(R.id.cPassET)      EditText cPassET;

	public static ChangePasswordDialog newInstance(Context context, Callback callback) {
		ChangePasswordDialog dialog = new ChangePasswordDialog();
		dialog.context = context;
		dialog.callback =callback;
		return dialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_change_pass;
	}

	@Override
	public void onViewReady() {

	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}

	@OnClick(R.id.exitIMG)
	void exitIMG(){
		this.dismiss();
	}

	@OnClick(R.id.updateBTN)
	void updateBTN(){
		if(oldPassET.getText().toString().isEmpty()){
			oldPassET.setError("Fields Required");
		}
		else if(newPassET.getText().toString().isEmpty()){
			newPassET.setError("Fields Required");
		}
		else if(cPassET.getText().toString().isEmpty()){
			newPassET.setError("Fields Required");
		}
		else if(!cPassET.getText().toString().equals(newPassET.getText().toString())){
			cPassET.setError("Password does not Match");
			newPassET.setError("Password does not Match");
		}
		else {
			callback.output(this, oldPassET.getText().toString().trim(), newPassET.getText().toString().trim(), cPassET.getText().toString().trim());
		}
	}

	public  interface Callback{
		void output(ChangePasswordDialog dialog, String old_password, String current_password, String confirm_password);
	}
}
