package com.highlysucceed.kaluapp.android.activity;

import android.content.res.ColorStateList;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.fragment.DefaultFragment;
import com.highlysucceed.kaluapp.android.fragment.main.HomeFragment;
import com.highlysucceed.kaluapp.android.fragment.main.NotificationFragment;
import com.highlysucceed.kaluapp.android.route.RouteActivity;
import com.highlysucceed.kaluapp.data.model.api.UserModel;
import com.highlysucceed.kaluapp.data.preference.UserData;
import com.highlysucceed.kaluapp.server.request.Auth;
import com.highlysucceed.kaluapp.vendor.android.java.Log;
import com.highlysucceed.kaluapp.vendor.android.java.ToastMessage;
import com.highlysucceed.kaluapp.vendor.server.transformer.BaseTransformer;
import com.highlysucceed.kaluapp.vendor.server.transformer.CollectionTransformer;
import com.highlysucceed.kaluapp.vendor.server.transformer.SingleTransformer;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class MainActivity extends RouteActivity  {
    public static final String TAG = MainActivity.class.getName().toString();
    private MainActivity activity;
    ColorStateList defaultColor;

    @BindView(R.id.homeCon)             View homeCON;
    @BindView(R.id.homeIMG)             ImageView homeIMG;
    @BindView(R.id.homeTXT)             TextView homeTXT;
    @BindView(R.id.activityCon)         View activityCon;
    @BindView(R.id.activityIMG)         ImageView activityIMG;
    @BindView(R.id.activityTXT)         TextView activityTXT;
    @BindView(R.id.mrCon)               View mrCon;
    @BindView(R.id.mrIMG)               ImageView mrIMG;
    @BindView(R.id.mrTXT)               TextView mrTXT;
    @BindView(R.id.vaccinationCon)      View vaccinationCon;
    @BindView(R.id.vaccinationIMG)      ImageView vaccinationIMG;
    @BindView(R.id.vaccinationTXT)      TextView vaccinationTXT;
    @BindView(R.id.programCon)          View programCon;
    @BindView(R.id.programIMG)          ImageView programIMG;
    @BindView(R.id.programTXT)          TextView programTXT;


    @Override
    public int onLayoutSet() {
        return R.layout.activity_main;
    }

    @Override
    public void onViewReady() {
        defaultColor = homeTXT.getTextColors();
        activity = (MainActivity) getContext();



    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName) {
            case "home":
                openHomeFragment();
                break;
            case "notif":
                openNotificationFragment();
                break;
        }
    }

    public void openDefaultFragment() {
        switchFragment(DefaultFragment.newInstance());
    }

    public void openHomeFragment() {
        switchFragment(HomeFragment.newInstance());
    }

    public void openNotificationFragment() {
        switchFragment(NotificationFragment.newInstance());
    }



    @Override
    protected void onResume() {
        super.onResume();
    }


    @OnClick(R.id.homeCon)
    public void homeActives() {
        homeActive();
        openHomeFragment();
    }

    @OnClick(R.id.activityCon)
    public void activityActives() {
        activityActive();
        openDefaultFragment();
    }

    @OnClick(R.id.mrCon)
    public void mrActives() {
        mrActive();
        openDefaultFragment();
    }

    @OnClick(R.id.vaccinationCon)
    public void vaccinationActives() {
        vaccinationActive();
        openDefaultFragment();
    }

    @OnClick(R.id.programCon)
    public void programActives() {
        programActive();
        openDefaultFragment();
    }



    public void homeActive() {
        homeCON.setSelected(true);
//        homeIMG.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
        homeTXT.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
        homeIMG.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.img_home_selected));

        activityCon.setSelected(false);
        activityIMG.clearColorFilter();
        activityTXT.setTextColor(defaultColor);
        activityIMG.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_activity));

        mrCon.setSelected(false);
        mrIMG.clearColorFilter();
        mrTXT.setTextColor(defaultColor);
        mrIMG.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_mr));


        vaccinationCon.setSelected(false);
        vaccinationIMG.clearColorFilter();
        vaccinationTXT.setTextColor(defaultColor);
        vaccinationIMG.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_vaccination));

        programCon.setSelected(false);
        programIMG.clearColorFilter();
        programTXT.setTextColor(defaultColor);
        programIMG.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.img_program));
    }

    public void activityActive() {
        activityCon.setSelected(true);
        activityTXT.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
        activityIMG.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.img_activity_selected));

        homeCON.setSelected(false);
        homeIMG.clearColorFilter();
        homeTXT.setTextColor(defaultColor);
        homeIMG.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.img_home));


        mrCon.setSelected(false);
        mrIMG.clearColorFilter();
        mrTXT.setTextColor(defaultColor);
        mrIMG.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_mr));

        vaccinationCon.setSelected(false);
        vaccinationIMG.clearColorFilter();
        vaccinationTXT.setTextColor(defaultColor);
        vaccinationIMG.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_vaccination));

        programCon.setSelected(false);
        programIMG.clearColorFilter();
        programTXT.setTextColor(defaultColor);
        programIMG.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.img_program));
    }

    public void mrActive() {
        mrCon.setSelected(true);
        mrTXT.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
        mrIMG.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.img_mr_selected));


        homeCON.setSelected(false);
        homeIMG.clearColorFilter();
        homeTXT.setTextColor(defaultColor);
        homeIMG.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.img_home));

        activityCon.setSelected(false);
        activityIMG.clearColorFilter();
        activityTXT.setTextColor(defaultColor);
        activityIMG.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_activity));

        vaccinationCon.setSelected(false);
        vaccinationIMG.clearColorFilter();
        vaccinationTXT.setTextColor(defaultColor);
        vaccinationIMG.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_vaccination));

        programCon.setSelected(false);
        programIMG.clearColorFilter();
        programTXT.setTextColor(defaultColor);
        programIMG.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.img_program));
    }


    public void vaccinationActive() {
        vaccinationCon.setSelected(true);
        vaccinationTXT.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
        vaccinationIMG.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.img_vaccination_selected));

        homeCON.setSelected(false);
        homeIMG.clearColorFilter();
        homeTXT.setTextColor(defaultColor);
        homeIMG.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.img_home));

        activityCon.setSelected(false);
        activityIMG.clearColorFilter();
        activityTXT.setTextColor(defaultColor);
        activityIMG.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_activity));

        mrCon.setSelected(false);
        mrIMG.clearColorFilter();
        mrTXT.setTextColor(defaultColor);
        mrIMG.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_mr));

        programCon.setSelected(false);
        programIMG.clearColorFilter();
        programTXT.setTextColor(defaultColor);
        programIMG.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.img_program));
    }

    public void programActive() {
        programCon.setSelected(true);
        programTXT.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
        programIMG.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.img_program_selected));

        homeCON.setSelected(false);
        homeIMG.clearColorFilter();
        homeTXT.setTextColor(defaultColor);
        homeIMG.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.img_home));

        activityCon.setSelected(false);
        activityIMG.clearColorFilter();
        activityTXT.setTextColor(defaultColor);
        activityIMG.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_activity));


        mrCon.setSelected(false);
        mrIMG.clearColorFilter();
        mrTXT.setTextColor(defaultColor);
        mrIMG.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_mr));

        vaccinationCon.setSelected(false);
        vaccinationIMG.clearColorFilter();
        vaccinationTXT.setTextColor(defaultColor);
        vaccinationIMG.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_vaccination));
    }


}
