package com.highlysucceed.kaluapp.android.infectious_covid;

import android.util.Log;
import android.widget.TextView;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.activity.PersonalInfoActivity;
import com.highlysucceed.kaluapp.data.model.api.InfoModel;
import com.highlysucceed.kaluapp.data.preference.UserData;
import com.highlysucceed.kaluapp.server.request.Auth;
import com.highlysucceed.kaluapp.server.request.Infectious;
import com.highlysucceed.kaluapp.vendor.android.base.BaseFragment;
import com.highlysucceed.kaluapp.vendor.server.request.APIRequest;
import com.highlysucceed.kaluapp.vendor.server.transformer.BaseTransformer;
import com.highlysucceed.kaluapp.vendor.server.transformer.CollectionTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class AntigenResultFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {
    public static final String TAG = AntigenResultFragment.class.getName().toString();

    @BindView(R.id.testDoneTXT)        TextView testDoneTXT;
    @BindView(R.id.negativeTXT)        TextView negativeTXT;
    @BindView(R.id.positiveTXT)        TextView positiveTXT;
    @BindView(R.id.pcrTXT)             TextView pcrTXT;
    @BindView(R.id.homeSRL)            SwipeRefreshLayout homeSRL;
    private PersonalInfoActivity activity;
    private APIRequest apiRequest;

    public static AntigenResultFragment newInstance() {
        AntigenResultFragment fragment = new AntigenResultFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_antigen_result;
    }

    @Override
    public void onViewReady() {
        activity = (PersonalInfoActivity) getContext();
        activity.setTitle("Antigen Results");
        homeSRL.setOnRefreshListener(this);
    }
    @Override
    public void onResume() {
        super.onResume();
        onRefresh();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Infectious.LoginResponse response){
        CollectionTransformer<InfoModel> transformer = response.getData(CollectionTransformer.class);
        if(transformer.status){
            if (!transformer.data.get(0).ANTIGENTESTRESULT.dateOfTestDone.equals("")) {
                testDoneTXT.setText(transformer.data.get(0).ANTIGENTESTRESULT.dateOfTestDone);
            }else {
                testDoneTXT.setText("--");
            }

            if (!transformer.data.get(0).ANTIGENTESTRESULT.negative.equals("")) {
                negativeTXT.setText(transformer.data.get(0).ANTIGENTESTRESULT.negative);
            }else {
                negativeTXT.setText("--");
            }

            if (!transformer.data.get(0).ANTIGENTESTRESULT.positive.equals("")) {
                positiveTXT.setText(transformer.data.get(0).ANTIGENTESTRESULT.positive);
            }else {
                positiveTXT.setText("--");
            }

            if (!transformer.data.get(0).ANTIGENTESTRESULT.pcrDone.equals("")) {
                pcrTXT.setText(transformer.data.get(0).ANTIGENTESTRESULT.pcrDone);
            }else {
                pcrTXT.setText("--");
            }

        }
    }

    @Override
    public void onRefresh() {
          refreshList();
    }
    public void refreshList(){
        apiRequest = Infectious.getDefault().getProfileDetails(activity, String.valueOf(UserData.getUserModel().userId), homeSRL);
        apiRequest.first();
    }
}
