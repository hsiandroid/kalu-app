package com.highlysucceed.kaluapp.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.data.model.api.HolderModel;
import com.highlysucceed.kaluapp.data.model.api.HospitalModel;
import com.highlysucceed.kaluapp.vendor.android.base.BaseRecylerViewAdapter;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class NearbyListRecyclerViewAdapter extends BaseRecylerViewAdapter<NearbyListRecyclerViewAdapter.ViewHolder, HolderModel>{

    private ClickListener clickListener;

    public NearbyListRecyclerViewAdapter(Context context, ClickListener clickListener) {
        super(context);
        this.clickListener = clickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_hospital_list));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.hospitalTXT.setText(holder.getItem().name);
        holder.addressTXT.setText(holder.getItem().address);

        if(!holder.getItem().full_path.equals("")) {
            Glide.with(getContext())
                    .load(holder.getItem().full_path)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.ic_no_image)
                            .error(R.drawable.ic_no_image))
                    .into(holder.hospitalIV);

        }
        else{
            Glide.with(getContext())
                    .load(R.drawable.ic_no_image)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.ic_no_image)
                            .error(R.drawable.ic_no_image))
                    .into(holder.hospitalIV);
        }
    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder implements View.OnClickListener {

        @BindView(R.id.hospitalIV)      ImageView  hospitalIV;
        @BindView(R.id.hospitalTXT)     TextView hospitalTXT;
        @BindView(R.id.addressTXT)      TextView addressTXT;
        @BindView(R.id.callBTN)         TextView callBTN;
        @BindView(R.id.bookBTN)         TextView bookBTN;

        public ViewHolder(View view) {
            super(view);
            callBTN.setOnClickListener(this);
            bookBTN.setOnClickListener(this);
        }

        public HolderModel getItem() {
            return (HolderModel) super.getItem();
        }

        @Override
        public void onClick(View v) {
             switch (v.getId()){

                 case R.id.callBTN:
                     clickListener.setContactNumber(getItem().contact_number);
                     break;
                 case R.id.bookBTN:
                     clickListener.setDetails(String.valueOf(getItem().id), getItem().type);
             }
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener{
        void setDetails(String  hospitalId, String type);
        void setContactNumber(String number);

    }

} 
