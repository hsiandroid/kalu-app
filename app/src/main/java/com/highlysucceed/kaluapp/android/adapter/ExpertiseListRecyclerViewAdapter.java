package com.highlysucceed.kaluapp.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.data.model.api.OccupationModel;
import com.highlysucceed.kaluapp.data.model.api.SampleModel;
import com.highlysucceed.kaluapp.vendor.android.base.BaseRecylerViewAdapter;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ExpertiseListRecyclerViewAdapter extends BaseRecylerViewAdapter<ExpertiseListRecyclerViewAdapter.ViewHolder, OccupationModel>{

    private ClickListener clickListener;

    public ExpertiseListRecyclerViewAdapter(Context context, ClickListener clickListener1) {
        super(context);
        this.clickListener = clickListener1;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_expertise_list));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.expertiseTXT.setText(holder.getItem().name);
    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.expertiseTXT)     TextView expertiseTXT;
        @BindView(R.id.viewBTN)          View viewBTN;

        public ViewHolder(View view) {
            super(view);
            viewBTN.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickListener.onClickExpertise(String.valueOf(getItem().id));
                }
            });
        }

        public OccupationModel getItem() {
            return (OccupationModel) super.getItem();
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener{
        void onClickExpertise(String id);
    }
} 
