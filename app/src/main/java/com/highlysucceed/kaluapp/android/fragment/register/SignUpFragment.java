package com.highlysucceed.kaluapp.android.fragment.register;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.activity.RegisterActivity;
import com.highlysucceed.kaluapp.android.dialog.BloodTypeDialog;
import com.highlysucceed.kaluapp.android.dialog.CalendarDialog;
import com.highlysucceed.kaluapp.config.Keys;
import com.highlysucceed.kaluapp.data.model.api.AccountModel;
import com.highlysucceed.kaluapp.data.model.api.SignUpModel;
import com.highlysucceed.kaluapp.data.model.api.UserModel;
import com.highlysucceed.kaluapp.server.request.Auth;
import com.highlysucceed.kaluapp.vendor.android.base.BaseFragment;
import com.highlysucceed.kaluapp.vendor.server.request.APIRequest;
import com.highlysucceed.kaluapp.vendor.server.transformer.BaseTransformer;
import com.highlysucceed.kaluapp.vendor.server.util.ErrorResponseManger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.security.Key;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class SignUpFragment extends BaseFragment implements CalendarDialog.DateTimePickerListener, BloodTypeDialog.Callback, AdapterView.OnItemSelectedListener {
    public static final String TAG = SignUpFragment.class.getName().toString();

    private RegisterActivity registerActivity;
    private AccountModel accountModel;


    public static SignUpFragment newInstance() {
        SignUpFragment fragment = new SignUpFragment();
        return fragment;
    }

    @BindView(R.id.signUpBTN)                       TextView signUpBTN;
    @BindView(R.id.firstNameET)                     EditText firstNameET;
    @BindView(R.id.lastNameET)                      EditText lastNameET;
    @BindView(R.id.addressET)                       EditText addressET;
    @BindView(R.id.phoneET)                         EditText phoneET;
    @BindView(R.id.birthdayBTN)                     View birthdayBTN;
    @BindView(R.id.birthdayTXT)                     TextView birthdayTXT;
    @BindView(R.id.bloodTypeET)                     TextView bloodTypeET;
    @BindView(R.id.heightET)                        TextView heightET;
    @BindView(R.id.weightET)                        TextView weightET;
    @BindView(R.id.spinnerGender)                   Spinner spinnerGender;

    String selectedGender;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_signup;
    }

    @Override
    public void onViewReady() {
        registerActivity = (RegisterActivity) getContext();
        registerActivity.showActivityNo1(false);
        registerActivity.showActivityNo2(true);
        accountModel = new AccountModel();

        spinnerGender.setOnItemSelectedListener(this);
    }

    @OnClick(R.id.signUpBTN)
    void onClick(){
        APIRequest apiRequest  = Auth.getDefault().preRegister(getContext())
                .addParameter(Keys.PROGRESS, 2)
                .addParameter(Keys.FIRSTNAME, firstNameET.getText().toString().trim())
                .addParameter(Keys.LASTNAME, lastNameET.getText().toString().trim())
                .addParameter(Keys.GENDER, selectedGender)
                .addParameter(Keys.ADDRESS, addressET.getText().toString().trim())
                .addParameter(Keys.CONTACT_NUMBER, phoneET.getText().toString().trim())
                .addParameter(Keys.BIRTHDATE, birthdayTXT.getText().toString().trim())
                .addParameter(Keys.BLOOD_TYPE, bloodTypeET.getText().toString().trim())
                .addParameter(Keys.HEIGHT, heightET.getText().toString().trim())
                .addParameter(Keys.WEIGHT, weightET.getText().toString().trim());
        apiRequest.execute();
    }

    @OnClick(R.id.birthdayBTN)
    void onBirthdayClick(){
        CalendarDialog.newInstance(this, birthdayTXT.getText().toString()).show(getChildFragmentManager(), TAG);
    }
    @OnClick(R.id.bloodTypeET)
    void onBloodTypeClick(){
        BloodTypeDialog.newInstance(registerActivity, this).show(getChildFragmentManager(), TAG);
    }

    @Override
    public void onResume() {
        super.onResume();
        registerActivity.setTitle("Personal Details");
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.SingleResponse response){
        BaseTransformer baseTransformer = response.getData(BaseTransformer.class);
        if(baseTransformer.status){
            SignUpModel signUpModel = new SignUpModel();
            signUpModel.email = registerActivity.getAccountModel().email;
            signUpModel.password = registerActivity.getAccountModel().password;
            signUpModel.confirm_password = registerActivity.getAccountModel().confirm_password;
            signUpModel.firstname = firstNameET.getText().toString().trim();
            signUpModel.lastname = lastNameET.getText().toString().trim();
            signUpModel.gender = selectedGender;
            signUpModel.address = addressET.getText().toString().trim();
            signUpModel.contactNumber = phoneET.getText().toString().trim();
            signUpModel.bloodType = bloodTypeET.getText().toString().trim();
            signUpModel.birthdate = birthdayTXT.getText().toString().trim();
            signUpModel.height = heightET.getText().toString().trim();
            signUpModel.weight = weightET.getText().toString().trim();
            registerActivity.setSignUpModel(signUpModel);
            registerActivity.openHealthInformationFragment();
        }
        else {
            if (!ErrorResponseManger.first(baseTransformer.error.firstname).equals("")){
                firstNameET.setError(ErrorResponseManger.first(baseTransformer.error.firstname));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.lastname).equals("")){
                lastNameET.setError(ErrorResponseManger.first(baseTransformer.error.lastname));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.address).equals("")){
                addressET.setError(ErrorResponseManger.first(baseTransformer.error.address));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.contactNumber).equals("")){
                phoneET.setError(ErrorResponseManger.first(baseTransformer.error.contactNumber));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.birthdate).equals("")){
                birthdayTXT.setError(ErrorResponseManger.first(baseTransformer.error.birthdate));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.bloodType).equals("")){
                bloodTypeET.setError(ErrorResponseManger.first(baseTransformer.error.bloodType));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.height).equals("")){
                heightET.setError(ErrorResponseManger.first(baseTransformer.error.height));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.weight).equals("")){
                weightET.setError(ErrorResponseManger.first(baseTransformer.error.weight));
            }
        }
    }




    @Override
    public void forDisplay(String date) {
        birthdayTXT.setText(date);
    }

    @Override
    public void getBloodType(String type) {
        bloodTypeET.setText(type);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        if (parent.getItemAtPosition(position).toString().equals("MALE")) {
            selectedGender = "Male";
        }else {
            selectedGender = "Female";
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
