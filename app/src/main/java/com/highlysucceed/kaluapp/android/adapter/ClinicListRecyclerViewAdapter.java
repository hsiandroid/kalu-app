package com.highlysucceed.kaluapp.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.data.model.api.ClinicListModel;
import com.highlysucceed.kaluapp.data.model.api.ClinicModel;
import com.highlysucceed.kaluapp.vendor.android.base.BaseRecylerViewAdapter;

import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ClinicListRecyclerViewAdapter extends BaseRecylerViewAdapter<ClinicListRecyclerViewAdapter.ViewHolder, ClinicModel>{

    private ClinicListener clickListener;
    private List<ClinicListModel> clinicListModels;

    public ClinicListRecyclerViewAdapter(Context context, ClinicListener clickListener) {
        super(context);
        this.clickListener = clickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_list_item));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));


        holder.clinicNameTXT.setText(holder.getItem().name);
        holder.clinicAddressTXT.setText(holder.getItem().address);

        if(!holder.getItem().thumbnail.filename.equals("")) {
            Glide.with(getContext())
                    .load(holder.getItem().thumbnail.fullPath)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.ic_no_image)
                            .error(R.drawable.ic_no_image))
                    .into(holder.clinicIMG);

        }
        else{
            Glide.with(getContext())
                    .load(R.drawable.ic_no_image)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.ic_no_image)
                            .error(R.drawable.ic_no_image))
                    .into(holder.clinicIMG);
        }
    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder implements View.OnClickListener {

        @BindView(R.id.clinicIMG)  CircleImageView clinicIMG;
        @BindView(R.id.clinicNameTXT)  TextView clinicNameTXT;
        @BindView(R.id.clinicAddressTXT)  TextView clinicAddressTXT;
        @BindView(R.id.listITEM) View listITEM;

        public ViewHolder(View view) {
            super(view);
            listITEM.setOnClickListener(this);
        }

        public ClinicModel getItem() {
            return (ClinicModel) super.getItem();
        }

        @Override
        public void onClick(View view) { clickListener.onSuccess(String.valueOf(getItem().id)); }
    }

    public void setClickListener(ClinicListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClinicListener{
        void onSuccess(String id);
    }

} 
