package com.highlysucceed.kaluapp.android.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.data.model.api.FeturedPhotoModel;
import com.highlysucceed.kaluapp.data.model.api.SlotModel;
import com.highlysucceed.kaluapp.vendor.android.java.Log;

import java.util.ArrayList;
import java.util.List;

public class OfficeHoursAdapter extends BaseAdapter {
    TextView txtHOURS;
    LayoutInflater inflter;
    private Context context;
    private  List<SlotModel> slotModelList = new ArrayList<>();

    public OfficeHoursAdapter(Context mCtx,   List<SlotModel> slotModels){
        this.context = mCtx;
        this.slotModelList = slotModels;
        inflter = (LayoutInflater.from(context));
    }


    @Override
    public int getCount() {
        return  slotModelList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }


    @Override
    public long getItemId(int i) {
        return 0;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        final SlotModel model = slotModelList.get(position);
        if(view == null){
            txtHOURS = new TextView(context);
            txtHOURS.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL);
            txtHOURS.setTextColor(ContextCompat.getColor(context, R.color.dark_gray));
            txtHOURS.setBackgroundResource(R.drawable.bg_gray_rounded_corners);
            txtHOURS.setTextSize(13);
            txtHOURS.setPadding(20, 20, 20, 20);

        }
        else {
            txtHOURS = (TextView) view;
        }
        txtHOURS.setText(model.time);

        return txtHOURS;
    }

}
