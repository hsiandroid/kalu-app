package com.highlysucceed.kaluapp.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.data.model.api.CenterModel;
import com.highlysucceed.kaluapp.data.model.api.SampleModel;
import com.highlysucceed.kaluapp.vendor.android.base.BaseRecylerViewAdapter;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class RecentAppointRecyclerViewAdapter extends BaseRecylerViewAdapter<RecentAppointRecyclerViewAdapter.ViewHolder, CenterModel>{

    private ClickListener clickListener;

    public RecentAppointRecyclerViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.layout_recent_appointments));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.centerIMG.setImageResource(holder.getItem().getImage());
        holder.txtCenterName.setText(holder.getItem().getName());
        holder.txtCenterAddress.setText(holder.getItem().getAddress());
    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.centerIMG)
        ImageView centerIMG;
        @BindView(R.id.txtCenterName)
        TextView txtCenterName;
        @BindView(R.id.txtCenterAddress)
        TextView txtCenterAddress;

        public ViewHolder(View view) {
            super(view);
        }

        public CenterModel getItem() {
            return (CenterModel) super.getItem();
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener{
        void onItemClick(SampleModel sampleModel);
    }
} 
