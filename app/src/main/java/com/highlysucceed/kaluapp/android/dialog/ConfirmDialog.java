package com.highlysucceed.kaluapp.android.dialog;

import android.content.Context;
import android.widget.TextView;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.vendor.android.base.BaseDialog;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ConfirmDialog extends BaseDialog {
	public static final String TAG = ConfirmDialog.class.getName().toString();
	private Context context;
	private Callback callback;
	String cDate, cService, cTime;

	@BindView(R.id.dateTXT)  TextView dateTXT;
	@BindView(R.id.serviceTypeTXT)  TextView serviceTypeTXT;
	@BindView(R.id.timeTXT)  TextView timeTXT;

	public static ConfirmDialog newInstance(Context context, Callback callback, String date, String service, String time) {
		ConfirmDialog dialog = new ConfirmDialog();
		dialog.context = context;
		dialog.callback = callback;
		dialog.cDate = date;
		dialog.cService = service;
		dialog.cTime = time;
		return dialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_confirm;
	}

	@Override
	public void onViewReady() {
		dateTXT.setText(cDate);
		serviceTypeTXT.setText(cService);
		timeTXT.setText(cTime);
	}

	@OnClick(R.id.btnNO)
	void exit(){
		if (callback!=null){
			dismiss();
		}
	}
	@OnClick(R.id.btnYES)
	void confirmYes(){
		if(callback !=null){
			callback.onConfirmSuccess();
			dismiss();
		}
	}

	@OnClick()
	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}
	public interface Callback{
		void onConfirmSuccess();
	}
}
