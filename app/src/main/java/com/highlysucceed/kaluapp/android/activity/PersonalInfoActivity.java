package com.highlysucceed.kaluapp.android.activity;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.infectious_covid.CompanyFragment;
import com.highlysucceed.kaluapp.android.fragment.DefaultFragment;
import com.highlysucceed.kaluapp.android.infectious_covid.AddEmployeeFragment;
import com.highlysucceed.kaluapp.android.infectious_covid.AntigenResultFragment;
import com.highlysucceed.kaluapp.android.infectious_covid.ClinicalHistoryFragment;
import com.highlysucceed.kaluapp.android.infectious_covid.CredentialsFragment;
import com.highlysucceed.kaluapp.android.infectious_covid.HistorySignsFragment;
import com.highlysucceed.kaluapp.android.infectious_covid.PersonDetailsFragment;
import com.highlysucceed.kaluapp.android.infectious_covid.PersonalInfoFragment;
import com.highlysucceed.kaluapp.android.infectious_covid.SignsFragment;
import com.highlysucceed.kaluapp.android.route.RouteActivity;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class PersonalInfoActivity extends RouteActivity {
    public static final String TAG = PersonalInfoActivity.class.getName().toString();

    @Override
    public int onLayoutSet() {
        return R.layout.activity_personal_info;
    }

    @Override
    public void onViewReady() {

    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "info":
                openPersonalInfoFragment();
                break;
            case "history_signs":
                openHistorySignsFragment();
                break;

        }
    }

    public void openDefaultFragment(){
        switchFragment(DefaultFragment.newInstance());
    }
    public void openPersonalInfoFragment(){ switchFragment(PersonalInfoFragment.newInstance()); }
    public void openClinicHistoryFragment(){ switchFragment(ClinicalHistoryFragment.newInstance()); }
    public void openAntigenFragment(){ switchFragment(AntigenResultFragment.newInstance()); }
    public void openCredentialsFragment(){ switchFragment(CredentialsFragment.newInstance()); }
    public void openSignsFragment(){ switchFragment(SignsFragment.newInstance()); }
    public void openAddEmployeeFragment(){ switchFragment(AddEmployeeFragment.newInstance()); }
    public void openHistorySignsFragment(){ switchFragment(HistorySignsFragment.newInstance()); }
    public void openPersonDetailsFragment(){ switchFragment(PersonDetailsFragment.newInstance()); }
    public void openCompanyFragment(){ switchFragment(CompanyFragment.newInstance()); }
}
