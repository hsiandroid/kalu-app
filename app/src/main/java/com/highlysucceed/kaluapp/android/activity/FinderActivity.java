package com.highlysucceed.kaluapp.android.activity;

import android.view.View;
import android.widget.ImageView;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.fragment.DefaultFragment;
import com.highlysucceed.kaluapp.android.fragment.finder.FastBookingFragment;
import com.highlysucceed.kaluapp.android.fragment.finder.FinderResultFagment;
import com.highlysucceed.kaluapp.android.route.RouteActivity;
import com.highlysucceed.kaluapp.data.model.api.FinderModel;
import com.highlysucceed.kaluapp.vendor.android.java.Log;

import java.util.List;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class FinderActivity extends RouteActivity {

    public static final String TAG = FinderActivity.class.getName().toString();

    @BindView(R.id.searchBTN)   ImageView searchBTN;
    @Override
    public int onLayoutSet() {
        return R.layout.activity_find;
    }

    @Override
    public void onViewReady() {
        searchBTN.setVisibility(View.GONE);
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "finder":
                openFinderDetails(getFragmentBundle().getString("id"),getFragmentBundle().getString("location"),getFragmentBundle().getString("date"),getFragmentBundle().getString("hmo"));
                break;
        }
    }

    public void openDefaultFragment(){
        switchFragment(DefaultFragment.newInstance());
    }
    public void openFinderDetails(String id, String location, String date, String hmo) { switchFragment(FinderResultFagment.newInstance(id,location,date,hmo)); }
    public void openFastBookingFragment(String clinicID, String providerID, String date, String timeStart, String timeEnd) { switchFragment(FastBookingFragment.newInstance(clinicID,providerID,date,timeStart,timeEnd)); }

}
