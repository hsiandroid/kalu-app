package com.highlysucceed.kaluapp.android.dialog;

import android.content.Context;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.vendor.android.base.BaseDialog;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Callback;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class HMODialog extends BaseDialog {
	public static final String TAG = HMODialog.class.getName().toString();
	private Context context;
	private Callback callback;

	@BindView(R.id.medicardNumberET)   EditText medicardNumberET;
	@BindView(R.id.medicardREFET)      EditText medicardREFET;
	@BindView(R.id.medicardEXPIRYET)   EditText medicardEXPIRYET;

	public static HMODialog newInstance(Context context, Callback callback) {
		HMODialog dialog = new HMODialog();
		dialog.context = context;
		dialog.callback = callback;
		return dialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_hmo_details;
	}

	@Override
	public void onViewReady() {

	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}
	@OnClick(R.id.btnADD)
	void success(){
		if (!medicardNumberET.getText().toString().isEmpty()){
			callback.outputHMO(true);
			dismiss();
		}
	}

	public interface Callback{
		void outputHMO(boolean hasValue);
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		getDialog().getWindow().getAttributes().windowAnimations = R.style.dialog_slide_animation; //style id
	}
}
