package com.highlysucceed.kaluapp.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.data.model.api.AccreditorModel;
import com.highlysucceed.kaluapp.data.model.api.AccridetorModel;
import com.highlysucceed.kaluapp.vendor.android.base.BaseRecylerViewAdapter;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class AccreditorDialogRecyclerViewAdapter extends BaseRecylerViewAdapter<AccreditorDialogRecyclerViewAdapter.ViewHolder, AccridetorModel>{



    public AccreditorDialogRecyclerViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_dialog_accreditor));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.hmoTXT.setText(holder.getItem().detail);
        if(!holder.getItem().thumbnail.filename.equals("")) {
            Glide.with(getContext())
                    .load(holder.getItem().thumbnail.fullPath)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.ic_no_image)
                            .error(R.drawable.ic_no_image))
                    .into(holder.hmoIMG);

        }
        else{
            Glide.with(getContext())
                    .load(R.drawable.ic_no_image)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.ic_no_image)
                            .error(R.drawable.ic_no_image))
                    .into(holder.hmoIMG);
        }


    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.hmoTXT)                TextView hmoTXT;
        @BindView(R.id.hmoIMG)                ImageView hmoIMG;

        public ViewHolder(View view) {
            super(view);
        }

        public AccridetorModel getItem() {
            return (AccridetorModel) super.getItem();
        }
    }

}
