package com.highlysucceed.kaluapp.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.data.model.api.ClinicModel;
import com.highlysucceed.kaluapp.data.model.api.HolderModel;
import com.highlysucceed.kaluapp.data.model.api.HospitalModel;
import com.highlysucceed.kaluapp.data.model.api.SampleModel;
import com.highlysucceed.kaluapp.vendor.android.base.BaseRecylerViewAdapter;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class HospitalRecyclerViewAdapter extends BaseRecylerViewAdapter<HospitalRecyclerViewAdapter.ViewHolder, ClinicModel>{

    private Callback callback;

    public HospitalRecyclerViewAdapter(Context context, Callback callback) {
        super(context);
        this.callback = callback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_hospital_list));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.hospitalTXT.setText(holder.getItem().name);
        holder.addressTXT.setText(holder.getItem().address);

        if(!holder.getItem().thumbnail.filename.equals("")) {
            Glide.with(getContext())
                    .load(holder.getItem().thumbnail.fullPath)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.ic_no_image)
                            .error(R.drawable.ic_no_image))
                    .into(holder.hospitalIV);

        }
        else{
            Glide.with(getContext())
                    .load(R.drawable.ic_no_image)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.ic_no_image)
                            .error(R.drawable.ic_no_image))
                    .into(holder.hospitalIV);
        }
    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder implements View.OnClickListener {

        @BindView(R.id.hospitalIV)      ImageView  hospitalIV;
        @BindView(R.id.hospitalTXT)     TextView hospitalTXT;
        @BindView(R.id.addressTXT)      TextView addressTXT;
        @BindView(R.id.callBTN)         TextView callBTN;
        @BindView(R.id.bookBTN)         TextView bookBTN;

        public ViewHolder(View view) {
            super(view);
            callBTN.setOnClickListener(this);
            bookBTN.setOnClickListener(this);
        }

        public ClinicModel getItem() {
            return (ClinicModel) super.getItem();
        }

        @Override
        public void onClick(View v) {
             switch (v.getId()){

                 case R.id.callBTN:
                     callback.setContactNumber(getItem().contactNumber);
                     break;
                 case R.id.bookBTN:
                     callback.setType(String.valueOf(getItem().id));
             }
        }
    }


    public interface Callback{
        void setType(String  id);
        void setContactNumber(String number);
    }

} 
