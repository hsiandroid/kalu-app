package com.highlysucceed.kaluapp.android.fragment.clinic;

import android.content.res.ColorStateList;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.activity.BookingActivity;
import com.highlysucceed.kaluapp.android.adapter.ClinicAccridetorRecyclerViewAdapter;
import com.highlysucceed.kaluapp.android.adapter.ServicesRecyclerViewAdapter;
import com.highlysucceed.kaluapp.android.dialog.HMOAccreditedDialog;
import com.highlysucceed.kaluapp.data.model.api.AccridetorModel;
import com.highlysucceed.kaluapp.data.model.api.ProviderDetailsModel;
import com.highlysucceed.kaluapp.data.model.api.ProviderModel;
import com.highlysucceed.kaluapp.data.model.api.ServiceModel;
import com.highlysucceed.kaluapp.server.request.Clinic;
import com.highlysucceed.kaluapp.vendor.android.base.BaseFragment;
import com.highlysucceed.kaluapp.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.highlysucceed.kaluapp.vendor.server.request.APIRequest;
import com.highlysucceed.kaluapp.vendor.server.transformer.CollectionTransformer;
import com.highlysucceed.kaluapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class DoctorDetailsFragment extends BaseFragment implements View.OnClickListener, EndlessRecyclerViewScrollListener.Callback, SwipeRefreshLayout.OnRefreshListener {
    public static final String TAG = DoctorDetailsFragment.class.getName().toString();
    private BookingActivity activity;
    private ClinicAccridetorRecyclerViewAdapter clinicAccridetorRecyclerViewAdapter;
    private ServicesRecyclerViewAdapter servicesRecyclerViewAdapter;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private LinearLayoutManager linearLayoutManager;
    private LinearLayoutManager servicesLinearLayoutManager;
    private  List<ServiceModel> serviceModels = new ArrayList<>();
    private APIRequest apiRequest;


    @BindView(R.id.btnBookApnmnt)           LinearLayout btnBookApnmnt;
    @BindView(R.id.recyclerHMOAccridetor)   RecyclerView recyclerHMOAccridetor;
    @BindView(R.id.services)                RecyclerView services;
    @BindView(R.id.backgroundIV)            ImageView backgroundIV;
    @BindView(R.id.doctorIV)                ImageView doctorIV;
    @BindView(R.id.doctorNameTXT)           TextView doctorIdoctorNameTXTV;
    @BindView(R.id.doctorExpertiseTXT)      TextView doctorExpertiseTXT;
    @BindView(R.id.activityTitleTXT)        TextView  activityTitleTXT;
    @BindView(R.id.hmoViewBTN)              TextView  hmoViewBTN;
    @BindView(R.id.backButton)              ImageView backButton;
    @BindView(R.id.searchBTN)               ImageView searchBTN;
    @BindView(R.id.homeSRL)                 SwipeRefreshLayout homeSRL;

    String clinicID, providerID;
    ColorStateList defaultColor;

    public static DoctorDetailsFragment newInstance(String clinic_ID,  String provider_ID) {
        DoctorDetailsFragment fragment = new DoctorDetailsFragment();
        fragment.clinicID = clinic_ID;
        fragment.providerID = provider_ID;
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_doctor_details;
    }

    @Override
    public void onViewReady() {
        btnBookApnmnt.setOnClickListener(this);
        activity = (BookingActivity) getContext();
        searchBTN.setVisibility(View.GONE);
        setUpListView();
        homeSRL.setOnRefreshListener(this);
        hmoViewBTN.setOnClickListener(this);
    }

    private void setUpListView() {
        clinicAccridetorRecyclerViewAdapter = new ClinicAccridetorRecyclerViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(RecyclerView.HORIZONTAL);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, this);
        recyclerHMOAccridetor.addOnScrollListener(endlessRecyclerViewScrollListener);
        recyclerHMOAccridetor.setLayoutManager(linearLayoutManager);
        recyclerHMOAccridetor.setAdapter(clinicAccridetorRecyclerViewAdapter);

        servicesRecyclerViewAdapter = new ServicesRecyclerViewAdapter(getContext());
        servicesLinearLayoutManager = new LinearLayoutManager(getContext());
        services.setLayoutManager(servicesLinearLayoutManager);
        services.setAdapter(servicesRecyclerViewAdapter);

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        activityTitleTXT.setText("Doctor Details");
        refreshList();
    }

    @Subscribe
    public void onResponse(Clinic.ProviderDetailsResponse detailsResponse) {
        SingleTransformer<ProviderDetailsModel> transformer = detailsResponse.getData(SingleTransformer.class);
        if(transformer.status){
            doctorIdoctorNameTXTV.setText(transformer.data.name);
            doctorExpertiseTXT.setText(transformer.data.expertise);

            if(!transformer.data.coverPhoto.filename.equals("")) {
                Glide.with(getContext())
                        .load(transformer.data.coverPhoto.fullPath)
                        .apply(new RequestOptions()
                                .placeholder(R.drawable.ic_no_image)
                                .error(R.drawable.ic_no_image))
                        .into(backgroundIV);
            }
            else{
                Glide.with(getContext())
                        .load(R.drawable.ic_no_image)
                        .apply(new RequestOptions()
                                .placeholder(R.drawable.ic_no_image)
                                .error(R.drawable.ic_no_image))
                        .into(backgroundIV);
            }

            if(!transformer.data.profilePicture.filename.equals("")) {
                Glide.with(getContext())
                        .load(transformer.data.profilePicture.fullPath)
                        .apply(new RequestOptions()
                                .placeholder(R.drawable.ic_no_image)
                                .error(R.drawable.ic_no_image))
                        .into(doctorIV);

            }
            else{
                Glide.with(getContext())
                        .load(R.drawable.ic_no_image)
                        .apply(new RequestOptions()
                                .placeholder(R.drawable.ic_no_image)
                                .error(R.drawable.ic_no_image))
                        .into(doctorIV);
            }

            if(detailsResponse.isNext()){
               servicesRecyclerViewAdapter.addNewData(transformer.data.services.data);
            }
            else{
               servicesRecyclerViewAdapter.setNewData(transformer.data.services.data);
            }

        }
    }

    @Subscribe
    public void onResponse(Clinic.AccreditorResponse detailsResponse) {
        CollectionTransformer<AccridetorModel> transformer = detailsResponse.getData(CollectionTransformer.class);
        if(transformer.status){
            if(transformer.data.size() == 0) {
                hmoViewBTN.setEnabled(false);
                hmoViewBTN.setTextColor(defaultColor);
                if (detailsResponse.isNext()) {
                    clinicAccridetorRecyclerViewAdapter.addNewData(transformer.data);
                } else {
                    clinicAccridetorRecyclerViewAdapter.setNewData(transformer.data);
                }
            }
            else {
                hmoViewBTN.setEnabled(true);
                hmoViewBTN.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
            }
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnBookApnmnt:
                activity.openBookFragment(clinicID, providerID);
                break;
            case R.id.hmoViewBTN:

                break;
        }
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {

    }

    @OnClick(R.id.activityBackBTN)
    void activityBackBTN(){ activity.onBackPressed(); }

    @Override
    public void onRefresh() {
        refreshList();
    }
    public void refreshList(){
        apiRequest =  Clinic.getDefault().getProviderDetails(activity, providerID, homeSRL);
        apiRequest.first();

        apiRequest =  Clinic.getDefault().getClinicAccreditation(activity, clinicID);
        apiRequest.setPerPage(4);
    }
}
