package com.highlysucceed.kaluapp.android.dialog;

import android.content.Context;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.activity.BookingActivity;
import com.highlysucceed.kaluapp.android.adapter.DoctorListRecyclerViewAdapter;
import com.highlysucceed.kaluapp.data.model.api.DoctorModel;
import com.highlysucceed.kaluapp.data.model.api.ProviderModel;
import com.highlysucceed.kaluapp.vendor.android.base.BaseDialog;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class DoctorDialog extends BaseDialog implements DoctorListRecyclerViewAdapter.ClinicListener {
	public static final String TAG = DoctorDialog.class.getName().toString();

	DoctorListRecyclerViewAdapter doctorListRecyclerViewAdapter;
	private LinearLayoutManager       linearLayoutManager;
	private BookingActivity activity;
	private Context context;
	private Callback callback;
	List<ProviderModel> providerModels;
	String recyclerDetails;

	@BindView(R.id.recyclerList)   RecyclerView recyclerList;


	public static DoctorDialog newInstance(Context mCtx, Callback cbk, BookingActivity activity, List<ProviderModel> data, String details) {
		DoctorDialog dialog = new DoctorDialog();
		dialog.context = mCtx;
		dialog.callback = cbk;
		dialog.activity = activity;
		dialog.providerModels = data;
		dialog.recyclerDetails = details;
		return dialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_doctor;
	}

	@Override
	public void onViewReady() {
              setUpListView();
	}

	private void setUpListView() {
		doctorListRecyclerViewAdapter  =  new DoctorListRecyclerViewAdapter(activity, this);
		linearLayoutManager = new LinearLayoutManager(getContext());
		recyclerList.setLayoutManager(linearLayoutManager);
		recyclerList.setAdapter(doctorListRecyclerViewAdapter);

		if(recyclerDetails.equals("newData")){
			doctorListRecyclerViewAdapter.addNewData(providerModels);
		}
		else if(recyclerDetails.equals("setData")){
			doctorListRecyclerViewAdapter.setNewData(providerModels);
		}

	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}

	@Override
	public void providerID(String id) {
        callback.providerDetails(id);
	}


	public interface Callback{
		void providerDetails(String id);
	}
	@OnClick(R.id.closeBTN)
	void closeBTN(){
		this.dismiss();
	}
}
