package com.highlysucceed.kaluapp.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.data.model.api.OccupationModel;
import com.highlysucceed.kaluapp.data.model.api.SampleModel;
import com.highlysucceed.kaluapp.vendor.android.base.BaseRecylerViewAdapter;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ExpertiseRecyclerViewAdapter extends BaseRecylerViewAdapter<ExpertiseRecyclerViewAdapter.ViewHolder, OccupationModel>{

    private ClickListener clickListener;
    private int currentID = -1;
    private int defaultID;

    public ExpertiseRecyclerViewAdapter(Context context, ClickListener clickListener1) {
        super(context);
        this.clickListener = clickListener1;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_expertise));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.nameTXT.setText(holder.getItem().name);
    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.nameTXT)                 TextView nameTXT;
        @BindView(R.id.viewBTN)                 View viewBTN;

        public ViewHolder(View view) {
            super(view);
            viewBTN.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickListener.onItemClick(String.valueOf(getItem().id+1), getItem().name);
                }
            });
        }

        public OccupationModel getItem() {
            return (OccupationModel) super.getItem();
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener{
        void onItemClick(String id, String name);
    }
} 
