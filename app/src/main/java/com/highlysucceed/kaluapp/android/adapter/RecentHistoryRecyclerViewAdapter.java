package com.highlysucceed.kaluapp.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.data.model.api.ForProfileModel;
import com.highlysucceed.kaluapp.data.model.api.SampleModel;
import com.highlysucceed.kaluapp.vendor.android.base.BaseRecylerViewAdapter;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class RecentHistoryRecyclerViewAdapter extends BaseRecylerViewAdapter<RecentHistoryRecyclerViewAdapter.ViewHolder, ForProfileModel>{



    public RecentHistoryRecyclerViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_recent_details));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.recentIMG.setImageResource(holder.getItem().image);
        holder.recentNAME.setText(holder.getItem().name);
        holder.recentDETAILS.setText(holder.getItem().info);

    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{
        @BindView(R.id.recentIMG)
        CircleImageView recentIMG;
        @BindView(R.id.recentNAME)
        TextView recentNAME;
        @BindView(R.id.recentDETAILS)
        TextView recentDETAILS;

        public ViewHolder(View view) {
            super(view);
        }

        public ForProfileModel getItem() {
            return (ForProfileModel) super.getItem();
        }
    }


} 
