package com.highlysucceed.kaluapp.android.fragment.profile;

import android.app.Dialog;
import android.content.DialogInterface;
import android.util.Log;

import androidx.appcompat.app.AlertDialog;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.activity.ProfileActivity;
import com.highlysucceed.kaluapp.android.dialog.ChangePasswordDialog;
import com.highlysucceed.kaluapp.config.Keys;
import com.highlysucceed.kaluapp.data.preference.UserData;
import com.highlysucceed.kaluapp.server.request.Auth;
import com.highlysucceed.kaluapp.vendor.android.base.BaseFragment;
import com.highlysucceed.kaluapp.vendor.android.java.ToastMessage;
import com.highlysucceed.kaluapp.vendor.server.request.APIRequest;
import com.highlysucceed.kaluapp.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ProfileDetailsFragment extends BaseFragment implements ChangePasswordDialog.Callback {
    public static final String TAG = ProfileDetailsFragment.class.getName().toString();

    private ProfileActivity activity;
    private ChangePasswordDialog dialog;

    public static ProfileDetailsFragment newInstance() {
        ProfileDetailsFragment fragment = new ProfileDetailsFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_profile_details;
    }

    @Override
    public void onViewReady() {
       activity = (ProfileActivity) getContext();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.PasswordResponse response){
        BaseTransformer baseTransformer = response.getData(BaseTransformer.class);
        if (baseTransformer.status){
            ToastMessage.show(activity,baseTransformer.msg, ToastMessage.Status.SUCCESS);
            dialog.dismiss();
        }
        else {
            ToastMessage.show(activity,baseTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

@OnClick(R.id.myqrBTN)
    void myqrBTN(){
    activity.startProfileActivity("digital");
}

    @OnClick(R.id.logoutBTN)
    void logoutBTNClicked(){

        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        Auth.getDefault().logout(activity);
                        activity.startLandingActivity("login");
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        dialog.dismiss();
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage("Are you sure you want to logout?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();


    }


    @Subscribe
    public void onResponse(Auth.LogoutResponse logoutResponse){
        BaseTransformer baseTransformer = logoutResponse.getData(BaseTransformer.class);
        if(baseTransformer.status){
            ToastMessage.show(activity,baseTransformer.msg, ToastMessage.Status.SUCCESS);
            UserData.clearData();
            activity.startLandingActivity("login");
        }else{
            ToastMessage.show(activity,baseTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @OnClick(R.id.editProfileBTN)
    void editProfileBTN(){
        activity.openUpdateProfileFragment();
    }
    @OnClick(R.id.changePassBTN)
    void changePassBTN(){
        ChangePasswordDialog.newInstance(getContext(), this).show(getChildFragmentManager(), TAG);
    }

//    @OnClick(R.id.bookingHistoryBTN)
//    void clickBookingHistory() {
//        ToastMessage.show(getContext(), "Not Available", ToastMessage.Status.FAILED);
//    }


    @Override
    public void output(ChangePasswordDialog dialog, String old_password, String current_password, String confirm_password) {
         this.dialog = dialog;
        APIRequest apiRequest = Auth.getDefault().updatePassword(getContext())
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.PASSWORD, current_password)
                .addParameter(Keys.CONFIRM_PASSWORD, confirm_password)
                .addParameter(Keys.USER_ID, UserData.getUserModel().userId)
                .addParameter(Keys.CURRENT_PASSWORD, old_password);
        apiRequest.execute();

    }
}
