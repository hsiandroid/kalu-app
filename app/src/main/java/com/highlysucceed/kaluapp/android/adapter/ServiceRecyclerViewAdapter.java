package com.highlysucceed.kaluapp.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.data.model.api.SampleModel;
import com.highlysucceed.kaluapp.data.model.api.ServiceModel;
import com.highlysucceed.kaluapp.vendor.android.base.BaseRecylerViewAdapter;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ServiceRecyclerViewAdapter extends BaseRecylerViewAdapter<ServiceRecyclerViewAdapter.ViewHolder, ServiceModel>{

    private ClickListener clickListener;
    private Context context;

    public ServiceRecyclerViewAdapter(Context mContext, ClickListener clickListener1) {
        super(mContext);
        this.context = mContext;
        this.clickListener = clickListener1;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_services));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.serviceType.setText(holder.getItem().name);

    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder implements View.OnClickListener {

        @BindView(R.id.serviceType)  TextView serviceType;
        @BindView(R.id.checkboxBTN) CheckBox checkboxBTN;

        public ViewHolder(View view) {
            super(view);
            checkboxBTN.setOnClickListener(this);
        }

        public ServiceModel getItem() {
            return (ServiceModel) super.getItem();
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(String.valueOf(getItem().id), getItem().name);

        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener{
        void onItemClick(String id, String service);
    }
} 
