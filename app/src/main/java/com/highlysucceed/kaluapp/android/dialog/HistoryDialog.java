package com.highlysucceed.kaluapp.android.dialog;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.vendor.android.base.BaseDialog;

import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class HistoryDialog extends BaseDialog {
	public static final String TAG = HistoryDialog.class.getName().toString();

	private Context context;
	private Callback callback;

	public static HistoryDialog newInstance(Context context, Callback callback) {
		HistoryDialog dialog = new HistoryDialog();
		dialog.context = context;
		dialog.callback = callback;
		return dialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_travel_history;
	}

	@Override
	public void onViewReady() {

	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}

	@OnClick(R.id.btnADD)
	void success(){
		if (callback!=null){
			callback.onSuccess();
			dismiss();
		}
	}
	public interface Callback{
		void onSuccess();
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		getDialog().getWindow().getAttributes().windowAnimations = R.style.dialog_slide_animation; //style id
	}

}
