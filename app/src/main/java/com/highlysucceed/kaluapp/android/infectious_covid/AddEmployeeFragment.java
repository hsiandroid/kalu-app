package com.highlysucceed.kaluapp.android.infectious_covid;

import android.app.Person;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.activity.PersonalInfoActivity;
import com.highlysucceed.kaluapp.android.dialog.FormsDialog;
import com.highlysucceed.kaluapp.server.request.Auth;
import com.highlysucceed.kaluapp.vendor.android.base.BaseFragment;
import com.highlysucceed.kaluapp.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class AddEmployeeFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = AddEmployeeFragment.class.getName().toString();
    PersonalInfoActivity activity = new PersonalInfoActivity();

    @BindView(R.id.idNumET)             EditText idNumET;
    @BindView(R.id.fullNameET)          EditText fullNameET;
    @BindView(R.id.spinnerGender)       Spinner spinnerGender;
    @BindView(R.id.birthdayTXT)         TextView birthdayTXT;
    @BindView(R.id.contactET)           EditText contactET;
    @BindView(R.id.emailET)             EditText emailET;
    @BindView(R.id.addrET)              EditText addrET;
    @BindView(R.id.philHealthET)        EditText philHealthET;
    @BindView(R.id.addBTN)              Button addBTN;
    @BindView(R.id.formsBTN)            TextView formsBTN;

    String selectedGender, selectedBdate;

    public static AddEmployeeFragment newInstance() {
        AddEmployeeFragment fragment = new AddEmployeeFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_add_employee;
    }

    @Override
    public void onViewReady() {
        activity = (PersonalInfoActivity) getContext();
        activity.setTitle("Add Employee");

        addBTN.setOnClickListener(this);
        formsBTN.setOnClickListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.addBTN:
                activity.openCompanyFragment();
                break;
            case R.id.formsBTN:
                FormsDialog.newInstance().show(getChildFragmentManager(), TAG);
                break;
        }
    }
}
