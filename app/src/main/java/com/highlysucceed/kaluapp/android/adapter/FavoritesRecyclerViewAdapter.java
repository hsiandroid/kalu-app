package com.highlysucceed.kaluapp.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.data.model.api.FavoritesModel;
import com.highlysucceed.kaluapp.vendor.android.base.BaseRecylerViewAdapter;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class FavoritesRecyclerViewAdapter extends BaseRecylerViewAdapter<FavoritesRecyclerViewAdapter.ViewHolder, FavoritesModel> {

    private ClickListener clickListener;

    public FavoritesRecyclerViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_favorites));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.nameTXT.setText(holder.getItem().name);
    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.nameTXT)     TextView nameTXT;
        @BindView(R.id.adapterCON)  View adapterCON;

        public ViewHolder(View view) {
            super(view);
        }

        public FavoritesModel getItem() {
            return (FavoritesModel) super.getItem();
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener{
        void onItemClick(FavoritesModel testKitModel);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.adapterCON:
                if (clickListener != null){
                    clickListener.onItemClick((FavoritesModel) v.getTag());
                }
                break;
        }
    }


} 
