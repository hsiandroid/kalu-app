package com.highlysucceed.kaluapp.android.activity;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.fragment.DefaultFragment;
import com.highlysucceed.kaluapp.android.fragment.practitioner.PractitionerDetailsFragment;
import com.highlysucceed.kaluapp.android.fragment.practitioner.PractitionersFragment;
import com.highlysucceed.kaluapp.android.fragment.practitioner.ProviderDetailsFragment;
import com.highlysucceed.kaluapp.android.fragment.practitioner.ViewAllPractitionerFragment;
import com.highlysucceed.kaluapp.android.route.RouteActivity;
import com.highlysucceed.kaluapp.server.request.Practitioner;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class PractitionerActivity extends RouteActivity {
    public static final String TAG = PractitionerActivity.class.getName().toString();

    @Override
    public int onLayoutSet() {
        return R.layout.activity_practitioner;
    }

    @Override
    public void onViewReady() {

    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "practitioner":
                openPractitionerDetails();
                break;

        }
    }

    public void openDefaultFragment(){
        switchFragment(DefaultFragment.newInstance());
    }
    public void openPractitionerDetails(){ switchFragment(PractitionerDetailsFragment.newInstance()); }
    public void openProviderDetails(String id){ switchFragment(ProviderDetailsFragment.newInstance(id)); }
    public void openViewAllPractitionerDetails(){ switchFragment(ViewAllPractitionerFragment.newInstance()); }
    public void openPractitionersFragment(String id){ switchFragment(PractitionersFragment.newInstance(id)); }
}
