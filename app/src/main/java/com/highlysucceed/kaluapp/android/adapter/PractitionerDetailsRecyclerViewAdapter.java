package com.highlysucceed.kaluapp.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.data.model.api.PractitionerModel;
import com.highlysucceed.kaluapp.data.model.api.SampleModel;
import com.highlysucceed.kaluapp.vendor.android.base.BaseRecylerViewAdapter;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class PractitionerDetailsRecyclerViewAdapter extends BaseRecylerViewAdapter<PractitionerDetailsRecyclerViewAdapter.ViewHolder, PractitionerModel>{

    private ClickListener clickListener;

    public PractitionerDetailsRecyclerViewAdapter(Context context, ClickListener clickListener1) {
        super(context);
        this.clickListener = clickListener1;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_practitioner_details));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));

        holder.nameTXT.setText(holder.getItem().name);
        holder.expertiseTitleTXT.setText(holder.getItem().expertise);
        if(!holder.getItem().profilePicture.filename.equals("")) {
            Glide.with(getContext())
                    .load(holder.getItem().profilePicture.fullPath)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.ic_doctor_new)
                            .error(R.drawable.ic_doctor_new))
                    .into(holder.imageIV);

        }
        else{
            Glide.with(getContext())
                    .load(R.drawable.ic_doctor_new)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.ic_doctor_new)
                            .error(R.drawable.ic_doctor_new))
                    .into(holder.imageIV);
        }

    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.imageIV)                ImageView imageIV;
        @BindView(R.id.nameTXT)                TextView nameTXT;
        @BindView(R.id.expertiseTitleTXT)      TextView expertiseTitleTXT;
        @BindView(R.id.vmdBTN)                 TextView vmdBTN;

        public ViewHolder(View view) {
            super(view);
            vmdBTN.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickListener.onClickPractitioner(String.valueOf(getItem().id));
                }
            });
        }

        public PractitionerModel getItem() {
            return (PractitionerModel) super.getItem();
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener{
        void onClickPractitioner(String id);
    }
} 
