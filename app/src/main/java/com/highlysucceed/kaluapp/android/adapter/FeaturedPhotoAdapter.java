package com.highlysucceed.kaluapp.android.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.data.model.api.FeturedPhotoModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class FeaturedPhotoAdapter  extends BaseAdapter {
    private  Context mContext;
    private  List<FeturedPhotoModel> feturedPhotoModelList = new ArrayList<>();
    @BindView(R.id.featuredIMG)  ImageView featuredIMG;

    public FeaturedPhotoAdapter(Context mContext, List<FeturedPhotoModel> feturedPhotoModelList) {
        this.mContext = mContext;
        this.feturedPhotoModelList = feturedPhotoModelList;
    }

    @Override
    public int getCount() {
        return feturedPhotoModelList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
          final FeturedPhotoModel photoModel = feturedPhotoModelList.get(position);
        ImageView imageView ;
        if (convertView == null) {  // if it's not recycled, initialize some attributes
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(150, 150));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

        } else {
            imageView = (ImageView) convertView;
        }

        if(!photoModel.thumbnail.filename.equals("")) {
            Glide.with(mContext)
                    .load(photoModel.thumbnail.fullPath)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.ic_no_image)
                            .error(R.drawable.ic_no_image))
                    .into(imageView);
        }
        else{
            Glide.with(mContext)
                    .load(photoModel.thumbnail.fullPath)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.ic_no_image)
                            .error(R.drawable.ic_no_image))
                    .into(imageView);
        }
        return imageView;
    }
}
