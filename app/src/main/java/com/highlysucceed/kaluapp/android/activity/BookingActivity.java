package com.highlysucceed.kaluapp.android.activity;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.fragment.DefaultFragment;
import com.highlysucceed.kaluapp.android.fragment.clinic.BookAppointmentFragment;
import com.highlysucceed.kaluapp.android.fragment.clinic.ClinicDetailsFragment;
import com.highlysucceed.kaluapp.android.fragment.clinic.ClinicFragment;
import com.highlysucceed.kaluapp.android.fragment.clinic.DoctorDetailsFragment;
import com.highlysucceed.kaluapp.android.fragment.clinic.HospitalFragment;
import com.highlysucceed.kaluapp.android.fragment.finder.FinderResultFagment;
import com.highlysucceed.kaluapp.android.route.RouteActivity;
import com.highlysucceed.kaluapp.data.model.api.DataModel;
import com.highlysucceed.kaluapp.data.model.api.FinderModel;
import com.highlysucceed.kaluapp.vendor.android.java.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class BookingActivity extends RouteActivity {
    public static final String TAG = BookingActivity.class.getName().toString();
    private DataModel dataModel;


    @Override
    public int onLayoutSet() {
        return R.layout.activity_booking;
    }

    @Override
    public void onViewReady() {
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "clinic":
                openClinic();
                break;

        }
    }

    public void openDefaultFragment(){
        switchFragment(DefaultFragment.newInstance());
    }
    public void openClinic() { switchFragment(ClinicFragment.newInstance());}
    public void openHospitalFragment(String id) { switchFragment(HospitalFragment.newInstance(id)); }
    public void openClinicDetails(String id) { switchFragment(ClinicDetailsFragment.newInstance(id)); }
    public void openDoctorDetails(String clinicId, String providerID) { switchFragment(DoctorDetailsFragment.newInstance(clinicId,providerID)); }
    public void openBookFragment(String clinicId, String providerID) { switchFragment(BookAppointmentFragment.newInstance(clinicId, providerID)); }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void closeKeyboard()
    {
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager manager = (InputMethodManager)
                    getSystemService(
                            Context.INPUT_METHOD_SERVICE);
            manager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
