package com.highlysucceed.kaluapp.android.dialog;

import android.content.Context;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.adapter.ExpertiseListRecyclerViewAdapter;
import com.highlysucceed.kaluapp.android.adapter.ExpertiseRecyclerViewAdapter;
import com.highlysucceed.kaluapp.data.model.api.OccupationModel;
import com.highlysucceed.kaluapp.vendor.android.base.BaseDialog;

import java.util.List;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ProviderDialog extends BaseDialog  implements ExpertiseRecyclerViewAdapter.ClickListener{
	public static final String TAG = ProviderDialog.class.getName().toString();

	private ExpertiseRecyclerViewAdapter expertiseRecyclerViewAdapter;
	private LinearLayoutManager linearLayoutManager;
	private List<OccupationModel> datalist;
	private Context context;
	private Callback callback;

	@BindView(R.id.recyclerProviderList)  RecyclerView recyclerProviderList;

	public static ProviderDialog newInstance(Context context,  Callback callback, List<OccupationModel> data) {
		ProviderDialog dialog = new ProviderDialog();
		dialog.context =context;
		dialog.datalist = data;
		dialog.callback =callback;
		return dialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_provider;
	}

	@Override
	public void onViewReady() {
        setUpListView();
	}

	private void setUpListView() {
		expertiseRecyclerViewAdapter = new ExpertiseRecyclerViewAdapter(getContext(), this);
		linearLayoutManager = new LinearLayoutManager(getContext());
		recyclerProviderList.setLayoutManager(linearLayoutManager);
		recyclerProviderList.setAdapter(expertiseRecyclerViewAdapter);

		expertiseRecyclerViewAdapter.setNewData(datalist);
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}


	@Override
	public void onItemClick(String id, String name) {
		callback.providerCallback(this, id, name);
	}

	public interface Callback{
		void providerCallback(ProviderDialog dialog, String id, String name);
	}
}
