package com.highlysucceed.kaluapp.android.fragment.practitioner;

import android.util.Log;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.activity.PractitionerActivity;
import com.highlysucceed.kaluapp.android.adapter.ClinicAccridetorRecyclerViewAdapter;
import com.highlysucceed.kaluapp.android.adapter.FeaturedPhotoAdapter;
import com.highlysucceed.kaluapp.android.adapter.FeaturedPhotoRecyclerViewAdapter;
import com.highlysucceed.kaluapp.android.adapter.ServicesRecyclerViewAdapter;
import com.highlysucceed.kaluapp.data.model.api.AccreditorModel;
import com.highlysucceed.kaluapp.data.model.api.FeturedPhotoModel;
import com.highlysucceed.kaluapp.data.model.api.ProviderModel;
import com.highlysucceed.kaluapp.data.model.api.ServiceModel;
import com.highlysucceed.kaluapp.server.request.Auth;
import com.highlysucceed.kaluapp.server.request.Clinic;
import com.highlysucceed.kaluapp.server.request.Practitioner;
import com.highlysucceed.kaluapp.vendor.android.base.BaseFragment;
import com.highlysucceed.kaluapp.vendor.server.request.APIRequest;
import com.highlysucceed.kaluapp.vendor.server.transformer.BaseTransformer;
import com.highlysucceed.kaluapp.vendor.server.transformer.CollectionTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ProviderDetailsFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {
    public static final String TAG = ProviderDetailsFragment.class.getName().toString();
    String provider_id;
    private PractitionerActivity activity;
    private ServicesRecyclerViewAdapter servicesRecyclerViewAdapter;
    private FeaturedPhotoRecyclerViewAdapter featuredPhotoRecyclerViewAdapter;
    private ClinicAccridetorRecyclerViewAdapter clinicAccridetorRecyclerViewAdapter;
    private LinearLayoutManager linearLayoutManager;
    private APIRequest apiRequest;

    @BindView(R.id.coverPhotoIV)            ImageView coverPhotoIV;
    @BindView(R.id.profileIMG)              ImageView profileIMG;
    @BindView(R.id.searchBTN)               ImageView searchBTN;
    @BindView(R.id.activityTitleTXT)        TextView activityTitleTXT;
    @BindView(R.id.expertiseTitleTXT)       TextView expertiseTitleTXT;
    @BindView(R.id.clinicDetailsTXT)        TextView clinicDetailsTXT;
    @BindView(R.id.clinicBHTXT)             TextView clinicBHTXT;
    @BindView(R.id.aboutTXT)                TextView  aboutTXT;
    @BindView(R.id.providerDetailsTXT)      TextView  providerDetailsTXT;
    @BindView(R.id.noResponseTXT)           TextView  noResponseTXT;
    @BindView(R.id.recyclerServices)        RecyclerView recyclerServices;
    @BindView(R.id.recyclerHMOAccridetor)   RecyclerView recyclerHMOAccridetor;
    @BindView(R.id.recyclerClinicPhoto)     RecyclerView recyclerClinicPhoto;
    @BindView(R.id.homeSRL)                 SwipeRefreshLayout homeSRL;



    public static ProviderDetailsFragment newInstance(String id) {
        ProviderDetailsFragment fragment = new ProviderDetailsFragment();
        fragment.provider_id = id;
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_provider_details;
    }

    @Override
    public void onViewReady() {
        activity = (PractitionerActivity) getContext();
        setUpListView();
        searchBTN.setVisibility(View.GONE);
        homeSRL.setOnRefreshListener(this);
    }

    private void setUpListView() {
        servicesRecyclerViewAdapter = new ServicesRecyclerViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerServices.setLayoutManager(linearLayoutManager);
        recyclerServices.setAdapter(servicesRecyclerViewAdapter);

        clinicAccridetorRecyclerViewAdapter = new ClinicAccridetorRecyclerViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(RecyclerView.HORIZONTAL);
        recyclerHMOAccridetor.setLayoutManager(linearLayoutManager);
        recyclerHMOAccridetor.setAdapter(clinicAccridetorRecyclerViewAdapter);

        featuredPhotoRecyclerViewAdapter = new FeaturedPhotoRecyclerViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(RecyclerView.HORIZONTAL);
        recyclerClinicPhoto.setLayoutManager(linearLayoutManager);
        recyclerClinicPhoto.setAdapter(featuredPhotoRecyclerViewAdapter);

    }


    @Override
    public void onResume() {
        super.onResume();
        onRefresh();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

//    @Subscribe
//    public void onResponse(Clinic.ProviderDetailsResponse detailsResponse) {
//        CollectionTransformer<ProviderModel> transformer = detailsResponse.getData(CollectionTransformer.class);
//        if(transformer.status){
////            String operatingHr = transformer.data.get(0).clinic.data.get(0).clinicStartTime + " - " + transformer.data.get(0).clinic.data.get(0).clinicEndTime;
////            activityTitleTXT.setText(transformer.data.get(0).name);
////            expertiseTitleTXT.setText(transformer.data.get(0).expertise);
////            aboutTXT.setText("About Dr. " + transformer.data.get(0).name);
////            clinicDetailsTXT.setText(transformer.data.get(0).clinic.data.get(0).address);
////            clinicBHTXT.setText(operatingHr);
//
//            if(!transformer.data.get(0).about.equals("")) {
//                noResponseTXT.setVisibility(View.GONE);
//                providerDetailsTXT.setText("" + transformer.data.get(0).about);
//            } else{
//                providerDetailsTXT.setVisibility(View.GONE);
//                noResponseTXT.setVisibility(View.VISIBLE);
//            }
//
//            if(!transformer.data.get(0).coverPhoto.filename.equals("")) {
//                Glide.with(getContext())
//                        .load(transformer.data.get(0).coverPhoto.fullPath)
//                        .apply(new RequestOptions()
//                                .placeholder(R.drawable.ic_no_image)
//                                .error(R.drawable.ic_no_image))
//                        .into(coverPhotoIV);
//            } else{
//                Glide.with(getContext())
//                        .load(R.drawable.clinic_bg)
//                        .apply(new RequestOptions()
//                                .placeholder(R.drawable.clinic_bg)
//                                .error(R.drawable.clinic_bg))
//                        .into(coverPhotoIV);
//            }
//
//            if(!transformer.data.get(0).profilePicture.filename.equals("")) {
//                Glide.with(getContext())
//                        .load(transformer.data.get(0).profilePicture.fullPath)
//                        .apply(new RequestOptions()
//                                .placeholder(R.drawable.ic_no_image)
//                                .error(R.drawable.ic_no_image))
//                        .into(profileIMG);
//
//            } else{
//                Glide.with(getContext())
//                        .load(R.drawable.ic_no_image)
//                        .apply(new RequestOptions()
//                                .placeholder(R.drawable.ic_no_image)
//                                .error(R.drawable.ic_no_image))
//                        .into(profileIMG);
//            }
//
//
//            if(detailsResponse.isNext()){
////                setAddClinicPhoto(transformer.data.get(0).clinicPhotos.data);
////                setAddService(transformer.data.get(0).services.data);
//            }
//            else{
////                setNewClinicPhoto(transformer.data.get(0).clinicPhotos.data);
////                setNewService(transformer.data.get(0).services.data);
//            }
//
//        }
//    }
    private void setAddClinicPhoto(List<FeturedPhotoModel> data) {
        featuredPhotoRecyclerViewAdapter.addNewData(data);
    }
    private void setNewClinicPhoto(List<FeturedPhotoModel> data) {
        featuredPhotoRecyclerViewAdapter.setNewData(data);
    }


    private void setAddService(List<ServiceModel> services) {
        servicesRecyclerViewAdapter.addNewData(services);
    }
    private void setNewService(List<ServiceModel> services) {
        servicesRecyclerViewAdapter.addNewData(services);
    }

@OnClick(R.id.activityBackBTN)
    void  activityBackBTN(){
        activity.onBackPressed();
}

    @Override
    public void onRefresh() {
         refreshList();
    }
    public void refreshList(){
//        apiRequest =   Clinic.getDefault().getProvider(activity, provider_id, homeSRL);
        apiRequest.first();
    }
}
