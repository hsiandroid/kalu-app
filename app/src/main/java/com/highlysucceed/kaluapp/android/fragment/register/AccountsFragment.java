package com.highlysucceed.kaluapp.android.fragment.register;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.activity.RegisterActivity;
import com.highlysucceed.kaluapp.config.Keys;
import com.highlysucceed.kaluapp.data.model.api.AccountModel;
import com.highlysucceed.kaluapp.data.model.api.UserModel;
import com.highlysucceed.kaluapp.data.preference.UserData;
import com.highlysucceed.kaluapp.server.request.Auth;
import com.highlysucceed.kaluapp.vendor.android.base.BaseFragment;
import com.highlysucceed.kaluapp.vendor.android.java.ToastMessage;
import com.highlysucceed.kaluapp.vendor.server.request.APIRequest;
import com.highlysucceed.kaluapp.vendor.server.transformer.BaseTransformer;
import com.highlysucceed.kaluapp.vendor.server.util.ErrorResponseManger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class AccountsFragment extends BaseFragment {
    public static final String TAG = AccountsFragment.class.getName().toString();

    private RegisterActivity registerActivity;

    @BindView(R.id.emailET)                 EditText emailET;
    @BindView(R.id.passET)                  EditText passET;
    @BindView(R.id.confirmPassET)           EditText confirmPassET;


    public static AccountsFragment newInstance() {
        AccountsFragment fragment = new AccountsFragment();
        return fragment;
    }


    @Override
    public int onLayoutSet() {
        return R.layout.fragment_accounts;
    }

    @Override
    public void onViewReady() {
        registerActivity = (RegisterActivity) getContext();
        registerActivity.showActivityNo1(true);
        registerActivity.showActivityNo2(false);
        registerActivity.showActivityNo3(false);
    }


    @Override
    public void onResume() {
        super.onResume();
        registerActivity.setTitle("Account");
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.SingleResponse singleResponse){
        BaseTransformer baseTransformer = singleResponse.getData(BaseTransformer.class);
        if(baseTransformer.status){
            AccountModel accountModel = new AccountModel();
            accountModel.email = emailET.getText().toString().trim();
            accountModel.password = passET.getText().toString().trim();
            accountModel.confirm_password = confirmPassET.getText().toString().trim();
            registerActivity.setAccountModel(accountModel);
            registerActivity.openSignUpFragment();
        }
        else {
            if (!ErrorResponseManger.first(baseTransformer.error.email).equals("")){
                emailET.setError(ErrorResponseManger.first(baseTransformer.error.email));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.password).equals("")){
                passET.setError(ErrorResponseManger.first(baseTransformer.error.password));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.password).equals("")){
                confirmPassET.setError(ErrorResponseManger.first(baseTransformer.error.password));
            }
        }
    }

    @OnClick(R.id.signUpBTN)
     void signUpBTN(){
        APIRequest apiRequest  = Auth.getDefault().preRegister(getContext())
                .addParameter(Keys.PROGRESS, 1)
                .addParameter(Keys.EMAIL, emailET.getText().toString().trim())
                .addParameter(Keys.PASSWORD, passET.getText().toString().trim())
                .addParameter(Keys.CONFIRM_PASSWORD, confirmPassET.getText().toString().trim());
                 apiRequest.execute();
    }

}
