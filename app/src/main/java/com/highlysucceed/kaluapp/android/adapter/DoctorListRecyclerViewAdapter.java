package com.highlysucceed.kaluapp.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.data.model.api.ClinicListModel;
import com.highlysucceed.kaluapp.data.model.api.DoctorModel;
import com.highlysucceed.kaluapp.data.model.api.ProviderModel;
import com.highlysucceed.kaluapp.vendor.android.base.BaseRecylerViewAdapter;

import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class DoctorListRecyclerViewAdapter extends BaseRecylerViewAdapter<DoctorListRecyclerViewAdapter.ViewHolder, ProviderModel>{

    private ClinicListener clickListener;
    private List<ClinicListModel> clinicListModels;

    public DoctorListRecyclerViewAdapter(Context context, ClinicListener clickListener) {
        super(context);
        this.clickListener = clickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_doctors_item));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));

//        holder.nameTXT.setText("Position " + holder.getItem().id);
          holder.doctorsName.setText(holder.getItem().name);
          holder.doctorsTypeTXT.setText(holder.getItem().expertise);
        if(!holder.getItem().profilePicture.fullPath.equals("")) {
            Glide.with(getContext())
                    .load(holder.getItem().profilePicture.fullPath)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.ic_no_image)
                            .error(R.drawable.ic_no_image))
                    .into(holder.doctorsIMG);

        }
        else{
            Glide.with(getContext())
                    .load(R.drawable.ic_no_image)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.ic_no_image)
                            .error(R.drawable.ic_no_image))
                    .into(holder.doctorsIMG);
        }

    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder implements View.OnClickListener {

        @BindView(R.id.doctorsIMG)  CircleImageView doctorsIMG;
        @BindView(R.id.doctorsName)  TextView doctorsName;
        @BindView(R.id.doctorsTypeTXT)  TextView doctorsTypeTXT;
        @BindView(R.id.doctorsListBTN) View doctorsListBTN;

        public ViewHolder(View view) {
            super(view);
            doctorsListBTN.setOnClickListener(this);
        }

        public ProviderModel getItem() {
            return (ProviderModel) super.getItem();
        }

        @Override
        public void onClick(View view) {
            clickListener.providerID(String.valueOf(getItem().id));
        }
    }

    public void setClickListener(ClinicListener clickListener) {
        this.clickListener = clickListener;
    }


    public interface ClinicListener{
        void providerID( String id);
    }

} 
