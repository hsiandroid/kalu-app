package com.highlysucceed.kaluapp.android.fragment.landing;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.activity.LandingActivity;
import com.highlysucceed.kaluapp.data.model.api.UserModel;
import com.highlysucceed.kaluapp.data.preference.UserData;
import com.highlysucceed.kaluapp.server.request.Auth;
import com.highlysucceed.kaluapp.vendor.android.base.BaseFragment;
import com.highlysucceed.kaluapp.vendor.android.java.ToastMessage;
import com.highlysucceed.kaluapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class SplashFragment extends BaseFragment {
    public static final String TAG = SplashFragment.class.getName();

    private LandingActivity activity;
    private Runnable runnable;
    private Handler handler;

    public static SplashFragment newInstance() {
        SplashFragment fragment = new SplashFragment();
        return fragment;
    }

    @Override
    public void onViewReady() {
        activity = (LandingActivity) getContext();


    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_splash;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isNetworkAvailable()){
            checkLogin();
        }else{
            openLoginFragment();
        }
    }

    public void openLoginFragment(){
        activity = (LandingActivity) getActivity();
        if (!UserData.isLogin()) {
            runnable = new Runnable() {
                @Override
                public void run() {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            activity.openLoginFragment();
                        }
                    });
                }
            };
            handler = new Handler();
            handler.postDelayed(runnable, 3000);
        } else {
            refreshToken();
        }
    }


    private void checkLogin() { Auth.getDefault().checkLogin(activity); }

    private void refreshToken() {
        Auth.getDefault().refreshToken(activity);
    }

    @Subscribe
    public void onResponse(Auth.CheckResponse response){
        SingleTransformer<UserModel> userTransformer = response.getData(SingleTransformer.class);
        if(userTransformer.status){
            UserData.insert(userTransformer.data);
            activity.startMainActivity("home");
            ToastMessage.show(getContext(),userTransformer.msg, ToastMessage.Status.SUCCESS);
        }else {
            refreshToken();
        }
    }

    @Subscribe
    public void onResponse(Auth.RefreshTokenResponse response){
        SingleTransformer<UserModel> userTransformer = response.getData(SingleTransformer.class);
        if(userTransformer.status){
            UserData.insert(userTransformer.data);
            UserData.insert(UserData.AUTHORIZATION, userTransformer.token);
            activity.startMainActivity("home");
            ToastMessage.show(getContext(),userTransformer.msg, ToastMessage.Status.SUCCESS);
        }else {
            activity.openLoginFragment();
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}