package com.highlysucceed.kaluapp.android.fragment.register;

import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.activity.RegisterActivity;
import com.highlysucceed.kaluapp.config.Keys;
import com.highlysucceed.kaluapp.data.model.api.SignUpModel;
import com.highlysucceed.kaluapp.data.model.api.UserModel;
import com.highlysucceed.kaluapp.server.request.Auth;
import com.highlysucceed.kaluapp.vendor.android.base.BaseFragment;
import com.highlysucceed.kaluapp.vendor.android.java.ToastMessage;
import com.highlysucceed.kaluapp.vendor.server.request.APIRequest;
import com.highlysucceed.kaluapp.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.security.Key;
import java.security.spec.KeySpec;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class HealthInformationFragment extends BaseFragment {
    public static final String TAG = HealthInformationFragment.class.getName().toString();

    private RegisterActivity registerActivity;
    private SignUpModel signUpModel;
    private APIRequest apiRequest;

    @BindView(R.id.signUpBTN)               TextView signUpBTN;
    @BindView(R.id.allergyET)               EditText allergyET;
    @BindView(R.id.historyET)               EditText historyET;
    @BindView(R.id.numberET)               EditText numberET;
    @BindView(R.id.referenceET)            EditText referenceET;
    @BindView(R.id.expiryET)               EditText expiryET;
    @BindView(R.id.maintenanceET)          EditText maintenanceET;


    public static HealthInformationFragment newInstance() {
        HealthInformationFragment fragment = new HealthInformationFragment();
        return fragment;
    }


    @Override
    public int onLayoutSet() {
        return R.layout.fragment_health_information;
    }

    @Override
    public void onViewReady() {
        registerActivity = (RegisterActivity) getContext();
        signUpModel = new SignUpModel();
        registerActivity.showActivityNo1(false);
        registerActivity.showActivityNo2(false);
        registerActivity.showActivityNo3(true);
    }

    @OnClick(R.id.signUpBTN)
    void onClick(){
        String allergy = allergyET.getText().toString();
        String history = historyET.getText().toString();
        String number  = numberET.getText().toString();
        String reference  = referenceET.getText().toString();
        String expiry  = expiryET.getText().toString();
        String maintainance  = maintenanceET.getText().toString();

        apiRequest = Auth.getDefault().register(registerActivity)
                .addParameter(Keys.EMAIL, registerActivity.getSignUpModel().email)
                .addParameter(Keys.PASSWORD, registerActivity.getSignUpModel().password)
                .addParameter(Keys.CONFIRM_PASSWORD, registerActivity.getSignUpModel().confirm_password)
                .addParameter(Keys.FIRSTNAME, registerActivity.getSignUpModel().firstname)
                .addParameter(Keys.LASTNAME, registerActivity.getSignUpModel().lastname)
                .addParameter(Keys.GENDER, registerActivity.getSignUpModel().gender)
                .addParameter(Keys.ADDRESS, registerActivity.getSignUpModel().address)
                .addParameter(Keys.CONTACT_NUMBER, registerActivity.getSignUpModel().contactNumber)
                .addParameter(Keys.BIRTHDATE, registerActivity.getSignUpModel().birthdate)
                .addParameter(Keys.BLOOD_TYPE, registerActivity.getSignUpModel().bloodType)
                .addParameter(Keys.HEIGHT, registerActivity.getSignUpModel().height)
                .addParameter(Keys.WEIGHT, registerActivity.getSignUpModel().weight)
                .addParameter(Keys.ALLERGIES, allergy)
                .addParameter(Keys.MEDICAL_HISTORY, history)
                .addParameter(Keys.MEDICARD_NUMBER, number)
                .addParameter(Keys.MEDICARD_REFERENCE, reference)
                .addParameter(Keys.MEDICARD_EXPIRY, expiry)
                .addParameter(Keys.CURRENT_MEDICATION, maintainance);
        apiRequest.execute();

    }

    @Override
    public void onResume() {
        super.onResume();
        registerActivity.setTitle("Health Information");
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.RegisterResponse registerResponse){
        BaseTransformer baseTransformer = registerResponse.getData(BaseTransformer.class);
        if (baseTransformer.status){
            ToastMessage.show(getContext(), baseTransformer.msg, ToastMessage.Status.SUCCESS);
            registerActivity.startLandingActivity("login");
        }
        else {
            ToastMessage.show(getContext(), baseTransformer.msg, ToastMessage.Status.FAILED);

        }
    }

}
