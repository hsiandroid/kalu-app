package com.highlysucceed.kaluapp.android.fragment.profile;

import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.activity.ProfileActivity;
import com.highlysucceed.kaluapp.android.dialog.BloodTypeDialog;
import com.highlysucceed.kaluapp.android.dialog.CalendarDialog;
import com.highlysucceed.kaluapp.config.Keys;
import com.highlysucceed.kaluapp.data.model.api.InfoModel;
import com.highlysucceed.kaluapp.data.model.api.UserModel;
import com.highlysucceed.kaluapp.data.preference.UserData;
import com.highlysucceed.kaluapp.server.request.Auth;
import com.highlysucceed.kaluapp.vendor.android.base.BaseFragment;
import com.highlysucceed.kaluapp.vendor.android.java.ImageManager;
import com.highlysucceed.kaluapp.vendor.android.java.ToastMessage;
import com.highlysucceed.kaluapp.vendor.server.request.APIRequest;
import com.highlysucceed.kaluapp.vendor.server.transformer.BaseTransformer;
import com.asksira.bsimagepicker.BSImagePicker;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.highlysucceed.kaluapp.vendor.server.transformer.CollectionTransformer;
import com.highlysucceed.kaluapp.vendor.server.transformer.SingleTransformer;
import com.highlysucceed.kaluapp.vendor.server.util.ErrorResponseManger;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.io.IOException;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Evanson on 2019-10-02.
 */

public class UpdateProfileFragment extends BaseFragment implements BSImagePicker.OnSingleImageSelectedListener, ImageManager.Callback, CalendarDialog.DateTimePickerListener, BloodTypeDialog.Callback, AdapterView.OnItemSelectedListener {
    public static final String TAG = UpdateProfileFragment.class.getName().toString();


    @BindView(R.id.fnameET)              EditText fnameET;
    @BindView(R.id.mNameET)              EditText mNameET;
    @BindView(R.id.lNameET)              EditText lNameET;
    @BindView(R.id.emailEDT)             EditText emailEDT;
    @BindView(R.id.phoneET)              EditText phoneET;
    @BindView(R.id.addressET)            EditText addressET;
    @BindView(R.id.bloodTypeET)          TextView bloodTypeET;
    @BindView(R.id.birthdayTXT)          TextView birthdayTXT;
    @BindView(R.id.heightET)             EditText heightET;
    @BindView(R.id.weightET)             EditText weightET;
    @BindView(R.id.genderET)            EditText genderET;
    @BindView(R.id.profileIMG)          ImageView profileIMG;
    @BindView(R.id.spinnerGender)       Spinner spinnerGender;

    String selectedGender;


    public static UpdateProfileFragment newInstance() {
        UpdateProfileFragment fragment = new UpdateProfileFragment();

        return fragment;
    }

    private ProfileActivity activity;


    @Override
    public int onLayoutSet() {
        return R.layout.fragment_update_profile;
    }

    @Override
    public void onViewReady() {
        activity = (ProfileActivity) getContext();
        activity.setTitle("Profile");
        spinnerGender.setOnItemSelectedListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        Auth.getDefault().getProfileDetails(activity);
    }



    @OnClick(R.id.uploadBTN)
    void uploadBTNClicked(){
        openFileChooser();
    }

    @OnClick(R.id.profileIMG)
    void profileIMGClicked(){
        openFileChooser();
    }

    @OnClick(R.id.birthdayTXT)
    void birthdayTXT(){
        CalendarDialog.newInstance(this, birthdayTXT.getText().toString()).show(getChildFragmentManager(), TAG);
    }
    @OnClick(R.id.bloodTypeET)
    void onBloodTypeClick(){
        BloodTypeDialog.newInstance(activity, this).show(getChildFragmentManager(), TAG);
    }
    @OnClick(R.id.doneBTN)
    void doneBTN(){
        APIRequest apiRequest  = Auth.getDefault().updateProfile(getContext())
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.FIRSTNAME, fnameET.getText().toString().trim())
                .addParameter(Keys.LASTNAME, lNameET.getText().toString().trim())
                .addParameter(Keys.GENDER, selectedGender)
                .addParameter(Keys.EMAIL,  emailEDT.getText().toString().trim())
                .addParameter(Keys.ADDRESS, addressET.getText().toString().trim())
                .addParameter(Keys.CONTACT_NUMBER, phoneET.getText().toString().trim())
                .addParameter(Keys.BIRTHDATE, birthdayTXT.getText().toString().trim())
                .addParameter(Keys.BLOOD_TYPE, bloodTypeET.getText().toString().trim())
                .addParameter(Keys.HEIGHT, heightET.getText().toString().trim())
                .addParameter(Keys.WEIGHT, weightET.getText().toString().trim())
                .addParameter(Keys.USER_ID, UserData.getUserModel().userId);
        apiRequest.execute();
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }


    @Subscribe
    public void onResponse(Auth.ProfileDetailsResponse response) {
        CollectionTransformer<InfoModel> transformer = response.getData(CollectionTransformer.class);
        if (transformer.status) {
                fnameET.setText(transformer.data.get(0).fname);
                lNameET.setText(transformer.data.get(0).lname);
                genderET.setText(transformer.data.get(0).gender);
                
                emailEDT.setText(transformer.data.get(0).PERSONALINFORMATION.email);
                phoneET.setText(transformer.data.get(0).PERSONALINFORMATION.contactNumber);
                addressET.setText(transformer.data.get(0).PERSONALINFORMATION.address);

                birthdayTXT.setText(transformer.data.get(0).PERSONALINFORMATION.birthdate);
                heightET.setText(transformer.data.get(0).PERSONALINFORMATION.height);
                weightET.setText(transformer.data.get(0).PERSONALINFORMATION.weight);
                
                if (transformer.data.get(0).PERSONALINFORMATION.gender.equals("Male")) {
                    spinnerGender.setSelection(0);
                }else {
                    spinnerGender.setSelection(1);
                }

                if (!transformer.data.get(0).PERSONALINFORMATION.bloodType.equals("null")) {
                    bloodTypeET.setText(transformer.data.get(0).PERSONALINFORMATION.bloodType);
                }else {
                    bloodTypeET.setText("--");
                }

                if (!transformer.data.get(0).profilePicture.filename.equals("")) {
                    Picasso.with(getContext()).load(transformer.data.get(0).profilePicture.fullPath)
                            .error(R.drawable.ic_user)
                            .placeholder(R.drawable.ic_user)
                            .into(profileIMG);
                } else {
                    Picasso.with(getContext()).load(R.drawable.ic_user)
                            .error(R.drawable.ic_user)
                            .placeholder(R.drawable.ic_user)
                            .into(profileIMG);
                }

        }



    }

    @Subscribe
    public void onResponse(Auth.SingleResponse singleResponse){
        BaseTransformer baseTransformer = singleResponse.getData(BaseTransformer.class);
        if (baseTransformer.status){
            ToastMessage.show(activity,baseTransformer.msg, ToastMessage.Status.SUCCESS);
            Glide.with(activity)
                    .load(file)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.ic_user)
                            .error(R.drawable.ic_user))
                    .into(profileIMG);
        }


    }
    @Subscribe
    public void onResponse(Auth.UpdateResponse response){
        BaseTransformer baseTransformer = response.getData(BaseTransformer.class);
        if (baseTransformer.status){
            ToastMessage.show(activity,baseTransformer.msg, ToastMessage.Status.SUCCESS);
            activity.onBackPressed();
        }
        else {
            ToastMessage.show(activity,baseTransformer.msg, ToastMessage.Status.FAILED);
            if (!ErrorResponseManger.first(baseTransformer.error.firstname).equals("")){
                fnameET.setError(ErrorResponseManger.first(baseTransformer.error.firstname));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.lastname).equals("")){
                lNameET.setError(ErrorResponseManger.first(baseTransformer.error.lastname));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.email).equals("")){
                emailEDT.setError(ErrorResponseManger.first(baseTransformer.error.email));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.address).equals("")){
                addressET.setError(ErrorResponseManger.first(baseTransformer.error.address));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.contactNumber).equals("")){
                phoneET.setError(ErrorResponseManger.first(baseTransformer.error.contactNumber));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.birthdate).equals("")){
                birthdayTXT.setError(ErrorResponseManger.first(baseTransformer.error.birthdate));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.bloodType).equals("")){
                bloodTypeET.setError(ErrorResponseManger.first(baseTransformer.error.bloodType));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.height).equals("")){
                heightET.setError(ErrorResponseManger.first(baseTransformer.error.height));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.weight).equals("")){
                weightET.setError(ErrorResponseManger.first(baseTransformer.error.weight));
            }
        }

    }

    private void openFileChooser() {
        BSImagePicker pickerDialog = new BSImagePicker.Builder("com.appgradingtechnology.labyalo.fileprovider").build();
        pickerDialog.show(getChildFragmentManager(), "picker");

    }


    @Override
    public void onSingleImageSelected(Uri uri, String tag) {
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(activity.getContentResolver(), uri);
            ImageManager.getFileFromBitmap(activity, bitmap, "image1", this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private File file;
    @Override
    public void success(File file) {
        this.file = file;
        Auth.getDefault().update_avatar(activity, file, String.valueOf(UserData.getUserModel().userId));
    }

    @Override
    public void forDisplay(String date) {
        birthdayTXT.setText(date);
    }

    @Override
    public void getBloodType(String type) {
        bloodTypeET.setText(type);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        if (parent.getItemAtPosition(position).toString().equals("MALE")) {
            selectedGender = "Male";
        }else {
            selectedGender = "Female";
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}