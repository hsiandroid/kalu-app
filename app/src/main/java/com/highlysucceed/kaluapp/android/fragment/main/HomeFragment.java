package com.highlysucceed.kaluapp.android.fragment.main;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.Image;
import android.net.Uri;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.activity.MainActivity;
import com.highlysucceed.kaluapp.android.adapter.ArticlesRecyclerViewAdapter;
import com.highlysucceed.kaluapp.android.adapter.FavoritesRecyclerViewAdapter;
import com.highlysucceed.kaluapp.android.adapter.NotificationRecyclerViewAdapter;
import com.highlysucceed.kaluapp.data.model.api.ArticlesModel;
import com.highlysucceed.kaluapp.data.model.api.FavoritesModel;
import com.highlysucceed.kaluapp.data.model.api.InfoModel;
import com.highlysucceed.kaluapp.data.model.api.UserModel;
import com.highlysucceed.kaluapp.data.preference.UserData;
import com.highlysucceed.kaluapp.server.request.Auth;
import com.highlysucceed.kaluapp.vendor.android.base.BaseFragment;
import com.highlysucceed.kaluapp.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.highlysucceed.kaluapp.vendor.android.java.ToastMessage;
import com.highlysucceed.kaluapp.vendor.server.transformer.BaseTransformer;
import com.highlysucceed.kaluapp.vendor.server.transformer.CollectionTransformer;
import com.highlysucceed.kaluapp.vendor.server.transformer.SingleTransformer;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class HomeFragment extends BaseFragment{
    public static final String TAG = HomeFragment.class.getName().toString();

    @BindView(R.id.profileIMG)              ImageView profileIMG;
    @BindView(R.id.daysIMG)                 ImageView daysIMG;
    @BindView(R.id.notAvailIMG)             ImageView notAvailIMG;
    @BindView(R.id.first1000DaysBTN)        View first1000DaysBTN;
    @BindView(R.id.menuBTN)                 ImageView menuBTN;

    private static final int ERROR_DIALOG_REQUEST = 9001;
    String userGender;
    boolean serviceOK = false;

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    private MainActivity activity;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_home;
    }

    @Override
    public void onViewReady() {
        activity = (MainActivity) getContext();
        if(isServicesOK()){
            serviceOK = true;
        }
        Auth.getDefault().getProfileDetails(getContext());
    }


    @Override
    public void onResume() {
        super.onResume();
//        activity.setTitle("Welcome " + "User");
        activity.homeActive();

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

        @Subscribe
    public void onResponse(Auth.ProfileDetailsResponse response) {
            CollectionTransformer<InfoModel> transformer = response.getData(CollectionTransformer.class);
        if (transformer.status) {

            if (!transformer.data.get(0).PERSONALINFORMATION.gender.equals("Female")) {
                first1000DaysBTN.setClickable(false);
                daysIMG.setVisibility(View.GONE);
                notAvailIMG.setVisibility(View.VISIBLE);

            }else {
                first1000DaysBTN.setVisibility(View.VISIBLE);
                daysIMG.setVisibility(View.VISIBLE);
                notAvailIMG.setVisibility(View.GONE);
            }

            if(!transformer.data.get(0).profilePicture.fullPath.equals("")) {
                Glide.with(getContext())
                    .load(transformer.data.get(0).profilePicture.fullPath)
                    .apply(new RequestOptions()
                    .placeholder(R.drawable.ic_user)
                    .error(R.drawable.ic_user))
                    .into(profileIMG);
            }
            else{
                Glide.with(getContext())
                    .load(R.drawable.ic_user)
                    .apply(new RequestOptions()
                    .placeholder(R.drawable.ic_user)
                    .error(R.drawable.ic_user))
                    .into(profileIMG);
            }
        }

    }

    @OnClick(R.id.clinicBTN)
    void clinicBTN(){
//     if(serviceOK){
//         activity.startBookingActivity("clinic", "");
//     }
        permission();
    }
    void permission(){
        Dexter.withActivity(activity)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        activity.startBookingActivity("clinic", "");
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        if(response.isPermanentlyDenied()){
                            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                            builder.setTitle("Permission Denied")
                                    .setMessage("Permission to access device location is permanently denied. you need to go to setting to allow the permission.")
                                    .setNegativeButton("Cancel", null)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Intent intent = new Intent();
                                            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                            intent.setData(Uri.fromParts("package", activity.getPackageName(), null));
                                        }
                                    })
                                    .show();
                        } else {
                            Toast.makeText(activity, "Permission Denied", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .check();
    }

    public boolean isServicesOK(){
        Log.d(TAG, "isServicesOK: checking google services version");

        int available = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(getContext());

        if(available == ConnectionResult.SUCCESS){
            //everything is fine and the user can make map requests
            Log.d(TAG, "isServicesOK: Google Play Services is working");
            return true;
        }
        else if(GoogleApiAvailability.getInstance().isUserResolvableError(available)){
            //an error occured but we can resolve it
            Log.d(TAG, "isServicesOK: an error occured but we can fix it");
            Dialog dialog = GoogleApiAvailability.getInstance().getErrorDialog(getActivity(), available, ERROR_DIALOG_REQUEST);
            dialog.show();
        }else{
            Toast.makeText(getContext(), "You can't make map requests", Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    @OnClick(R.id.practitionerBTN)
    void practitionerBTN(){
        activity.startPractitionerActivity("practitioner");
    }


    @OnClick(R.id.first1000DaysBTN)
    void first1000DaysBTN(){
        activity.startFirst1000DaysActivity("1stdays");
    }
    @OnClick(R.id.profileIMG)
    void settingsClicked() {
        activity.startProfileActivity("profiledetails");
    }

    @OnClick(R.id.homeServiceBTN)
    void homeServiceBTN(){
//        ToastMessage.show(getContext(), "Not available", ToastMessage.Status.FAILED);
        Toast.makeText(activity, "Not available", Toast.LENGTH_SHORT).show();
    }
    @OnClick(R.id.labBTN)
    void labBTN(){
//        ToastMessage.show(getContext(), "Not available", ToastMessage.Status.FAILED);
        Toast.makeText(activity, "Not available", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.covidBTN)
    void covidBTN(){
        activity.startPersonalInfoActivity("history_signs");
    }

    @OnClick(R.id.menuBTN)
    void menuBTN() {
        activity.startMainActivity("notif");
    }
}
