package com.highlysucceed.kaluapp.android.infectious_covid;

import android.content.Intent;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.activity.First1000DaysActivity;
import com.highlysucceed.kaluapp.android.activity.PersonalInfoActivity;
import com.highlysucceed.kaluapp.data.model.api.InfoModel;
import com.highlysucceed.kaluapp.data.model.api.OccupationModel;
import com.highlysucceed.kaluapp.data.preference.UserData;
import com.highlysucceed.kaluapp.server.request.Auth;
import com.highlysucceed.kaluapp.server.request.Clinic;
import com.highlysucceed.kaluapp.server.request.Infectious;
import com.highlysucceed.kaluapp.vendor.android.base.BaseFragment;
import com.highlysucceed.kaluapp.vendor.android.java.ToastMessage;
import com.highlysucceed.kaluapp.vendor.server.request.APIRequest;
import com.highlysucceed.kaluapp.vendor.server.transformer.BaseTransformer;
import com.highlysucceed.kaluapp.vendor.server.transformer.CollectionTransformer;
import com.highlysucceed.kaluapp.vendor.server.transformer.SingleTransformer;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class PersonalInfoFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {
    public static final String TAG = PersonalInfoFragment.class.getName().toString();

    private PersonalInfoActivity activity;

    @BindView(R.id.nameTXT)             TextView nameTXT;
//    @BindView(R.id.addressTXT)          TextView addressTXT;
    @BindView(R.id.emailTXT)            TextView emailTXT;
    @BindView(R.id.txtNumber)           TextView txtNumber;
    @BindView(R.id.ageTXT)              TextView ageTXT;
    @BindView(R.id.bdateTXT)            TextView bdateTXT;
    @BindView(R.id.genderTXT)           TextView genderTXT;
    @BindView(R.id.profileIMG)    ImageView profileIMG;
    @BindView(R.id.homeSRL)      SwipeRefreshLayout homeSRL;
    private APIRequest apiRequest;

    String strAge, strDateofBirth, strDayToday, strMonthToday, strYearToday;
    int age, yearToday, dayToday, monthToday, birthYear, birthMonth, birthDate;


    public static PersonalInfoFragment newInstance() {
        PersonalInfoFragment fragment = new PersonalInfoFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_personal_info;
    }

    @Override
    public void onViewReady() {
        activity = (PersonalInfoActivity) getContext();
        activity.setTitle("Personal Information");
        homeSRL.setOnRefreshListener(this);

        Calendar calendarToday = Calendar.getInstance();

        yearToday = calendarToday.get(Calendar.YEAR);
        dayToday = calendarToday.get(Calendar.DAY_OF_YEAR);
        monthToday = calendarToday.get(Calendar.MONTH);
    }

    @Override
    public void onResume() {
        super.onResume();
        onRefresh();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Infectious.LoginResponse response){
        CollectionTransformer<InfoModel> transformer = response.getData(CollectionTransformer.class);
        if(transformer.status){
            strDateofBirth = transformer.data.get(0).PERSONALINFORMATION.birthdate;
            nameTXT.setText(transformer.data.get(0).name);
//            addressTXT.setText(transformer.data.get(0).PERSONALINFORMATION.address);
            emailTXT.setText(transformer.data.get(0).PERSONALINFORMATION.email);
            txtNumber.setText(transformer.data.get(0).PERSONALINFORMATION.contactNumber);
            bdateTXT.setText(transformer.data.get(0).PERSONALINFORMATION.birthdate);
            genderTXT.setText(transformer.data.get(0).PERSONALINFORMATION.gender);
//            ageTXT.setText(transformer.data.get(0).PERSONALINFORMATION.age);

            birthYear = Integer.parseInt(strDateofBirth.substring(0,4));
            birthMonth = Integer.parseInt(strDateofBirth.substring(5,7));
            birthDate = Integer.parseInt(strDateofBirth.substring(8,10));

            age = yearToday - birthYear;
            int finalAge = 0;

             if (monthToday+1  == birthMonth) { //if month today is your birth month
                //we check the date
                 if (dayToday == birthDate) {//it's your birthday
                     finalAge = age-1;
                 }else if (dayToday > birthDate) { //past your birthday date
                     finalAge = age;
                 }else if (dayToday < birthDate){
                     finalAge = age;
                 }
             }else if (monthToday+1 < birthMonth){
                finalAge = age-1;
             }else if (monthToday+1 > birthMonth) {
                 finalAge = age;
             }else {
                 ageTXT.setText("");
             }

            ageTXT.setText(", "+finalAge+" y/o");


            if (!transformer.data.get(0).profilePicture.filename.equals("")){
                Picasso.with(getContext()).load(transformer.data.get(0).profilePicture.fullPath)
                    .error(R.drawable.ic_no_image)
                    .placeholder(R.drawable.ic_no_image)
                    .into(profileIMG);
            }  else {
                Picasso.with(getContext()).load(R.drawable.ic_no_image)
                    .error(R.drawable.ic_no_image)
                    .placeholder(R.drawable.ic_no_image)
                    .into(profileIMG);
            }
        }
        else {

        }
    }

    @Override
    public void onRefresh() {
        refreshList();
    }
    public void refreshList(){
        apiRequest = Infectious.getDefault().getProfileDetails(activity, String.valueOf(UserData.getUserModel().userId), homeSRL);
        apiRequest.first();
    }

    @OnClick(R.id.clinicalHistoryBTN)
    void clinicalHistoryBTN(){
        activity.openClinicHistoryFragment();
    }
    @OnClick(R.id.signsBTN)
    void signsBTN(){
        activity.openSignsFragment();
    }
    @OnClick(R.id.antigenBTN)
    void antigenBTN(){
        activity.openAntigenFragment();
    }
    @OnClick(R.id.credentialsBTN)
    void credentialsBTN(){
        activity.openCredentialsFragment();
    }

}
