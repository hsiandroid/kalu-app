package com.highlysucceed.kaluapp.android.fragment.practitioner;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.activity.PractitionerActivity;
import com.highlysucceed.kaluapp.android.adapter.ExpertiseListRecyclerViewAdapter;
import com.highlysucceed.kaluapp.android.adapter.PractitionerDetailsRecyclerViewAdapter;
import com.highlysucceed.kaluapp.data.model.api.OccupationModel;
import com.highlysucceed.kaluapp.data.model.api.PractitionerModel;
import com.highlysucceed.kaluapp.server.request.Practitioner;
import com.highlysucceed.kaluapp.vendor.android.base.BaseFragment;
import com.highlysucceed.kaluapp.vendor.server.transformer.CollectionTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class PractitionersFragment extends BaseFragment implements ExpertiseListRecyclerViewAdapter.ClickListener, PractitionerDetailsRecyclerViewAdapter.ClickListener {
    public static final String TAG = PractitionersFragment.class.getName().toString();

    @BindView(R.id.recyclerDoctorList)             RecyclerView recyclerDoctorList;
    @BindView(R.id.activityTitleTXT)               TextView activityTitleTXT;
    @BindView(R.id.searchBTN)                       ImageView searchBTN;


    private PractitionerDetailsRecyclerViewAdapter practitionerDetailsRecyclerViewAdapter;
    private LinearLayoutManager linearLayoutManager;
    private PractitionerActivity activity;
    String expertiseID;

    public static PractitionersFragment newInstance(String id) {
        PractitionersFragment fragment = new PractitionersFragment();
        fragment.expertiseID = id;
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_viewall_practitioner;
    }

    @Override
    public void onViewReady() {
         activity = (PractitionerActivity) getContext();
         activityTitleTXT.setText("Practitioner Details");
         setUpListView();
    }

    private void setUpListView() {
        practitionerDetailsRecyclerViewAdapter = new PractitionerDetailsRecyclerViewAdapter(getContext(), this);
        linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerDoctorList.setLayoutManager(linearLayoutManager);
        recyclerDoctorList.setAdapter(practitionerDetailsRecyclerViewAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        searchBTN.setVisibility(View.GONE);
        Practitioner.getDefault().getProviderLists(activity, expertiseID);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Practitioner.PractitionersList response){
        CollectionTransformer<PractitionerModel> transformer = response.getData(CollectionTransformer.class);
        if(transformer.status){
            if(response.isFirst()){
                practitionerDetailsRecyclerViewAdapter.addNewData(transformer.data);
            }
            else {
                practitionerDetailsRecyclerViewAdapter.setNewData(transformer.data);
            }
        }

    }


    @OnClick(R.id.activityBackBTN)
    void  activityBackBTN(){
        activity.onBackPressed();
    }

    @Override
    public void onClickExpertise(String id) {
        Practitioner.getDefault().getProviderLists(activity, id);
    }

    @Override
    public void onClickPractitioner(String id) {
        activity.openProviderDetails(id);
    }
}
