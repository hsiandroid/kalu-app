package com.highlysucceed.kaluapp.android.fragment.practitioner;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.activity.PractitionerActivity;
import com.highlysucceed.kaluapp.android.adapter.ClinicRecommendedRecyclerViewAdapter;
import com.highlysucceed.kaluapp.android.adapter.ExpertiseListRecyclerViewAdapter;
import com.highlysucceed.kaluapp.android.adapter.PractitionerDetailsRecyclerViewAdapter;
import com.highlysucceed.kaluapp.data.model.api.ClinicsModel;
import com.highlysucceed.kaluapp.data.model.api.OccupationModel;
import com.highlysucceed.kaluapp.data.model.api.PractitionerModel;
import com.highlysucceed.kaluapp.server.request.Practitioner;
import com.highlysucceed.kaluapp.vendor.android.base.BaseFragment;
import com.highlysucceed.kaluapp.vendor.android.java.ToastMessage;
import com.highlysucceed.kaluapp.vendor.server.request.APIRequest;
import com.highlysucceed.kaluapp.vendor.server.transformer.CollectionTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class PractitionerDetailsFragment extends BaseFragment implements PractitionerDetailsRecyclerViewAdapter.ClickListener, ExpertiseListRecyclerViewAdapter.ClickListener, SwipeRefreshLayout.OnRefreshListener {
    public static final String TAG = PractitionerDetailsFragment.class.getName().toString();

//    @BindView(R.id.recyclerExpertise)    RecyclerView recyclerExpertise;
    @BindView(R.id.recyclerClinic)         RecyclerView recyclerClinic;
    @BindView(R.id.recyclerDoctorList)     RecyclerView recyclerDoctorList;
    @BindView(R.id.recyclerSearchClinic)   RecyclerView recyclerSearchClinic;
    @BindView(R.id.titleheaderVIEW)        View titleheaderVIEW;
    @BindView(R.id.searchHeaderVIEW)       View searchHeaderVIEW;
    @BindView(R.id.mainLayout)             View mainLayout;
    @BindView(R.id.searchList)             View searchList;
    @BindView(R.id.noResponseVIEW)         View noResponseVIEW;
    @BindView(R.id.activityTitleTXT)       TextView activityTitleTXT;
    @BindView(R.id.btnVIEWALL)             TextView btnVIEWALL;
    @BindView(R.id.backButton)             ImageView backButton;
    @BindView(R.id.searchBTN)              ImageView searchBTN;
    @BindView(R.id.homeSRL)                SwipeRefreshLayout homeSRL;

    private ExpertiseListRecyclerViewAdapter expertiseListRecyclerViewAdapter;
    private ClinicRecommendedRecyclerViewAdapter clinicRecommendedRecyclerViewAdapter;
    private PractitionerDetailsRecyclerViewAdapter practitionerDetailsRecyclerViewAdapter;
    private LinearLayoutManager linearLayoutManager;
    private PractitionerActivity activity;
    private APIRequest apiRequest;
    String expertiseID;



    public static PractitionerDetailsFragment newInstance() {
        PractitionerDetailsFragment fragment = new PractitionerDetailsFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_practitioner_details;
    }

    @Override
    public void onViewReady() {
      activity = (PractitionerActivity) getContext();
      searchBTN.setVisibility(View.GONE);
      setUpListView();
      homeSRL.setOnRefreshListener(this);
    }

    private void setUpListView() {

        clinicRecommendedRecyclerViewAdapter = new ClinicRecommendedRecyclerViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(RecyclerView.HORIZONTAL);
        recyclerClinic.setLayoutManager(linearLayoutManager);
        recyclerClinic.setAdapter(clinicRecommendedRecyclerViewAdapter);

        practitionerDetailsRecyclerViewAdapter = new PractitionerDetailsRecyclerViewAdapter(getContext(), this);
        linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerDoctorList.setLayoutManager(linearLayoutManager);
        recyclerDoctorList.setAdapter(practitionerDetailsRecyclerViewAdapter);

        expertiseListRecyclerViewAdapter = new ExpertiseListRecyclerViewAdapter(getContext(), this);
        linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerDoctorList.setLayoutManager(linearLayoutManager);
        recyclerDoctorList.setAdapter(expertiseListRecyclerViewAdapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        activityTitleTXT.setText("Practitioner Details");
        onRefresh();
    }

    @Subscribe
    public void onResponse(Practitioner.OccupationList response){
        CollectionTransformer<OccupationModel> transformer = response.getData(CollectionTransformer.class);
        if(transformer.status){
            if(response.isFirst()){
                expertiseListRecyclerViewAdapter.addNewData(transformer.data);
            }
            else {
                expertiseListRecyclerViewAdapter.addNewData(transformer.data);
            }
        }
    }

    @Subscribe
    public void onResponse(Practitioner.ClinicList response){
        CollectionTransformer<ClinicsModel> transformer = response.getData(CollectionTransformer.class);
        if(transformer.status){
            if(response.isFirst()){
               
                clinicRecommendedRecyclerViewAdapter.addNewData(transformer.data);
            }
            else {
                clinicRecommendedRecyclerViewAdapter.setNewData(transformer.data);
            }
        }
    }
    @Subscribe
    public void onResponse(Practitioner.PractitionerList response){
        CollectionTransformer<PractitionerModel> transformer = response.getData(CollectionTransformer.class);
        if(transformer.status){
//            btnVIEWALL.setClickable(true);
//            btnVIEWALL.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
            if(response.isFirst()){
                noResponseVIEW.setVisibility(View.GONE);
                recyclerDoctorList.setVisibility(View.VISIBLE);
                practitionerDetailsRecyclerViewAdapter.addNewData(transformer.data);
            }
            else {
                noResponseVIEW.setVisibility(View.GONE);
                recyclerDoctorList.setVisibility(View.VISIBLE);
                practitionerDetailsRecyclerViewAdapter.setNewData(transformer.data);
            }
        }
    }
    @Subscribe
    public void onResponse(Practitioner.PractitionersList response){
        CollectionTransformer<PractitionerModel> transformer = response.getData(CollectionTransformer.class);
        if(transformer.status){
            activity.openPractitionersFragment(expertiseID);
        }
        else{
         ToastMessage.show(getContext(), "No Practitioner Available", ToastMessage.Status.FAILED);
        }
    }



    @Override
    public void onClickPractitioner(String id) {
        activity.openProviderDetails(id);
    }


    @OnClick(R.id.btnVIEWALL)
    void btnVIEWALL(){
            activity.openViewAllPractitionerDetails();
    }

    @OnClick(R.id.activityBackBTN)
    void  activityBackBTN(){
        activity.onBackPressed();
    }

    @Override
    public void onClickExpertise(String id) {
        expertiseID = id;
        Practitioner.getDefault().getProviderLists(activity, id);
    }

    @Override
    public void onRefresh() {
        refreshList();
    }
    public void refreshList(){
        apiRequest =  Practitioner.getDefault().getClinicList(activity, homeSRL);
        apiRequest.first();
        Practitioner.getDefault().getOccupationList(activity);
    }
}
