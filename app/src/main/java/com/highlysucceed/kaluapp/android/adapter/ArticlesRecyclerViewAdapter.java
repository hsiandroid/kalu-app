package com.highlysucceed.kaluapp.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.data.model.api.ArticlesModel;
import com.highlysucceed.kaluapp.vendor.android.base.BaseRecylerViewAdapter;
import com.highlysucceed.kaluapp.vendor.android.java.Log;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ArticlesRecyclerViewAdapter extends BaseRecylerViewAdapter<ArticlesRecyclerViewAdapter.ViewHolder, ArticlesModel> {

    private ClickListener clickListener;

    public ArticlesRecyclerViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_articles));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.titleTXT.setText(holder.getItem().title);
        holder.likeCountTXT.setText(holder.getItem().like + " likes");
        holder.shareCountTXT.setText(holder.getItem().share + " shares");
    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.likeCountTXT)    TextView likeCountTXT;
        @BindView(R.id.shareCountTXT)   TextView shareCountTXT;
        @BindView(R.id.titleTXT)        TextView titleTXT;
        @BindView(R.id.likeBTN)         View likeBTN;
        @BindView(R.id.shareBTN)        View shareBTN;
        @BindView(R.id.imageIV)         ImageView imageIV;
        @BindView(R.id.adapterCON)      View adapterCON;

        public ViewHolder(View view) {
            super(view);
        }

        public ArticlesModel getItem() {
            return (ArticlesModel) super.getItem();
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener{
        void onItemClick(ArticlesModel testKitModel);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.adapterCON:
                if (clickListener != null){
                    clickListener.onItemClick((ArticlesModel) v.getTag());
                }
                break;
        }
    }


} 
