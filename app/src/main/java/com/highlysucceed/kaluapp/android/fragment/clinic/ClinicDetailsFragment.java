package com.highlysucceed.kaluapp.android.fragment.clinic;

import android.annotation.SuppressLint;
import android.content.res.ColorStateList;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.activity.BookingActivity;
import com.highlysucceed.kaluapp.android.adapter.DoctorDetailsRecyclerViewAdapter;
import com.highlysucceed.kaluapp.android.adapter.DoctorListRecyclerViewAdapter;
import com.highlysucceed.kaluapp.android.adapter.FeaturedPhotoAdapter;
import com.highlysucceed.kaluapp.android.dialog.DoctorDialog;
import com.highlysucceed.kaluapp.android.dialog.FeaturedPhotoDialog;
import com.highlysucceed.kaluapp.data.model.api.ClinicDetailsModel;
import com.highlysucceed.kaluapp.data.model.api.FeturedPhotoModel;
import com.highlysucceed.kaluapp.data.model.api.ProviderModel;
import com.highlysucceed.kaluapp.server.request.Clinic;
import com.highlysucceed.kaluapp.vendor.android.base.BaseFragment;
import com.highlysucceed.kaluapp.vendor.server.transformer.CollectionTransformer;
import com.highlysucceed.kaluapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ClinicDetailsFragment extends BaseFragment implements DoctorDialog.Callback, View.OnClickListener, DoctorDetailsRecyclerViewAdapter.DoctorListener, DoctorListRecyclerViewAdapter.ClinicListener, SwipeRefreshLayout.OnRefreshListener {
    public static final String TAG = ClinicDetailsFragment.class.getName().toString();


    @BindView(R.id.recycleDoctors)              RecyclerView recycleDoctors;
    @BindView(R.id.recycleSearchDoctors)        RecyclerView recycleSearchDoctors;
    @BindView(R.id.titleheaderVIEW)             View titleheaderVIEW;
    @BindView(R.id.searchHeaderVIEW)            View searchHeaderVIEW;
    @BindView(R.id.mainLayout)                  View mainLayout;
    @BindView(R.id.searchList)                  View searchList;
    @BindView(R.id.activityTitleTXT)            TextView activityTitleTXT;
    @BindView(R.id.backButton)                  ImageView backButton;
    @BindView(R.id.clinicTITLE)                 TextView clinicTITLE;
    @BindView(R.id.addressTXT)                  TextView addressTXT;
    @BindView(R.id.emailTXT)                    TextView emailTXT;
    @BindView(R.id.contactTXT)                  TextView contactTXT;
    @BindView(R.id.aboutTXT)                    TextView aboutTXT;
    @BindView(R.id.txtTimein)                   TextView txtTimein;
    @BindView(R.id.txtTimeout)                  TextView txtTimeout;
    @BindView(R.id.nodataTXT)                   TextView nodataTXT;
    @BindView(R.id.photoViewBTN)                TextView photoViewBTN;
    @BindView(R.id.doctorsViewBTN)              TextView doctorsViewBTN;
    @BindView(R.id.coverPhotoIV)                ImageView coverPhotoIV;
    @BindView(R.id.logoIV)                      ImageView logoIV;
    @BindView(R.id.removeBTN)                   ImageView removeBTN;
    @BindView(R.id.gridIMG)                     GridView gridIMG;
    @BindView(R.id.searchEDT)                   EditText searchEDT;
    @BindView(R.id.homeSRL)                     SwipeRefreshLayout homeSRL;
    @BindView(R.id.searchSRL)                   SwipeRefreshLayout searchSRL;


    DoctorListRecyclerViewAdapter doctorListRecyclerViewAdapter;
    DoctorDetailsRecyclerViewAdapter detailsRecyclerViewAdapter;
    LinearLayoutManager linearLayoutManager;
    private FeaturedPhotoAdapter featuredPhotoAdapter;
    private BookingActivity activity;
    private List<FeturedPhotoModel> feturedPhotoModelList = new ArrayList<>();
    private List<ProviderModel> providerModelList = new ArrayList<>();
    String clinicId, recyclerLoadType = "";
    ColorStateList defaultColor;
    private Handler handler;
    private Runnable runnable;

    public static ClinicDetailsFragment newInstance(String id) {
        ClinicDetailsFragment fragment = new ClinicDetailsFragment();
        fragment.clinicId = id;
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_clinic_details;
    }

    @Override
    public void onViewReady() {
        activity = (BookingActivity) getContext();
        populateData();
        populateSearchList();
        setClickListener();
        homeSRL.setOnRefreshListener(this);
        searchSRL.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                 refreshSearchList();
            }
        });
    }

    private void setClickListener() {
        photoViewBTN.setOnClickListener(this);
        doctorsViewBTN.setOnClickListener(this);
    }

    private void populateData() {
        detailsRecyclerViewAdapter = new DoctorDetailsRecyclerViewAdapter(activity, this);
        linearLayoutManager = new LinearLayoutManager(getContext());
        recycleDoctors.setLayoutManager(linearLayoutManager);
        recycleDoctors.setAdapter(detailsRecyclerViewAdapter);
    }

    private void populateSearchList() {
        doctorListRecyclerViewAdapter = new DoctorListRecyclerViewAdapter(activity, this);
        linearLayoutManager = new LinearLayoutManager(getContext());
        recycleSearchDoctors.setLayoutManager(linearLayoutManager);
        recycleSearchDoctors.setAdapter(doctorListRecyclerViewAdapter);
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        activityTitleTXT.setText("Clinic Details");
        onRefresh();
    }

    @Subscribe
    public void onResponse(Clinic.ClinicDetailsReponse response) {
        SingleTransformer<ClinicDetailsModel> transformer = response.getData(SingleTransformer.class);
        if (transformer.status) {
            clinicTITLE.setText(transformer.data.name);
            addressTXT.setText(transformer.data.address);
            emailTXT.setText(transformer.data.email);
            contactTXT.setText(transformer.data.contactNumber);
            aboutTXT.setText(transformer.data.aboutUs);
            txtTimein.setText(transformer.data.clinicStartTime + " -");
            txtTimeout.setText(transformer.data.clinicEndTime);
            featuredPhotoAdapter = new FeaturedPhotoAdapter(activity, transformer.data.featuredPhoto.data);
            gridIMG.setAdapter(featuredPhotoAdapter);

            if (transformer.data.featuredPhoto.data.size() == 0) {
                photoViewBTN.setEnabled(false);
                photoViewBTN.setTextColor(defaultColor);
            } else {
                photoViewBTN.setEnabled(true);
                photoViewBTN.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
                for (FeturedPhotoModel feturedPhotoModel : transformer.data.featuredPhoto.data) {
                    feturedPhotoModelList.add(feturedPhotoModel);
                }
            }
            if (transformer.data.provider.data.size() == 0) {
                doctorsViewBTN.setEnabled(false);
                doctorsViewBTN.setTextColor(defaultColor);
            } else {
                doctorsViewBTN.setEnabled(true);
                doctorsViewBTN.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
            }

            if (response.isNext()) {
                detailsRecyclerViewAdapter.addNewData(transformer.data.provider.data);
                addNewData(transformer.data.provider.data);
            } else {
                detailsRecyclerViewAdapter.setNewData(transformer.data.provider.data);
                setNewData(transformer.data.provider.data);

            }

        }
    }

    @Subscribe
    public void onResponse(Clinic.DoctorListResponse response) {
        CollectionTransformer<ProviderModel> transformer = response.getData(CollectionTransformer.class);
        if (transformer.status) {
             if(response.isNext()){
                 recycleSearchDoctors.setVisibility(View.VISIBLE);
                 doctorListRecyclerViewAdapter.addNewData(transformer.data);
             }
             else{
                 recycleSearchDoctors.setVisibility(View.VISIBLE);
                 doctorListRecyclerViewAdapter.setNewData(transformer.data);
             }

        }
    }

    private void addNewData(List<ProviderModel> data) {
        for (ProviderModel providerModel : data) {
            providerModelList.add(providerModel);
        }
        recyclerLoadType = "newData";
    }

    private void setNewData(List<ProviderModel> data) {
        for (ProviderModel providerModel : data) {
            providerModelList.add(providerModel);
        }
        recyclerLoadType = "setData";

    }

    @OnClick(R.id.searchBTN)
    void searchBTN() {
        titleheaderVIEW.setVisibility(View.GONE);
        homeSRL.setVisibility(View.GONE);
        searchHeaderVIEW.setVisibility(View.VISIBLE);
        searchList.setVisibility(View.VISIBLE);
        refreshSearchList();
        textSearch();
    }

    @OnClick(R.id.backButton)
    void backButton() {
        activity.closeKeyboard();
        searchHeaderVIEW.setVisibility(View.GONE);
        homeSRL.setVisibility(View.VISIBLE);
        titleheaderVIEW.setVisibility(View.VISIBLE);
        searchList.setVisibility(View.GONE);
    }

    @OnClick(R.id.activityBackBTN)
    void activityBackBTN() {
        activity.onBackPressed();
    }

    @Override
    public void onRefresh() {
        refreshList();
    }
    public void refreshList() {
        Clinic.getDefault().getClinicDetails(activity, clinicId, homeSRL);
    }
    public void refreshSearchList(){
        Clinic.getDefault().getDoctorsList(activity, "", searchSRL);
    }


    @SuppressLint("ResourceAsColor")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.photoViewBTN:
                FeaturedPhotoDialog.newInstance(getContext(), activity, feturedPhotoModelList).show(getChildFragmentManager(), TAG);
                break;
            case R.id.doctorsViewBTN:
                DoctorDialog.newInstance(getContext(), this, activity, providerModelList, recyclerLoadType).show(getChildFragmentManager(), TAG);
                break;
        }
    }


    @Override
    public void doctorId(String id) {
        activity.openDoctorDetails(clinicId, id);
    }

    @Override
    public void providerDetails(String id) {
        activity.openDoctorDetails(clinicId, id);
    }

    @Override
    public void providerID(String id) {
        activity.openDoctorDetails(clinicId, id);
    }


    private void textSearch() {
        searchEDT.requestFocus();
        searchEDT.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.toString().isEmpty()) {
                    runnable = new Runnable() {
                        @Override
                        public void run() {
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    recycleSearchDoctors.setVisibility(View.GONE);
                                    removeBTN.setVisibility(View.GONE);
                                    Clinic.getDefault().getDoctorsList(activity, editable.toString(), searchSRL);
                                }
                            });
                        }
                    };
                    handler = new Handler();
                    handler.postDelayed(runnable, 500);

                }
                else{
                    runnable = new Runnable() {
                        @Override
                        public void run() {
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    recycleSearchDoctors.setVisibility(View.GONE);
                                    removeBTN.setVisibility(View.VISIBLE);
                                    removeBTN.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            searchEDT.getText().clear();
                                        }
                                    });
                                    Clinic.getDefault().getDoctorsList(activity, editable.toString(), searchSRL);
                                }
                            });
                        }
                    };
                    handler = new Handler();
                    handler.postDelayed(runnable, 500);

                }

            }

        });
    }


}
