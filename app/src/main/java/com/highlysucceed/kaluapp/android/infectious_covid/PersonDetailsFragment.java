package com.highlysucceed.kaluapp.android.infectious_covid;

import android.Manifest;
import android.app.Person;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.activity.PersonalInfoActivity;
import com.highlysucceed.kaluapp.server.request.Auth;
import com.highlysucceed.kaluapp.vendor.android.base.BaseFragment;
import com.highlysucceed.kaluapp.vendor.server.transformer.BaseTransformer;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class PersonDetailsFragment extends BaseFragment implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    public static final String TAG = PersonDetailsFragment.class.getName().toString();

    PersonalInfoActivity activity = new PersonalInfoActivity();

    @BindView(R.id.spinnerBooker)           Spinner spinnerBooker;
    @BindView(R.id.spinnerServiceType)      Spinner spinnerServiceType;
    @BindView(R.id.spinnerTestType)         Spinner spinnerTestType;
    @BindView(R.id.spinnerVaccineType)      Spinner spinnerVaccineType;
    @BindView(R.id.companyET)               EditText companyET;
    @BindView(R.id.companyEmailET)          EditText companyEmailET;
    @BindView(R.id.companyContactET)        EditText companyContactET;
    @BindView(R.id.companyAddrET)           EditText companyAddrET;
    @BindView(R.id.philHealthET)            EditText philHealthET;
    @BindView(R.id.continueBTN)             Button continueBTN;
    @BindView(R.id.companyVIEw)             View companyVIEw;
    @BindView(R.id.testTXT)                 TextView testTXT;
    @BindView(R.id.testVIEW)                View testVIEW;
    @BindView(R.id.vaccineTXT)              TextView vaccineTXT;
    @BindView(R.id.vaccineVIEW)             View vaccineVIEW;
    @BindView(R.id.philHealthTXT)           TextView philHealthTXT;

    String selectedBooker, selectedServiceType, selectedTestType, selectedVaccineType;

    public static PersonDetailsFragment newInstance() {
        PersonDetailsFragment fragment = new PersonDetailsFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_person_details;
    }

    @Override
    public void onViewReady() {
        activity = (PersonalInfoActivity) getContext();
        activity.setTitle("Book an Appointment");
        continueBTN.setOnClickListener(this);
        spinnerBooker.setOnItemSelectedListener(this);
        spinnerServiceType.setOnItemSelectedListener(this);
        spinnerTestType.setOnItemSelectedListener(this);
        spinnerVaccineType.setOnItemSelectedListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.continueBTN:
                if (!selectedBooker.equals("For Me")) { //proceed to company/employee details
                    activity.openCompanyFragment();
                }else { //proceed to booking process
                    openPermission();
                }
                break;
        }
    }

    void openPermission(){
        Dexter.withActivity(activity)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        activity.startBookingActivity("clinic", "");
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        if(response.isPermanentlyDenied()){
                            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                            builder.setTitle("Permission Denied")
                                    .setMessage("Permission to access device location is permanently denied. you need to go to setting to allow the permission.")
                                    .setNegativeButton("Cancel", null)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Intent intent = new Intent();
                                            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                            intent.setData(Uri.fromParts("package", activity.getPackageName(), null));
                                        }
                                    })
                                    .show();
                        } else {
                            Toast.makeText(activity, "Permission Denied", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .check();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.spinnerBooker:
                selectedBooker = parent.getItemAtPosition(position).toString();
                if (parent.getItemAtPosition(position).equals("For Me")) {
                    companyVIEw.setVisibility(View.GONE);
                    philHealthTXT.setVisibility(View.VISIBLE);
                    philHealthET.setVisibility(View.VISIBLE);
                }else {
                    companyVIEw.setVisibility(View.VISIBLE);
                    philHealthTXT.setVisibility(View.GONE);
                    philHealthET.setVisibility(View.GONE);
                }
                break;
            case R.id.spinnerServiceType:
                if (parent.getItemAtPosition(position).equals("Test")){
                        testTXT.setVisibility(View.VISIBLE);
                        testVIEW.setVisibility(View.VISIBLE);
                        vaccineTXT.setVisibility(View.GONE);
                        vaccineVIEW.setVisibility(View.GONE);
                }else {
                    testTXT.setVisibility(View.GONE);
                    testVIEW.setVisibility(View.GONE);
                    vaccineTXT.setVisibility(View.VISIBLE);
                    vaccineVIEW.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
