package com.highlysucceed.kaluapp.android.fragment;

import android.util.Log;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.server.request.Auth;
import com.highlysucceed.kaluapp.vendor.android.base.BaseFragment;
import com.highlysucceed.kaluapp.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class DefaultFragment extends BaseFragment {
    public static final String TAG = DefaultFragment.class.getName().toString();

    public static DefaultFragment newInstance() {
        DefaultFragment fragment = new DefaultFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_coming_soon;
    }

    @Override
    public void onViewReady() {

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }
}
