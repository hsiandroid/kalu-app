package com.highlysucceed.kaluapp.android.calendar;

import android.content.Context;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class CalendarWeeks {
    private Calendar calendar;
    private Context context;

    public CalendarWeeks (Context mCtx){
       this.context = mCtx;
    }

    public String[] getCurrentWeek() {
        this.calendar = Calendar.getInstance();
        this.calendar.setFirstDayOfWeek(Calendar.SUNDAY);
        this.calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        return getNextWeek();
    }
    public String[] getNextWeek() {
        SimpleDateFormat format = new SimpleDateFormat("d-MMM-yyyy");
        String[] days = new String[7];
        for (int i = 0; i < 7; i++) {
            days[i] = format.format(this.calendar.getTime());
            this.calendar.add(Calendar.DATE, 1);
        }
        return days;
    }
    public String[] getPreviousWeek() {
        this.calendar.add(Calendar.DATE, -14);
        return getNextWeek();
    }

}
