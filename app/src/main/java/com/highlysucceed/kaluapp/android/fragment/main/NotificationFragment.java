package com.highlysucceed.kaluapp.android.fragment.main;

import android.util.Log;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.activity.MainActivity;
import com.highlysucceed.kaluapp.android.adapter.NotificationRecyclerViewAdapter;
import com.highlysucceed.kaluapp.data.model.api.NotifModel;
import com.highlysucceed.kaluapp.server.request.Auth;
import com.highlysucceed.kaluapp.vendor.android.base.BaseFragment;
import com.highlysucceed.kaluapp.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.highlysucceed.kaluapp.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class NotificationFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener, EndlessRecyclerViewScrollListener.Callback {
    public static final String TAG = NotificationFragment.class.getName().toString();

    private LinearLayoutManager linearLayoutManager;
    private NotificationRecyclerViewAdapter notificationRecyclerViewAdapter;
    private MainActivity mainActivity;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;

    @BindView(R.id.notifRV)                 RecyclerView notifRV;
    @BindView(R.id.notifSRL)                SwipeRefreshLayout notifSRL;
    @BindView(R.id.placeholderTXT)          TextView placeholderTXT;


    public static NotificationFragment newInstance() {
        NotificationFragment fragment = new NotificationFragment();
        return fragment;
    }


    @Override
    public int onLayoutSet() {
        return R.layout.fragment_notification;
    }

    @Override
    public void onViewReady() {
        mainActivity = (MainActivity) getContext();
        mainActivity.setTitle("Notification");
        setUpListView();
    }

    private void setUpListView() {
        notificationRecyclerViewAdapter = new NotificationRecyclerViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        notifSRL.setOnRefreshListener(this);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, this);
        notifRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        notifRV.setLayoutManager(linearLayoutManager);
        notifRV.setAdapter(notificationRecyclerViewAdapter);
        notificationRecyclerViewAdapter.setNewData(dummyData());
    }

    private List<NotifModel> dummyData(){
        List<NotifModel> notifModels = new ArrayList<>();

        for (int i = 0; i<=8; i ++){
            NotifModel notifModel = new NotifModel();
            notifModel.id = i + 1;
            notifModel.title = "Lorem ipsum";
            notifModel.description = "this is a dummy data";
            notifModels.add(notifModel);
        }

        return notifModels;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }

    @Override
    public void onRefresh() {
        notificationRecyclerViewAdapter.setNewData(dummyData());
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        notificationRecyclerViewAdapter.setNewData(dummyData());

    }
}
