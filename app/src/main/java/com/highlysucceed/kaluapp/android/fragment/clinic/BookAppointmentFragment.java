package com.highlysucceed.kaluapp.android.fragment.clinic;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CalendarView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.activity.BookingActivity;
import com.highlysucceed.kaluapp.android.adapter.OfficeHoursAdapter;
import com.highlysucceed.kaluapp.android.dialog.ConfirmDialog;
import com.highlysucceed.kaluapp.android.dialog.HMODialog;
import com.highlysucceed.kaluapp.android.dialog.NotesDialog;
import com.highlysucceed.kaluapp.android.dialog.ServiceDialog;
import com.highlysucceed.kaluapp.config.Keys;
import com.highlysucceed.kaluapp.data.model.api.ScheduleModel;
import com.highlysucceed.kaluapp.data.model.api.ServiceModel;
import com.highlysucceed.kaluapp.data.model.api.SlotModel;
import com.highlysucceed.kaluapp.data.preference.UserData;
import com.highlysucceed.kaluapp.server.request.Clinic;
import com.highlysucceed.kaluapp.vendor.android.base.BaseFragment;
import com.highlysucceed.kaluapp.vendor.android.java.Log;
import com.highlysucceed.kaluapp.vendor.android.java.ToastMessage;
import com.highlysucceed.kaluapp.vendor.server.request.APIRequest;
import com.highlysucceed.kaluapp.vendor.server.transformer.BaseTransformer;
import com.highlysucceed.kaluapp.vendor.server.transformer.CollectionTransformer;
import com.highlysucceed.kaluapp.vendor.server.transformer.SingleTransformer;
import com.highlysucceed.kaluapp.vendor.server.util.ErrorResponseManger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import dk.nodes.filepicker.FilePickerActivity;
import dk.nodes.filepicker.FilePickerConstants;
import dk.nodes.filepicker.uriHelper.FilePickerUriHelper;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class BookAppointmentFragment extends BaseFragment implements ServiceDialog.Callback, View.OnClickListener, ConfirmDialog.Callback, HMODialog.Callback, NotesDialog.Callback {
    public static final String TAG = BookAppointmentFragment.class.getName().toString();
    private BookingActivity activity;
    private OfficeHoursAdapter officeHoursAdapter;
    private List<ServiceModel> serviceModelList = new ArrayList<>();

    @BindView(R.id.gridHR)              GridView gridHR;
    @BindView(R.id.btnServices)         View btnServices;
    @BindView(R.id.btnNOTES)            View btnNOTES;
    @BindView(R.id.selectedServiceTXT)  TextView selectedServiceTXT;
    @BindView(R.id.pathTXT)             TextView pathTXT;
    @BindView(R.id.btnHMO)              View btnHMO;
    @BindView(R.id.activityTitleTXT)    TextView activityTitleTXT;
    @BindView(R.id.txtNODATA)           TextView txtNODATA;
    @BindView(R.id.notesTXT)            TextView notesTXT;
    @BindView(R.id.HMOTXT)              TextView HMOTXT;
    @BindView(R.id.searchBTN)           ImageView searchBTN;
    @BindView(R.id.calendarView)        CalendarView calendarView;
    @BindView(R.id.progress_bar)        ProgressBar progress_bar;

    String clinic_ID, provider_ID, selectedServiceID;
    String currentDate, selectedHours;
    String dateFormat = "", serviceType="";
    String details="", notes="", medicard_number="", medicard_reference, medicard_expiry;
    private static final int REQ_CODE = 101;
    File file;
    int hrBackposition = -1;


    public static BookAppointmentFragment newInstance(String clinicID, String providerID) {
        BookAppointmentFragment fragment = new BookAppointmentFragment();
        fragment.clinic_ID = clinicID;
        fragment.provider_ID = providerID;
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_book_appointment;
    }


    @SuppressLint("ResourceAsColor")
    @Override
    public void onViewReady() {
        activity = (BookingActivity) getContext();
        btnServices.setOnClickListener(this);
        btnHMO.setOnClickListener(this);
        btnNOTES.setOnClickListener(this);
        setInitializeCalendar();

    }

    private void setInitializeCalendar() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, Calendar.getInstance().getActualMinimum(Calendar.HOUR_OF_DAY));
        long date = calendar.getTime().getTime();
        calendarView.setMinDate(date);
        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                dateFormat = year + "-" + (month + 1) + "-" + dayOfMonth;
                txtNODATA.setVisibility(View.GONE);
                gridHR.setVisibility(View.GONE);
                progress_bar.setVisibility(View.VISIBLE);
                Clinic.getDefault().getProviderSchedule(activity, provider_ID, dateFormat);
            }
        });
    }



    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        activityTitleTXT.setText("Book Appointment");
        searchBTN.setVisibility(View.GONE);
        gridHR.setVisibility(View.GONE);
        progress_bar.setVisibility(View.VISIBLE);
        txtNODATA.setVisibility(View.GONE);
        getCurrentDate();
        Clinic.getDefault().getServiceList(getContext(), provider_ID);
    }

    @Subscribe
    public void onResponse(Clinic.ServiceResponse serviceResponse) {
        CollectionTransformer<ServiceModel> transformer = serviceResponse.getData(CollectionTransformer.class);
        if (transformer.status) {
            for(int i=0; i<transformer.data.size(); i++){
                ServiceModel model = new ServiceModel();
                model.id = transformer.data.get(i).id;
                model.name = transformer.data.get(i).name;
                serviceModelList.add(model);
            }

            if(serviceResponse.isNext()){
                setData(serviceModelList, "new");
            }
            else {
                setData(serviceModelList, "refresh");
            }
        }
        else {
            ToastMessage.show(getContext(), transformer.msg, ToastMessage.Status.FAILED);
        }
    }


    @Subscribe
    public void onResponse(Clinic.BookingResponse bookingResponse) {
        BaseTransformer transformer = bookingResponse.getData(BaseTransformer.class);
        if (transformer.status) {
            ToastMessage.show(activity, transformer.msg, ToastMessage.Status.SUCCESS);
            activity.startMainActivity("home");
        }
        else{
            if (!ErrorResponseManger.first(transformer.error.date).equals("")){
                ToastMessage.show(activity, ErrorResponseManger.first(transformer.error.date), ToastMessage.Status.FAILED);

            }
            else {
                ToastMessage.show(activity, transformer.msg, ToastMessage.Status.FAILED);
            }

        }
    }

    @Subscribe
    public void onResponse(Clinic.SlotResponse response) {
        CollectionTransformer<SlotModel> transformer = response.getData(CollectionTransformer.class);
        if(transformer.status){
            gridHR.setVisibility(View.VISIBLE);
            progress_bar.setVisibility(View.GONE);
            txtNODATA.setVisibility(View.GONE);
            officeHoursAdapter = new OfficeHoursAdapter(activity, transformer.data);
            gridHR.setAdapter(officeHoursAdapter);
            gridHR.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    TextView hrTV = (TextView) view;
                    TextView backSelected;
                    hrTV.setBackgroundResource(R.drawable.bg_blue_rounded_corners);
                    hrTV.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
                    selectedHours = hrTV.getText().toString();
                    backSelected = (TextView) gridHR.getChildAt(hrBackposition);
                    if (hrBackposition != -1) {
                        backSelected.setSelected(false);
                        backSelected.setBackgroundResource(0);
                        backSelected.setBackgroundResource(R.drawable.bg_gray_rounded_corners);
                        backSelected.setTextColor(ContextCompat.getColor(activity, R.color.dark_gray));
                    }
                    hrBackposition = position;
                }
            });
        }
        else {
            gridHR.setVisibility(View.GONE);
            progress_bar.setVisibility(View.GONE);
            txtNODATA.setVisibility(View.VISIBLE);
        }

    }


    private void getCurrentDate() {
        currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
        Clinic.getDefault().getProviderSchedule(activity, provider_ID, currentDate);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnServices:
             ServiceDialog.newInstance(activity, this, serviceModelList, details).show(getFragmentManager(), TAG);
                break;
            case R.id.btnHMO:
                HMODialog.newInstance(activity, this).show(getChildFragmentManager(), TAG);
                break;
            case R.id.btnNOTES:
                NotesDialog.newInstance(activity, this).show(getChildFragmentManager(), TAG);
                break;
        }
    }

    @Override
    public void onServiceID(String selectedID, String serviceName) {
                 selectedServiceID = selectedID;
                 serviceType = serviceName;
                 if(!serviceType.isEmpty()){
                     btnServices.setVisibility(View.GONE);
                     selectedServiceTXT.setVisibility(View.VISIBLE);
                     selectedServiceTXT.setText(serviceType);
                 }
    }

    @OnClick(R.id.uploadBTN)
    void uploadBTN(){
       openFilePicker(REQ_CODE);
    }
    private void openFilePicker(int req_code){
        Intent intent = new Intent(getContext(), FilePickerActivity.class);
        intent.putExtra(FilePickerConstants.FILE, true);
        intent.putExtra(FilePickerConstants.TYPE,   FilePickerConstants.MIME_PDF);
        startActivityForResult(intent, req_code);
    }
    @Override
    public  void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                file = FilePickerUriHelper.getFile(getContext(), data);
                pathTXT.setText(file.getName());
            }
        }
    }

    @Override
    public void onConfirmSuccess() {
        APIRequest apiRequest = Clinic.getDefault().bookAppointment(activity)
                .addParameter(Keys.USER_ID, UserData.getUserModel().userId)
                .addParameter(Keys.PROVIDER_ID, provider_ID)
                .addParameter(Keys.SERVICE_ID, selectedServiceID)
                .addParameter(Keys.CLINIC_ID, clinic_ID)
                .addParameter(Keys.DATE, dateFormat)
                .addParameter(Keys.START_TIME, selectedHours)
                .addParameter(Keys.APPOINTMENT_TYPE, "online")
                .addParameter(Keys.NOTE, notes)
                .addParameter(Keys.MEDICARD_NUMBER, medicard_number)
                .addParameter(Keys.MEDICARD_REFERENCE, medicard_reference)
                .addParameter(Keys.MEDICARD_EXPIRY, medicard_expiry)
                .addParameter(Keys.FILE, file);
        apiRequest.execute();
    }
    @OnClick(R.id.confirmBTN)
    void confirmBTN(){
        ConfirmDialog.newInstance(activity, this, dateFormat, serviceType, selectedHours ).show(getFragmentManager(), TAG);
    }

    @OnClick(R.id.activityBackBTN)
    void activityBackBTN(){ activity.onBackPressed(); }

    private void setData(List<ServiceModel> serviceModelList, String type) {
                this.serviceModelList = serviceModelList;
                this.details = type;
    }

    @Override
    public void outputHMO(boolean hasValue) {
     if(hasValue){
         btnHMO.setVisibility(View.GONE);
         HMOTXT.setVisibility(View.VISIBLE);
     }
    }

    @Override
    public void notesResult(boolean hasValue) {
        if(hasValue){
            btnNOTES.setVisibility(View.GONE);
            notesTXT.setVisibility(View.VISIBLE);
        }
    }
}
