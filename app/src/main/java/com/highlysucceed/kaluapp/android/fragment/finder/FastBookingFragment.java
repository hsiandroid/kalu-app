package com.highlysucceed.kaluapp.android.fragment.finder;

import android.annotation.SuppressLint;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CalendarView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.activity.BookingActivity;
import com.highlysucceed.kaluapp.android.activity.FinderActivity;
import com.highlysucceed.kaluapp.android.adapter.CalendarWeekAdapter;
import com.highlysucceed.kaluapp.android.adapter.OfficeHoursAdapter;
import com.highlysucceed.kaluapp.android.calendar.CalendarWeeks;
import com.highlysucceed.kaluapp.android.dialog.ConfirmDialog;
import com.highlysucceed.kaluapp.android.dialog.ServiceDialog;
import com.highlysucceed.kaluapp.config.Keys;
import com.highlysucceed.kaluapp.data.model.api.ScheduleModel;
import com.highlysucceed.kaluapp.data.model.api.ServiceModel;
import com.highlysucceed.kaluapp.data.preference.UserData;
import com.highlysucceed.kaluapp.server.request.Clinic;
import com.highlysucceed.kaluapp.vendor.android.base.BaseFragment;
import com.highlysucceed.kaluapp.vendor.android.java.ToastMessage;
import com.highlysucceed.kaluapp.vendor.server.request.APIRequest;
import com.highlysucceed.kaluapp.vendor.server.transformer.BaseTransformer;
import com.highlysucceed.kaluapp.vendor.server.transformer.CollectionTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class FastBookingFragment extends BaseFragment implements ServiceDialog.Callback, View.OnClickListener, ConfirmDialog.Callback {
    public static final String TAG = FastBookingFragment.class.getName().toString();
    private FinderActivity activity;
    private OfficeHoursAdapter officeHoursAdapter;
    private List<ServiceModel> serviceModelList = new ArrayList<>();
    private String [] monthsList = {"January", "Febuary", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};

    @BindView(R.id.gridHR)              GridView gridHR;
    @BindView(R.id.btnServices)         View btnServices;
    @BindView(R.id.addMoreBTN)          View addMoreBTN;
    @BindView(R.id.selectedServiceTXT)  TextView selectedServiceTXT;
    @BindView(R.id.btnHMO)              View btnHMO;
    @BindView(R.id.dateTXT)             TextView dateTXT;
    @BindView(R.id.monthTXT)            TextView monthTXT;
    @BindView(R.id.yearTXT)             TextView yearTXT;

    String clinic_ID, provider_ID, selectedServiceID, date, selectedHours;
    String  serviceType="";
    String start_time ="", time_end ="";
    int hrBackposition = -1;
    String details="";


    public static FastBookingFragment newInstance(String clinicID, String providerID, String date, String timeStart, String timeEnd) {
        FastBookingFragment fragment = new FastBookingFragment();
        fragment.clinic_ID = clinicID;
        fragment.provider_ID = providerID;
        fragment.date = date;
        fragment.start_time = timeStart;
        fragment.time_end = timeEnd;
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_fast_booking;
    }


    @SuppressLint("ResourceAsColor")
    @Override
    public void onViewReady() {
        activity = (FinderActivity) getContext();
        btnServices.setOnClickListener(this);
        btnHMO.setOnClickListener(this);
        Clinic.getDefault().getServiceList(getContext(), provider_ID);
        splitHrs();
        splitDate();
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.setTitle("Booking Appointment");
    }

    @Subscribe
    public void onResponse(Clinic.ServiceResponse serviceResponse) {
        CollectionTransformer<ServiceModel> transformer = serviceResponse.getData(CollectionTransformer.class);
        if (transformer.status) {
            for(int i=0; i<transformer.data.size(); i++){
                ServiceModel model = new ServiceModel();
                model.id = transformer.data.get(i).id;
                model.name = transformer.data.get(i).name;
                serviceModelList.add(model);
            }

            if(serviceResponse.isNext()){
                setData(serviceModelList, "new");
            }
            else {
                setData(serviceModelList, "refresh");
            }

        }
    }
    @Subscribe
    public void onResponse(Clinic.BookingResponse bookingResponse) {
        BaseTransformer transformer = bookingResponse.getData(BaseTransformer.class);
        if (transformer.status) {
            ToastMessage.show(activity, transformer.msg, ToastMessage.Status.SUCCESS);
            activity.startMainActivity("home");
        }
        else{
            ToastMessage.show(activity, transformer.msg, ToastMessage.Status.FAILED);
        }
    }



    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnServices:
                ServiceDialog.newInstance(activity, this, serviceModelList, details).show(getFragmentManager(), TAG);
                break;
            case R.id.btnHMO:
//                HMODialog.newInstance(activity, this).show(getFragmentManager(), TAG);
                break;
        }

    }




    public void splitHrs(){
        int startTime =0;
        int endTime =00;
        int minStart=00;
        int  minEnd=00;
        String MinTime="";
        SimpleDateFormat sdf  = new SimpleDateFormat("hh:mm");
        SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm a");
//        try {
//            Date dt = sdf.parse(start_time);
//            Date dts = sdf.parse(time_end);
//            String startTime = sdfs.format(dt);
//            String endTime = sdfs.format(dts);
//            System.out.println("buntag" + startTime);
//            System.out.println("buntag" + endTime);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }

        String [] hrsStart = start_time.split(":");
        for (int i=0; i<hrsStart.length; i++) {
            if(i==0) {
                startTime = Integer.parseInt(hrsStart[0]);
            }
            else if(i==1) {
                minEnd  =Integer.parseInt(hrsStart[i]);
                MinTime = hrsStart[i];
            }

        }
        String [] hrsEnd = time_end.split(":");
        for (int i=0; i<hrsEnd.length; i++){
            if(i==0) {
                endTime = Integer.parseInt(hrsEnd[0]);
            }
            else if(i==1) {
                minEnd = Integer.parseInt(hrsEnd[1]);
            }
        }
        int timeDiff= endTime - startTime;

        int [] hrs = new int[timeDiff];
        for(int i=0; i<timeDiff; i++){
              hrs[i] =   startTime +i;
        }
        String [] list = new String[timeDiff];
          for(int j=0; j<hrs.length; j++){
              if(hrs[j]<12){
                  String morning = hrs[j] + ":" + MinTime;

                  Date dt = null;
                  try {
                      dt = sdf.parse(morning);
                      String mtime = sdfs.format(dt);
                      list[j] = mtime;
                  } catch (ParseException e) {
                      e.printStackTrace();
                  }
              }
              else{
                  String afternoon = hrs[j] + ":" + MinTime;
                  try {
                      Date dt = sdf.parse(afternoon);
                      String atime = sdfs.format(dt);
                      list[j] = atime;
                  } catch (ParseException e) {
                      e.printStackTrace();
                  }
              }
           }

//
//        officeHoursAdapter = new OfficeHoursAdapter(activity, list);
//        gridHR.setAdapter(officeHoursAdapter);
//        gridHR.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                TextView hrTV = (TextView) view;
//                TextView backSelected;
//                hrTV.setBackgroundResource(R.drawable.bg_blue_rounded_corners);
//                hrTV.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
//                selectedHours = hrTV.getText().toString();
//                backSelected = (TextView) gridHR.getChildAt(hrBackposition);
//                if (hrBackposition != -1) {
//                    backSelected.setSelected(false);
//                    backSelected.setBackgroundResource(0);
//                    backSelected.setBackgroundResource(R.drawable.bg_gray_rounded_corners);
//                    backSelected.setTextColor(ContextCompat.getColor(activity, R.color.dark_gray));
//                }
//                hrBackposition = position;
//            }
//        });
    }
    private void splitDate() {
         String [] splitDate = date.split("-");
         for(int i=0; i<splitDate.length; i++){
             if(i ==0){
                 yearTXT.setText(splitDate[i]);
             }
             else if(i == 1){
                   int mnth = Integer.parseInt(splitDate[i]);
                   for(int j=0; j<mnth; j++){
                       monthTXT.setText(monthsList[j]);
                   }
             }
             else{
                 dateTXT.setText(splitDate[i]);
             }
         }
    }

    @Override
    public void onServiceID(String selectedID, String serviceName) {
                 selectedServiceID = selectedID;
                 serviceType = serviceName;
                 if(!serviceType.isEmpty()){
                     btnServices.setVisibility(View.GONE);
                     addMoreBTN.setVisibility(View.VISIBLE);
                     selectedServiceTXT.setVisibility(View.VISIBLE);
                     selectedServiceTXT.setText(serviceType);
                 }
    }


    @Override
    public void onConfirmSuccess() {
        APIRequest apiRequest = Clinic.getDefault().bookAppointment(activity)
                .addParameter(Keys.USER_ID, UserData.getUserModel().userId)
                .addParameter(Keys.PROVIDER_ID, provider_ID)
                .addParameter(Keys.SERVICE_ID, selectedServiceID)
                .addParameter(Keys.CLINIC_ID, clinic_ID)
                .addParameter(Keys.DATE, date)
                .addParameter(Keys.START_TIME, selectedHours)
                .addParameter(Keys.APPOINTMENT_TYPE, "online");
        apiRequest.execute();
    }
    @OnClick(R.id.confirmBTN)
    void confirmBTN(){
        ConfirmDialog.newInstance(activity, this, date, serviceType, selectedHours ).show(getFragmentManager(), TAG);
    }

    private void setData(List<ServiceModel> serviceModelList, String type) {
        this.serviceModelList = serviceModelList;
        this.details = type;
    }
}
