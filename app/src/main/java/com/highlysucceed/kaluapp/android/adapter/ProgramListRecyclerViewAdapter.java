package com.highlysucceed.kaluapp.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.data.model.api.ArticlesModel;
import com.highlysucceed.kaluapp.data.model.api.ProgramListModel;
import com.highlysucceed.kaluapp.vendor.android.base.BaseRecylerViewAdapter;
import com.highlysucceed.kaluapp.vendor.android.java.Log;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ProgramListRecyclerViewAdapter extends BaseRecylerViewAdapter<ProgramListRecyclerViewAdapter.ViewHolder, ProgramListModel> {

    private ClickListener clickListener1;

    public ProgramListRecyclerViewAdapter(Context context, ClickListener clickListener)
    {
        super(context);
        this.clickListener1 = clickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_item_program), clickListener1);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.txtAddress.setText(holder.getItem().getAddress());
        holder.txtEmail.setText(holder.getItem().getEmail());
        holder.txtNumber.setText(holder.getItem().getNumber());

    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder implements View.OnClickListener {

       @BindView(R.id.txtAddress)
       TextView txtAddress;
       @BindView(R.id.txtEmail)
       TextView txtEmail;
       @BindView(R.id.txtNumber)
       TextView txtNumber;
       @BindView(R.id.programDetailsLinear)
        LinearLayout programDetailsLinear;
       ClickListener clickListener;

        public ViewHolder(View view, ClickListener clickListener) {
            super(view);
            view.setOnClickListener(this);
            this.clickListener = clickListener;
        }

        public ProgramListModel getItem() {
            return (ProgramListModel) super.getItem();
        }


        @Override
        public void onClick(View v) {
            clickListener.onItemClick(true);

        }
    }


    public interface ClickListener{
        void onItemClick(boolean click);
    }




} 
