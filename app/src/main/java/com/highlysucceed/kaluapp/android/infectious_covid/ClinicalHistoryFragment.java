package com.highlysucceed.kaluapp.android.infectious_covid;

import android.util.Log;
import android.widget.TextView;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.activity.PersonalInfoActivity;
import com.highlysucceed.kaluapp.data.model.api.InfoModel;
import com.highlysucceed.kaluapp.data.preference.UserData;
import com.highlysucceed.kaluapp.server.request.Auth;
import com.highlysucceed.kaluapp.server.request.Infectious;
import com.highlysucceed.kaluapp.vendor.android.base.BaseFragment;
import com.highlysucceed.kaluapp.vendor.server.request.APIRequest;
import com.highlysucceed.kaluapp.vendor.server.transformer.BaseTransformer;
import com.highlysucceed.kaluapp.vendor.server.transformer.CollectionTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ClinicalHistoryFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {
    public static final String TAG = ClinicalHistoryFragment.class.getName().toString();

    @BindView(R.id.travelHistoryTXT)    TextView travelHistoryTXT;
    @BindView(R.id.exposureHistoryTXT)  TextView exposureHistoryTXT;
    @BindView(R.id.onsetTXT)            TextView onsetTXT;
    @BindView(R.id.dateSymptompsTXT)    TextView dateSymptompsTXT;
    @BindView(R.id.homeSRL)             SwipeRefreshLayout homeSRL;
    private APIRequest apiRequest;
    private PersonalInfoActivity activity;

    public static ClinicalHistoryFragment newInstance() {
        ClinicalHistoryFragment fragment = new ClinicalHistoryFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_clinical_history;
    }

    @Override
    public void onViewReady() {  activity = (PersonalInfoActivity) getContext();
        activity.setTitle("Concise Clinical History");
        homeSRL.setOnRefreshListener(this);

    }
    @Override
    public void onResume() {
        super.onResume();
        onRefresh();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }


    @Subscribe
    public void onResponse(Infectious.LoginResponse response){
        CollectionTransformer<InfoModel> transformer = response.getData(CollectionTransformer.class);
        if(transformer.status) {

            if (!transformer.data.get(0).CONCISECLINICALHISTORY.travelHistory.equals("")) {
                travelHistoryTXT.setText(transformer.data.get(0).CONCISECLINICALHISTORY.travelHistory);
            }else {
                travelHistoryTXT.setText("--");
            }

            if (!transformer.data.get(0).CONCISECLINICALHISTORY.exposureHistory.equals("")) {
                exposureHistoryTXT.setText(transformer.data.get(0).CONCISECLINICALHISTORY.exposureHistory);
            }else {
                exposureHistoryTXT.setText("--");
            }

            if (!transformer.data.get(0).CONCISECLINICALHISTORY.initialOnsetOfSymptoms.equals("")){
                onsetTXT.setText(transformer.data.get(0).CONCISECLINICALHISTORY.initialOnsetOfSymptoms);
            }else {
                onsetTXT.setText("--");
            }

            if (!transformer.data.get(0).CONCISECLINICALHISTORY.dateOfResolutionOfSymptoms.equals("")) {
                dateSymptompsTXT.setText(transformer.data.get(0).CONCISECLINICALHISTORY.dateOfResolutionOfSymptoms);
            }else {
                dateSymptompsTXT.setText("--");
            }

         }
        }

    @Override
    public void onRefresh() {
        refreshList();
    }
    public void refreshList(){
        apiRequest = Infectious.getDefault().getProfileDetails(activity, String.valueOf(UserData.getUserModel().userId), homeSRL);
        apiRequest.first();
    }
}
