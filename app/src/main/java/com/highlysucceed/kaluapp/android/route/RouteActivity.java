package com.highlysucceed.kaluapp.android.route;

import android.content.Intent;
import android.os.Bundle;

import com.highlysucceed.kaluapp.android.activity.BookingActivity;
import com.highlysucceed.kaluapp.android.activity.FinderActivity;
import com.highlysucceed.kaluapp.android.activity.First1000DaysActivity;
import com.highlysucceed.kaluapp.android.activity.LandingActivity;
import com.highlysucceed.kaluapp.android.activity.MainActivity;
import com.highlysucceed.kaluapp.android.activity.PersonalInfoActivity;
import com.highlysucceed.kaluapp.android.activity.PractitionerActivity;
import com.highlysucceed.kaluapp.android.activity.ProfileActivity;
import com.highlysucceed.kaluapp.android.activity.RegisterActivity;
import com.highlysucceed.kaluapp.data.model.api.FinderModel;
import com.highlysucceed.kaluapp.vendor.android.base.BaseActivity;
import com.highlysucceed.kaluapp.vendor.android.base.RouteManager;

import java.util.List;

/**
 * Created by Labyalo on 2/6/2017.
 */

public class RouteActivity extends BaseActivity {

    public void startMainActivity(String fragmentTAG){
        RouteManager.Route.with(this)
                .addActivityClass(MainActivity.class)
                .addActivityTag("main")
                .addFragmentTag(fragmentTAG)
                .startActivity(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                finish();
    }

    public void startRegisterActivity(String fragmentTAG){
        RouteManager.Route.with(this)
                .addActivityClass(RegisterActivity.class)
                .addActivityTag("register")
                .addFragmentTag(fragmentTAG)
                .startActivity();
    }

    public void startLandingActivity(String fragmentTAG){
        RouteManager.Route.with(this)
                .addActivityClass(LandingActivity.class)
                .addActivityTag("landing")
                .addFragmentTag(fragmentTAG)
                .startActivity(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        finish();
    }

    public void startProfileActivity(String fragmentTAG){
        Bundle bundle = new Bundle();
        RouteManager.Route.with(this)
                .addActivityClass(ProfileActivity.class)
                .addActivityTag("cash_in")
                .addFragmentTag(fragmentTAG)
                .startActivity();

    }

    public void startBookingActivity(String fragmentTAG, String id){
        Bundle bundle = new Bundle();
        bundle.putString("id", id);
        RouteManager.Route.with(this)
                .addActivityClass(BookingActivity.class)
                .addActivityTag("booking")
                .addFragmentTag(fragmentTAG)
                .addFragmentBundle(bundle)
                .startActivity();
    }
    public void startPractitionerActivity(String fragmentTAG){
        Bundle bundle = new Bundle();
        RouteManager.Route.with(this)
                .addActivityClass(PractitionerActivity.class)
                .addActivityTag("practitioner")
                .addFragmentTag(fragmentTAG)
                .addFragmentBundle(bundle)
                .startActivity();
    }
    public void startFirst1000DaysActivity(String fragmentTAG){
        Bundle bundle = new Bundle();
        RouteManager.Route.with(this)
                .addActivityClass(First1000DaysActivity.class)
                .addActivityTag("1000days")
                .addFragmentTag(fragmentTAG)
                .startActivity();
    }
    public void startFinderActivity(String fragmentTAG, String id, String location, String date, String hmo){
        Bundle bundle = new Bundle();
        bundle.putString("id", id);
        bundle.putString("location", location);
        bundle.putString("date", date);
        bundle.putString("hmo", hmo);
        RouteManager.Route.with(this)
                .addActivityClass(FinderActivity.class)
                .addActivityTag("finder")
                .addFragmentTag(fragmentTAG)
                .addFragmentBundle(bundle)
                .startActivity();
    }
    public void startPersonalInfoActivity(String fragmentTAG){
        Bundle bundle = new Bundle();
        RouteManager.Route.with(this)
                .addActivityClass(PersonalInfoActivity.class)
                .addActivityTag("info")
                .addFragmentTag(fragmentTAG)
                .startActivity();
    }

//    @Subscribe
//    public void invalidToken(InvalidToken invalidToken){
//        Log.e("Token", "Expired");
//        EventBus.getDefault().unregister(this);
//        UserData.insert(new UserModel());
//        UserData.insert(UserData.TOKEN_EXPIRED, true);
//        startLandingActivity();
//    }
}
