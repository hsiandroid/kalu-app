package com.highlysucceed.kaluapp.android.fragment.register;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.activity.RegisterActivity;
import com.highlysucceed.kaluapp.server.request.Auth;
import com.highlysucceed.kaluapp.vendor.android.base.BaseFragment;
import com.highlysucceed.kaluapp.vendor.android.java.ToastMessage;
import com.highlysucceed.kaluapp.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class TermsFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = TermsFragment.class.getName().toString();

    private RegisterActivity registerActivity;

    @BindView(R.id.termsBTN)            Button termsBTN;
    @BindView(R.id.agreeCHBX)           CheckBox agreeCHBX;



    public static TermsFragment newInstance() {
        TermsFragment fragment = new TermsFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_terms;
    }

    @Override
    public void onViewReady() {
        registerActivity = (RegisterActivity) getContext();
        registerActivity.showActivityNo1(false);
        registerActivity.showActivityNo2(false);
        registerActivity.showActivityNo3(false);
        termsBTN.setOnClickListener(this);

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        registerActivity.setTitle("Terms & Conditions");
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.termsBTN:
                if (agreeCHBX.isChecked()){
                    registerActivity.openAccountsFragment();
                }else {
                    ToastMessage.show(getContext(), "You must accept our Terms and Condition before you proceed", ToastMessage.Status.FAILED);
                }
                break;
        }
    }
}
