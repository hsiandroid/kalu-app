package com.highlysucceed.kaluapp.android.fragment.onethousanddays;

import android.util.Log;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.activity.First1000DaysActivity;
import com.highlysucceed.kaluapp.server.request.Auth;
import com.highlysucceed.kaluapp.vendor.android.base.BaseFragment;
import com.highlysucceed.kaluapp.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class TodayFragment extends BaseFragment {
    public static final String TAG = TodayFragment.class.getName().toString();
    private First1000DaysActivity activity;

    public static TodayFragment newInstance() {
        TodayFragment fragment = new TodayFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_today;
    }

    @Override
    public void onViewReady() {
        activity = (First1000DaysActivity) getContext();
        activity.setTitle("Today");
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }


}
