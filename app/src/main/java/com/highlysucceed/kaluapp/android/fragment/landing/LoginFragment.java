package com.highlysucceed.kaluapp.android.fragment.landing;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.activity.LandingActivity;
import com.highlysucceed.kaluapp.data.model.api.UserModel;
import com.highlysucceed.kaluapp.data.preference.UserData;
import com.highlysucceed.kaluapp.server.request.Auth;
import com.highlysucceed.kaluapp.vendor.android.base.BaseFragment;
import com.highlysucceed.kaluapp.vendor.android.java.ToastMessage;
import com.highlysucceed.kaluapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class LoginFragment extends BaseFragment implements View.OnClickListener, LandingActivity.FBCallback , LandingActivity.GmailCallback{
    public static final String TAG = LoginFragment.class.getName().toString();

    private LandingActivity landingActivity;
    private static final String EMAIL = "email";
    private CallbackManager callbackManager;


    @BindView(R.id.emailET)             EditText emailET;
    @BindView(R.id.passET)              EditText passET;
    @BindView(R.id.loginBTN)            TextView loginBTN;
    @BindView(R.id.signUpBTN)           TextView signUpBTN;
    @BindView(R.id.forgotBTN)           TextView forgotBTN;
    @BindView(R.id.facebookBTN)         View facebookBTN;


    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_login;
    }

    @Override
    public void onViewReady() {
        landingActivity = (LandingActivity) getContext();
        loginBTN.setOnClickListener(this);
        signUpBTN.setOnClickListener(this);
        callbackManager  = CallbackManager.Factory.create();

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
            SingleTransformer<UserModel> singleTransformer = loginResponse.getData(SingleTransformer.class);

            if (singleTransformer.status){
                UserData.insert(singleTransformer.data);
                UserData.insert(UserData.AUTHORIZATION, singleTransformer.token);
                ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
                landingActivity.startMainActivity("home");
//
            } else{
                Toast.makeText(landingActivity, singleTransformer.msg, Toast.LENGTH_SHORT).show();

            }
    }
    @Subscribe
    public void onResponse(Auth.FBResponse loginResponse){
        SingleTransformer<UserModel> singleTransformer = loginResponse.getData(SingleTransformer.class);
        if (singleTransformer.status){
            UserData.insert(singleTransformer.data);
            UserData.insert(UserData.AUTHORIZATION, singleTransformer.token);
            ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
            landingActivity.startMainActivity("home");
        }
        else{
            ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.loginBTN:
                if(!emailET.getText().toString().isEmpty() && !passET.getText().toString().isEmpty()){
                Auth.getDefault().login(getContext(), emailET.getText().toString(), passET.getText().toString());
                }
                else if(emailET.getText().toString().isEmpty()){
                    emailET.setError("Empty Fields");
                }
                else{
                    passET.setError("Empty Fields");
                }
              break;
            case R.id.signUpBTN:
                landingActivity.startRegisterActivity("terms");
                break;

        }

    }

    @OnClick({R.id.facebookBTN})
    void facebookBTN(){
        landingActivity.attemptFBLogin(this);
    }
    @OnClick({R.id.googleBTN})
    void googleBTN(){
        landingActivity.attemptGMLogin(this);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode , resultCode , data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void success(UserModel userModel) {
        Log.e("TokenS", ">>>" + landingActivity.getAccessToken());
        //dto ka mag call ng api
    }

    @Override
    public void failed(String message) {
        Log.e("FBERROR", message);
    }

    @Override
    public void fbtokenResult(String token) {
        Auth.getDefault().fbLogin(landingActivity, token);
    }

    @Override
    public void gmailTokenResult(String token) {

    }
}
