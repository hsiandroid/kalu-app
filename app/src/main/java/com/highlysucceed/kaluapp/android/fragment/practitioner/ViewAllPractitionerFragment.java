package com.highlysucceed.kaluapp.android.fragment.practitioner;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.activity.PractitionerActivity;
import com.highlysucceed.kaluapp.android.adapter.ExpertiseListRecyclerViewAdapter;
import com.highlysucceed.kaluapp.android.adapter.PractitionerDetailsRecyclerViewAdapter;
import com.highlysucceed.kaluapp.data.model.api.OccupationModel;
import com.highlysucceed.kaluapp.data.model.api.PractitionerModel;
import com.highlysucceed.kaluapp.server.request.Auth;
import com.highlysucceed.kaluapp.server.request.Practitioner;
import com.highlysucceed.kaluapp.vendor.android.base.BaseFragment;
import com.highlysucceed.kaluapp.vendor.android.java.ToastMessage;
import com.highlysucceed.kaluapp.vendor.server.transformer.BaseTransformer;
import com.highlysucceed.kaluapp.vendor.server.transformer.CollectionTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ViewAllPractitionerFragment extends BaseFragment implements ExpertiseListRecyclerViewAdapter.ClickListener {
    public static final String TAG = ViewAllPractitionerFragment.class.getName().toString();

    @BindView(R.id.recyclerDoctorList)             RecyclerView recyclerDoctorList;
    @BindView(R.id.activityTitleTXT)               TextView activityTitleTXT;
    @BindView(R.id.searchBTN)                       ImageView searchBTN;


    private ExpertiseListRecyclerViewAdapter expertiseListRecyclerViewAdapter;
    private LinearLayoutManager linearLayoutManager;
    private PractitionerActivity activity;
    String expertiseID;

    public static ViewAllPractitionerFragment newInstance() {
        ViewAllPractitionerFragment fragment = new ViewAllPractitionerFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_viewall_practitioner;
    }

    @Override
    public void onViewReady() {
         activity = (PractitionerActivity) getContext();
         activityTitleTXT.setText("Profession Details");
         setUpListView();
    }

    public void setUpListView() {
        expertiseListRecyclerViewAdapter = new ExpertiseListRecyclerViewAdapter(getContext(), this);
        linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerDoctorList.setLayoutManager(linearLayoutManager);
        recyclerDoctorList.setAdapter(expertiseListRecyclerViewAdapter);
    }

    @Subscribe
    public void onResponse(Practitioner.OccupationList response){
        CollectionTransformer<OccupationModel> transformer = response.getData(CollectionTransformer.class);
        if(transformer.status){
            if(response.isFirst()){
//                expertiseRecyclerViewAdapter.addNewData(transformer.data);
                expertiseListRecyclerViewAdapter.addNewData(transformer.data);
            }
            else {
//                expertiseRecyclerViewAdapter.setNewData(transformer.data);
                expertiseListRecyclerViewAdapter.addNewData(transformer.data);
            }
        }
    }
    @Subscribe
    public void onResponse(Practitioner.PractitionersList response){
        CollectionTransformer<PractitionerModel> transformer = response.getData(CollectionTransformer.class);
        if(transformer.status){
            activity.openPractitionersFragment(expertiseID);
        }
        else{
            ToastMessage.show(getContext(), transformer.msg, ToastMessage.Status.FAILED);
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        searchBTN.setVisibility(View.GONE);
        Practitioner.getDefault().getOccupationList(activity);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }


    @OnClick(R.id.activityBackBTN)
    void  activityBackBTN(){
        activity.onBackPressed();
    }

    @Override
    public void onClickExpertise(String id) {
        expertiseID = id;
        Practitioner.getDefault().getProviderLists(activity, id);
    }
}
