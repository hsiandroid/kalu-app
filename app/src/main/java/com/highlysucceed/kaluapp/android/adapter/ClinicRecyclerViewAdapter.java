package com.highlysucceed.kaluapp.android.adapter;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.data.model.api.ClinicModel;
import com.highlysucceed.kaluapp.data.model.api.ClinicsModel;
import com.highlysucceed.kaluapp.data.model.api.HospitalModel;
import com.highlysucceed.kaluapp.data.model.api.ProgramListModel;
import com.highlysucceed.kaluapp.vendor.android.base.BaseRecylerViewAdapter;
import com.highlysucceed.kaluapp.vendor.android.java.Log;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ClinicRecyclerViewAdapter extends BaseRecylerViewAdapter<ClinicRecyclerViewAdapter.ViewHolder, ClinicsModel> {

    private ClickListener clickListener;

    public ClinicRecyclerViewAdapter(Context context, ClickListener clickListener)
    {
        super(context);
        this.clickListener = clickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_clinic_list));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
            holder.setItem(getItem(position));

            holder.txtCLINIC.setText(holder.getItem().name);
            holder.clinicAddress.setText(holder.getItem().address);
            if(!holder.getItem().thumbnail.filename.equals("")) {
                Glide.with(getContext())
                        .load(holder.getItem().thumbnail.fullPath)
                        .apply(new RequestOptions()
                                .placeholder(R.drawable.ic_no_image)
                                .error(R.drawable.ic_no_image))
                        .into(holder.clinicIV);

            }
            else{
                Glide.with(getContext())
                        .load(R.drawable.ic_no_image)
                        .apply(new RequestOptions()
                                .placeholder(R.drawable.ic_no_image)
                                .error(R.drawable.ic_no_image))
                        .into(holder.clinicIV);
            }

    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder implements View.OnClickListener {

        @BindView(R.id.clinicIV)       ImageView clinicIV;
        @BindView(R.id.txtCLINIC)      TextView txtCLINIC;
        @BindView(R.id.clinicAddress)  TextView clinicAddress;
        @BindView(R.id.clinicView)     View clinicView;


        public ViewHolder(View view) {
            super(view);
            clinicView.setOnClickListener(this);
        }

        public ClinicsModel getItem() {
            return (ClinicsModel) super.getItem();
        }

        @Override
        public void onClick(View v) {
            if(v.getId() == R.id.clinicView) {

                clickListener.onClickClinic(String.valueOf(getItem().id));
            }
        }
    }
    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener{
        void onClickClinic(String id);
    }

} 
