package com.highlysucceed.kaluapp.android.fragment.clinic;

import android.content.Context;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.activity.BookingActivity;
import com.highlysucceed.kaluapp.android.adapter.ClinicListRecyclerViewAdapter;
import com.highlysucceed.kaluapp.android.adapter.ClinicRecyclerViewAdapter;
import com.highlysucceed.kaluapp.android.adapter.FeaturedPhotoAdapter;
import com.highlysucceed.kaluapp.android.dialog.ClinicDialog;
import com.highlysucceed.kaluapp.android.dialog.FeaturedPhotoDialog;
import com.highlysucceed.kaluapp.data.model.api.ClinicModel;
import com.highlysucceed.kaluapp.data.model.api.ClinicsModel;
import com.highlysucceed.kaluapp.data.model.api.FeturedPhotoModel;
import com.highlysucceed.kaluapp.data.model.api.HospitalModel;
import com.highlysucceed.kaluapp.server.request.Clinic;
import com.highlysucceed.kaluapp.vendor.android.base.BaseFragment;
import com.highlysucceed.kaluapp.vendor.server.request.APIRequest;
import com.highlysucceed.kaluapp.vendor.server.transformer.CollectionTransformer;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class HospitalFragment extends BaseFragment implements ClinicRecyclerViewAdapter.ClickListener, ClinicListRecyclerViewAdapter.ClinicListener, ClinicDialog.Callback, SwipeRefreshLayout.OnRefreshListener {
    public static final String TAG = HospitalFragment.class.getName().toString();
    private ClinicListRecyclerViewAdapter clinicListRecyclerViewAdapter;
    private ClinicRecyclerViewAdapter clinicRecyclerViewAdapter;
    private LinearLayoutManager linearLayoutManager;
    private BookingActivity activity;
    private Handler handler;
    private Runnable runnable;
    private FeaturedPhotoAdapter adapter;
    private String hospitalID;
    private APIRequest apiRequest;
    @BindView(R.id.activityTitleTXT)   TextView activityTitleTXT;
    @BindView(R.id.recycleClinic)     RecyclerView recycleClinic;
    @BindView(R.id.recyclerSearchClinic)  RecyclerView recyclerSearchClinic;
    @BindView(R.id.coverPhotoIV)      ImageView coverPhotoIV;
    @BindView(R.id.logoIV)            ImageView logoIV;
    @BindView(R.id.hospitalNameTXT)   TextView hospitalNameTXT;
    @BindView(R.id.emailTXT)          TextView emailTXT;
    @BindView(R.id.addressTXT)        TextView addressTXT;
    @BindView(R.id.contactTXT)        TextView contactTXT;
    @BindView(R.id.aboutTXT)          TextView aboutTXT;
    @BindView(R.id.titleheaderVIEW)     View titleheaderVIEW;
    @BindView(R.id.searchHeaderVIEW)    View searchHeaderVIEW;
    @BindView(R.id.mainLayout)          View mainLayout;
    @BindView(R.id.searchList)          View searchList;
    @BindView(R.id.backButton)         ImageView backButton;
    @BindView(R.id.removeBTN)          ImageView removeBTN;
    @BindView(R.id.searchEDT)          EditText searchEDT;
    @BindView(R.id.gridIMG)            GridView gridIMG;
    @BindView(R.id.progress_bar)       ProgressBar progress_bar;
    @BindView(R.id.homeSRL)            SwipeRefreshLayout homeSRL;

    private String details="";
    private   List<ClinicsModel> list = new ArrayList<>();

    public static HospitalFragment newInstance(String id) {
        HospitalFragment fragment = new HospitalFragment();
        fragment.hospitalID = id;
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_hospital;
    }

    @Override
    public void onViewReady() {
        activity = (BookingActivity) getContext();
        setUpListView();
        setUpSearchList();
        homeSRL.setOnRefreshListener(this);
    }

    private void setUpListView() {
       clinicRecyclerViewAdapter  =  new ClinicRecyclerViewAdapter(activity, this);
        linearLayoutManager = new LinearLayoutManager(getContext());
        recycleClinic.setLayoutManager(linearLayoutManager);
        recycleClinic.setAdapter(clinicRecyclerViewAdapter);
    }
    private void setUpSearchList() {
        clinicListRecyclerViewAdapter = new ClinicListRecyclerViewAdapter(getContext() , this);
        linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerSearchClinic.setLayoutManager(linearLayoutManager);
        recyclerSearchClinic.setAdapter(clinicListRecyclerViewAdapter);

    }
    @Override
    public void onResume() {
        super.onResume();
        activityTitleTXT.setText("Hospital Details");
        onRefresh();

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }


    @Subscribe
    public void onResponse(Clinic.HosptalDetailsReponse hosptalDetailsReponse){
        CollectionTransformer<HospitalModel> transformer = hosptalDetailsReponse.getData(CollectionTransformer.class);
          if(transformer.status){
              contactTXT.setText((transformer.data.get(0).contactNumber));
              hospitalNameTXT.setText(transformer.data.get(0).name);
              addressTXT.setText((transformer.data.get(0).address));
              emailTXT.setText((transformer.data.get(0).email));

              if (!transformer.data.get(0).thumbnail.filename.equals("")){
                  Picasso.with(getContext()).load(transformer.data.get(0).thumbnail.fullPath)
                          .error(R.drawable.ic_no_image)
                          .placeholder(R.drawable.ic_no_image)
                          .into(logoIV);
              }  else {
                  Picasso.with(getContext()).load(R.drawable.ic_no_image)
                          .error(R.drawable.ic_no_image)
                          .placeholder(R.drawable.ic_no_image)
                          .into(logoIV);
              }
              if (!transformer.data.get(0).thumbnail.filename.equals("")){
                  Picasso.with(getContext()).load(transformer.data.get(0).thumbnail.fullPath)
                          .error(R.drawable.clinic_bg)
                          .placeholder(R.drawable.clinic_bg)
                          .into(coverPhotoIV);
              }  else {
                  Picasso.with(getContext()).load(R.drawable.ic_thumbnail)
                          .error(R.drawable.clinic_bg)
                          .placeholder(R.drawable.clinic_bg)
                          .into(coverPhotoIV);
              }

//              if(hosptalDetailsReponse.isNext()){
//                   setAddClinic(transformer.data.get(0).clinics.data);
//              }
//              else {
//                  setNewClinic(transformer.data.get(0).clinics.data);
//              }
          }

    }

    @Subscribe
    public void onResponse(Clinic.HospitalReponse reponse) {
        CollectionTransformer<HospitalModel> transformer = reponse.getData(CollectionTransformer.class);
        if(transformer.status){
//            if(reponse.isNext()){
//                ClinicDialog.newInstance(getContext(), this, activity, transformer.data.get(0).clinics.data, "new").show(getChildFragmentManager(), TAG);
//            }
//            else{
//                ClinicDialog.newInstance(getContext(), this, activity, transformer.data.get(0).clinics.data, "refresh").show(getChildFragmentManager(), TAG);
//            }
        }

    }
    @Subscribe
    public void onResponse(Clinic.SearchResponse response){
        CollectionTransformer<ClinicModel> transformer = response.getData(CollectionTransformer.class);
        if(response.isNext()){
            progress_bar.setVisibility(View.GONE);
            recyclerSearchClinic.setVisibility(View.VISIBLE);
            clinicListRecyclerViewAdapter.addNewData(transformer.data);
        }
        else {
            progress_bar.setVisibility(View.GONE);
            recyclerSearchClinic.setVisibility(View.VISIBLE);
            clinicListRecyclerViewAdapter.setNewData(transformer.data);
        }
    }

    @Subscribe
    public void onResponse(Clinic.ALlClinicReponse response){
        CollectionTransformer<ClinicModel> transformer = response.getData(CollectionTransformer.class);
        if (transformer.status) {
            if(response.isNext()){
                progress_bar.setVisibility(View.GONE);
                recyclerSearchClinic.setVisibility(View.VISIBLE);
                clinicListRecyclerViewAdapter.addNewData(transformer.data);
            }
            else {
                progress_bar.setVisibility(View.GONE);
                recyclerSearchClinic.setVisibility(View.VISIBLE);
                clinicListRecyclerViewAdapter.setNewData(transformer.data);
            }
        }
    }
    @Subscribe
    public void onResponse(Clinic.FeaturedPhotoResponse response) {
        CollectionTransformer<FeturedPhotoModel> transformer = response.getData(CollectionTransformer.class);
        if(transformer.status){
            adapter = new FeaturedPhotoAdapter(activity, transformer.data);
            gridIMG.setAdapter(adapter);
        }
    }
    @Subscribe
    public void onResponse(Clinic.PhotoResponse response) {
        CollectionTransformer<FeturedPhotoModel> transformer = response.getData(CollectionTransformer.class);
        if(transformer.status){
            FeaturedPhotoDialog.newInstance(getContext(), activity, transformer.data).show(getChildFragmentManager(), TAG);
        }
    }


    private void setAddClinic(List<ClinicsModel> clinicModels) {
        clinicRecyclerViewAdapter.addNewData(clinicModels);
    }
    private void setNewClinic(List<ClinicsModel> clinicModels) {
        clinicRecyclerViewAdapter.setNewData(clinicModels);

    }


    @Override
    public void onClickClinic(String id) {
        activity.openClinicDetails(id);
    }

    @OnClick(R.id.searchBTN)
    void searchBTN(){
        titleheaderVIEW.setVisibility(View.GONE);
        homeSRL.setVisibility(View.GONE);
        searchHeaderVIEW.setVisibility(View.VISIBLE);
        searchList.setVisibility(View.VISIBLE);
//        Clinic.getDefault().getAllClinic(activity);
        textSearch();
    }
    private void textSearch() {
        searchEDT.requestFocus();
        searchEDT.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.toString().isEmpty()) {
                    runnable = new Runnable() {
                        @Override
                        public void run() {
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progress_bar.setVisibility(View.VISIBLE);
                                    recyclerSearchClinic.setVisibility(View.GONE);
                                    removeBTN.setVisibility(View.GONE);
                                  //  Clinic.getDefault().searchClinic(activity, editable.toString().toUpperCase());
                                }
                            });
                        }
                    };
                    handler = new Handler();
                    handler.postDelayed(runnable, 500);

                }
                else{
                    runnable = new Runnable() {
                        @Override
                        public void run() {
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progress_bar.setVisibility(View.VISIBLE);
                                    recyclerSearchClinic.setVisibility(View.GONE);
                                    removeBTN.setVisibility(View.VISIBLE);
                                    removeBTN.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            searchEDT.getText().clear();
                                        }
                                    });
                                    //Clinic.getDefault().searchClinic(activity, editable.toString().toUpperCase());
                                }
                            });
                        }
                    };
                    handler = new Handler();
                    handler.postDelayed(runnable, 500);

                }

            }

        });
    }


    @OnClick(R.id.backButton)
    void backButton(){
        closeKeyboard();
        searchHeaderVIEW.setVisibility(View.GONE);
        homeSRL.setVisibility(View.VISIBLE);
        titleheaderVIEW.setVisibility(View.VISIBLE);
        searchList.setVisibility(View.GONE);
    }

    private void closeKeyboard()
    {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager manager = (InputMethodManager)
                    activity.getSystemService(
                            Context.INPUT_METHOD_SERVICE);
            manager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onSuccess(String id) {
        activity.openClinicDetails(id);
    }

    @OnClick(R.id.activityBackBTN)
    void activityBackBTN(){ activity.onBackPressed();}
    @OnClick(R.id.clinicVIEWBTN)
    void clinicVIEWBTN(){
        Clinic.getDefault().getCliniclist(activity, hospitalID);
    }
    @OnClick(R.id.photoVIEWBTN)
    void photoVIEWBTN(){
        Clinic.getDefault().getFeaturedPhoto(activity, hospitalID);
    }
    @Override
    public void clickedItem(String id) {
        activity.openClinicDetails(id);
    }

    @Override
    public void onRefresh() {
        refreshList();
    }
    public void refreshList(){
        apiRequest = Clinic.getDefault().getHospitalDetails(activity, hospitalID, homeSRL);
        apiRequest.first();
        Clinic.getDefault().getFeaturedHospital(activity, hospitalID);
    }
}
