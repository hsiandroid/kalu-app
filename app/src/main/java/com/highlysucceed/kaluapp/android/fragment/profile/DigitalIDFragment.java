package com.highlysucceed.kaluapp.android.fragment.profile;

import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.activity.ProfileActivity;
import com.highlysucceed.kaluapp.data.preference.UserData;
import com.highlysucceed.kaluapp.server.request.Auth;
import com.highlysucceed.kaluapp.vendor.android.base.BaseFragment;
import com.highlysucceed.kaluapp.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class DigitalIDFragment extends BaseFragment {
    public static final String TAG = DigitalIDFragment.class.getName().toString();


    private ProfileActivity activity;
    @BindView(R.id.nameTXT)   TextView nameTXT;
    @BindView(R.id.idTXT)     TextView idTXT;
    @BindView(R.id.emailTXT)   TextView emailTXT;
    @BindView(R.id.profileIMG) ImageView profileIMG;

    public static DigitalIDFragment newInstance() {
        DigitalIDFragment fragment = new DigitalIDFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_front_id;
    }

    @Override
    public void onViewReady() {
        activity = (ProfileActivity) getContext();
        nameTXT.setText(UserData.getUserModel().firstname + " " + UserData.getUserModel().lastname);
        idTXT.setText(UserData.getUserModel().qrCode);
        emailTXT.setText(UserData.getUserModel().email);

        if(!UserData.getUserModel().avatar.filename.equals("")) {
            Glide.with(getContext())
                    .load(UserData.getUserModel().avatar.fullPath)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.ic_no_image)
                            .error(R.drawable.ic_no_image))
                    .into(profileIMG);

        }
        else{
            Glide.with(getContext())
                    .load(R.drawable.ic_no_image)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.ic_no_image)
                            .error(R.drawable.ic_no_image))
                    .into(profileIMG);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }

    @OnClick(R.id.viewBTN)
    void  viewBTN(){
        activity.startProfileActivity("qrcode");
        activity.onBackPressed();
    }
}
