package com.highlysucceed.kaluapp.android.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.fragment.DefaultFragment;
import com.highlysucceed.kaluapp.android.fragment.register.AccountsFragment;
import com.highlysucceed.kaluapp.android.fragment.register.HealthInformationFragment;
import com.highlysucceed.kaluapp.android.fragment.register.SignUpFragment;
import com.highlysucceed.kaluapp.android.fragment.register.TermsFragment;
import com.highlysucceed.kaluapp.android.route.RouteActivity;
import com.highlysucceed.kaluapp.data.model.api.AccountModel;
import com.highlysucceed.kaluapp.data.model.api.SignUpModel;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class RegisterActivity extends RouteActivity {
    public static final String TAG = RegisterActivity.class.getName().toString();
    private AccountModel accountModels;
    private SignUpModel signUpModels;
    @BindView(R.id.activityNumber1TXT)
    TextView activityNumber1TXT;
    @BindView(R.id.activityNumber2TXT)
    TextView activityNumber2TXT;
    @BindView(R.id.activityNumber3TXT)
    TextView activityNumber3TXT;

    @Override
    public int onLayoutSet() {
        return R.layout.activity_register;
    }

    @Override
    public void onViewReady() {

    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {

        switch (fragmentName) {
            case "terms":
                openTermsFragment();
                break;
        }
    }

    public void openDefaultFragment() {
        switchFragment(DefaultFragment.newInstance());
    }

    public void openSignUpFragment() {
        switchFragment(SignUpFragment.newInstance());
    }

    public void openAccountsFragment() {
        switchFragment(AccountsFragment.newInstance());
    }

    public void openTermsFragment() {
        switchFragment(TermsFragment.newInstance());
    }

    public void openHealthInformationFragment() {
        switchFragment(HealthInformationFragment.newInstance());
    }

    public void showActivityNo1(boolean choose){
        if (choose){
            activityNumber1TXT.setVisibility(View.VISIBLE);
            activityNumber1TXT.setText("1");
            activityNumber2TXT.setVisibility(View.GONE);
            activityNumber3TXT.setVisibility(View.GONE);
        }else{
            activityNumber1TXT.setVisibility(View.GONE);
        }
    }

    public void showActivityNo2(boolean choose){
        if (choose){
            activityNumber1TXT.setVisibility(View.VISIBLE);
            activityNumber1TXT.setText("\u2713");
            activityNumber2TXT.setVisibility(View.VISIBLE);
            activityNumber2TXT.setText("2");
            activityNumber3TXT.setVisibility(View.GONE);
        }else{
            activityNumber2TXT.setVisibility(View.GONE);
        }
    }

    public void showActivityNo3(boolean choose){
        if (choose){
            activityNumber1TXT.setVisibility(View.VISIBLE);
            activityNumber1TXT.setText("\u2713");
            activityNumber2TXT.setVisibility(View.VISIBLE);
            activityNumber2TXT.setText("\u2713");
            activityNumber3TXT.setVisibility(View.VISIBLE);
            activityNumber3TXT.setText("3");
        }else{
            activityNumber3TXT.setVisibility(View.GONE);
        }
    }

    public void setAccountModel(AccountModel accountModel){
        this.accountModels = accountModel;
    }

    public AccountModel getAccountModel(){
        return accountModels;
    }

    public void setSignUpModel(SignUpModel signUpModel){
        this.signUpModels = signUpModel;
    }

    public SignUpModel getSignUpModel(){
        return signUpModels;
    }


}
