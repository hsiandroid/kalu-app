package com.highlysucceed.kaluapp.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.data.model.api.DoctorModel;
import com.highlysucceed.kaluapp.data.model.api.SampleModel;
import com.highlysucceed.kaluapp.vendor.android.base.BaseRecylerViewAdapter;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class MemberRecyclerViewAdapter extends BaseRecylerViewAdapter<MemberRecyclerViewAdapter.ViewHolder, DoctorModel>{


    public MemberRecyclerViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_doctor_details));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
//        holder.doctorIMG.setImageResource(getItem(position).getImage());
//        holder.doctorName.setText(getItem(position).getName());
//        holder.doctorDetails.setText(getItem(position).getDetails());

    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{
        @BindView(R.id.doctorIMG)
        ImageView doctorIMG;
        @BindView(R.id.doctorName)
        TextView doctorName;
        @BindView(R.id.doctorDetails)
        TextView doctorDetails;

        public ViewHolder(View view) {
            super(view);
        }

        public DoctorModel getItem() {
            return (DoctorModel) super.getItem();
        }
    }

} 
