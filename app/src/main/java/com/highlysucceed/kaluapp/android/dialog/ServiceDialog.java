package com.highlysucceed.kaluapp.android.dialog;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.adapter.ServiceRecyclerViewAdapter;
import com.highlysucceed.kaluapp.data.model.api.SampleModel;
import com.highlysucceed.kaluapp.data.model.api.ServiceModel;
import com.highlysucceed.kaluapp.vendor.android.base.BaseDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;


/**
 * Created by Labyalo on 8/12/2017.
 */

public class  ServiceDialog extends BaseDialog implements ServiceRecyclerViewAdapter.ClickListener {
	public static final String TAG = ServiceDialog.class.getName().toString();
	List<ServiceModel> serviceModelList =  new ArrayList<>();


	private Context context;
	private Callback callback;
	private ServiceRecyclerViewAdapter serviceRecyclerViewAdapter;
	private LinearLayoutManager linearLayoutManager;
	@BindView(R.id.recyclerServices)   RecyclerView recyclerServices;
	String selectedID,  serviceName;
	String type;
;



	public static ServiceDialog newInstance(Context context, Callback callback, List<ServiceModel> serviceModels, String details) {
		ServiceDialog dialog = new ServiceDialog();
		dialog.context = context;
		dialog.callback = callback;
		dialog.serviceModelList = serviceModels;
		dialog.type = details;
		return dialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_services;
	}

	@Override
	public void onViewReady() {
     setUpServiceList();
	}

	private void setUpServiceList() {
		serviceRecyclerViewAdapter = new ServiceRecyclerViewAdapter(getContext(), this);
		linearLayoutManager = new LinearLayoutManager(getContext());
		recyclerServices.setLayoutManager(linearLayoutManager);
		recyclerServices.setAdapter(serviceRecyclerViewAdapter);
		if(type.equals("new")) {
			serviceRecyclerViewAdapter.addNewData(serviceModelList);
		}
		else {
			serviceRecyclerViewAdapter.setNewData(serviceModelList);
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}
	@OnClick(R.id.exitIMG)
	void onExit(){
		if (callback!=null){
			dismiss();
		}
	}
	@OnClick(R.id.btnADD)
	void success(){
		if (callback!=null){
			callback.onServiceID(selectedID, serviceName);
			dismiss();
		}
	}

	@Override
	public void onItemClick(String serviceId, String serviceTYPE) {
		selectedID = serviceId;
		serviceName = serviceTYPE;
	}

	public interface Callback{
		 void onServiceID(String selectedServiceID, String serviceTYPE);
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		getDialog().getWindow().getAttributes().windowAnimations = R.style.dialog_slide_animation; //style id
	}

}
