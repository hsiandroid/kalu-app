package com.highlysucceed.kaluapp.android.dialog;

import android.content.Context;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.activity.BookingActivity;
import com.highlysucceed.kaluapp.android.adapter.ClinicRecyclerViewAdapter;
import com.highlysucceed.kaluapp.data.model.api.ClinicModel;
import com.highlysucceed.kaluapp.data.model.api.ClinicsModel;
import com.highlysucceed.kaluapp.vendor.android.base.BaseDialog;
import com.highlysucceed.kaluapp.vendor.android.java.Log;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Response;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ClinicDialog extends BaseDialog implements ClinicRecyclerViewAdapter.ClickListener {
	public static final String TAG = ClinicDialog.class.getName().toString();

	private ClinicRecyclerViewAdapter clinicRecyclerViewAdapter;
	private LinearLayoutManager       linearLayoutManager;
	private BookingActivity activity;
	private Context context;
	private Callback callback;
	List<ClinicsModel> clinicsModels;
	String recyclerDetails;

	@BindView(R.id.recyclerList)   RecyclerView recyclerList;


	public static ClinicDialog newInstance(Context mCtx, Callback cbk, BookingActivity activity, List<ClinicsModel> data, String details) {
		ClinicDialog dialog = new ClinicDialog();
		dialog.context = mCtx;
		dialog.callback = cbk;
		dialog.activity = activity;
		dialog.clinicsModels = data;
		dialog.recyclerDetails = details;
		return dialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_clinic;
	}

	@Override
	public void onViewReady() {
              setUpListView();
	}

	private void setUpListView() {
		clinicRecyclerViewAdapter  =  new ClinicRecyclerViewAdapter(activity, this);
		linearLayoutManager = new LinearLayoutManager(getContext());
		recyclerList.setLayoutManager(linearLayoutManager);
		recyclerList.setAdapter(clinicRecyclerViewAdapter);

		if(recyclerDetails.equals("new")){
			clinicRecyclerViewAdapter.addNewData(clinicsModels);
		}
		else if(recyclerDetails.equals("refresh")){
			clinicRecyclerViewAdapter.setNewData(clinicsModels);
		}

	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}

	@Override
	public void onClickClinic(String id) {
         callback.clickedItem(id);
	}

	public interface Callback{
		void clickedItem(String id);
	}
	@OnClick(R.id.closeBTN)
	void closeBTN(){
		this.dismiss();
	}
}
