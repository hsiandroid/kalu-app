package com.highlysucceed.kaluapp.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.data.model.api.AccreditorModel;
import com.highlysucceed.kaluapp.data.model.api.SampleModel;
import com.highlysucceed.kaluapp.vendor.android.base.BaseRecylerViewAdapter;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class AccreditorRecyclerViewAdapter extends BaseRecylerViewAdapter<AccreditorRecyclerViewAdapter.ViewHolder, AccreditorModel>{



    public AccreditorRecyclerViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.layout_accreditor_name));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.txtAccreditorName.setText(holder.getItem().name);

    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.txtAccreditorName)     TextView txtAccreditorName;

        public ViewHolder(View view) {
            super(view);
        }

        public AccreditorModel getItem() {
            return (AccreditorModel) super.getItem();
        }
    }

}
