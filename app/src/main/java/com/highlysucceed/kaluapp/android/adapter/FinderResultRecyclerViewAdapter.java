package com.highlysucceed.kaluapp.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.data.model.api.FinderModel;

import com.highlysucceed.kaluapp.vendor.android.base.BaseRecylerViewAdapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class FinderResultRecyclerViewAdapter extends BaseRecylerViewAdapter<FinderResultRecyclerViewAdapter.ViewHolder, FinderModel>{

    private Callback callback;

    public FinderResultRecyclerViewAdapter(Context context , Callback callback) {
        super(context);
        this.callback = callback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_finder_result));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.providerNameTXT.setText(holder.getItem().providerName);
        holder.clinicNameTXT.setText(holder.getItem().Clinic);
        holder.scheduleTXT.setText(startTime(holder.getItem().startTime)+" - " + endTime(holder.getItem().endTime));

        if(holder.getItem().profilePictureFullPath != null) {
            Glide.with(getContext())
                    .load(holder.getItem().profilePictureFullPath)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.ic_no_image)
                            .error(R.drawable.ic_no_image))
                    .into(holder.doctorsIMG);

        }
        else{
            Glide.with(getContext())
                    .load(R.drawable.ic_no_image)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.ic_no_image)
                            .error(R.drawable.ic_no_image))
                    .into(holder.doctorsIMG);
        }


    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.providerNameTXT)     TextView providerNameTXT;
        @BindView(R.id.clinicNameTXT)       TextView clinicNameTXT;
        @BindView(R.id.scheduleTXT)         TextView scheduleTXT;
        @BindView(R.id.viewBTN)              View viewBTN;
        @BindView(R.id.doctorsIMG)           ImageView doctorsIMG;
        String start_time, time_end;


        public ViewHolder(View view) {
            super(view);
            viewBTN.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                       callback.onItemClick(String.valueOf(getItem().clinicId), String.valueOf(getItem().providerId), getItem().startTime, getItem().endTime);
                }
            });        }

        public FinderModel getItem() {
            return (FinderModel) super.getItem();
        }
    }


  public String startTime(String time) {
      SimpleDateFormat sdf  = new SimpleDateFormat("hh:mm");
       SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm a");
       Date dt = new Date();
       String timeFormat="", tFormat="";
       String hr="", lt="";
       String [] splitTime = time.split(":");
        for(int i=0; i<splitTime.length; i++){
            if(i==0){
                int hour = Integer.parseInt(splitTime[0]);
                 if(hour<12){
                     hr = " " +hour;
                 }
                 else {
                     hr = " " +hour;
                 }
            }
            else if(i==1){
                 tFormat = hr + ":" + splitTime[1];
                try {
                    dt = sdf.parse(tFormat);
                    timeFormat = sdfs.format(dt);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }

      return timeFormat;
  }
    public String endTime(String time) {
        String timeFormat="";
        String hr="", lt="";
        String [] splitTime = time.split(":");
        for(int i=0; i<splitTime.length; i++){
            if(i==0){
                int hour = Integer.parseInt(splitTime[0]);
                if(hour<12){
                    hr = " " +hour;
                    lt = "AM";
                }
                else {
                    hr = " " +hour;
                    lt = "PM";
                }
            }
            else if(i==1){
                timeFormat = hr + ":" + splitTime[1] + " " +lt;
            }
        }

        return timeFormat;
    }

    public interface Callback{
        void onItemClick(String clinicID, String providerID, String timeStart, String timeEnd);
    }
} 
