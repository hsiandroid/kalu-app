package com.highlysucceed.kaluapp.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.data.model.api.AccreditorModel;
import com.highlysucceed.kaluapp.data.model.api.ServiceModel;
import com.highlysucceed.kaluapp.vendor.android.base.BaseRecylerViewAdapter;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ServicesRecyclerViewAdapter extends BaseRecylerViewAdapter<ServicesRecyclerViewAdapter.ViewHolder, ServiceModel> {


    public ServicesRecyclerViewAdapter(Context context) {
        super(context);

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_services_items));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.serviceType.setText(holder.getItem().name);

    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder {
        @BindView(R.id.serviceType)
        TextView serviceType;
        @BindView(R.id.servicePrice)
        TextView servicePrice;


        public ViewHolder(View view) {
            super(view);

        }

        public ServiceModel getItem() {
            return (ServiceModel) super.getItem();
        }
    }


}
