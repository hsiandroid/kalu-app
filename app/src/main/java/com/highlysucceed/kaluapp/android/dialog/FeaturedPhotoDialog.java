package com.highlysucceed.kaluapp.android.dialog;

import android.content.Context;
import android.widget.GridView;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.activity.BookingActivity;
import com.highlysucceed.kaluapp.android.adapter.FeaturedPhotoAdapter;
import com.highlysucceed.kaluapp.android.adapter.FeaturedPhotosAdapter;
import com.highlysucceed.kaluapp.data.model.api.FeturedPhotoModel;
import com.highlysucceed.kaluapp.vendor.android.base.BaseDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class FeaturedPhotoDialog extends BaseDialog {
	public static final String TAG = FeaturedPhotoDialog.class.getName().toString();

	private  List<FeturedPhotoModel> modelList = new ArrayList<>();
	private FeaturedPhotosAdapter adapter;
    private  BookingActivity bookingActivity;
    private Context context;

	@BindView(R.id.gridIMG)   GridView gridIMG;


	public static FeaturedPhotoDialog newInstance(Context mCtx, BookingActivity activity, List<FeturedPhotoModel> feturedPhotoModelList) {
		FeaturedPhotoDialog dialog = new FeaturedPhotoDialog();
		dialog.context = mCtx;
		dialog.bookingActivity = activity;
		dialog.modelList  = feturedPhotoModelList;
		return dialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_featured_photo;
	}

	@Override
	public void onViewReady() {
		setAdapter();
	}

	private void setAdapter() {
		adapter = new FeaturedPhotosAdapter(bookingActivity, modelList);
		gridIMG.setAdapter(adapter);
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}
	@OnClick(R.id.closeBTN)
	void closeBTN(){
		this.dismiss();
	}
}
