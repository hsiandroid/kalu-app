package com.highlysucceed.kaluapp.android.dialog;

import android.content.Context;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.vendor.android.base.BaseDialog;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class BloodTypeDialog extends BaseDialog {
	public static final String TAG = BloodTypeDialog.class.getName().toString();
	private Context context;
	private Callback callback;

	@BindView(R.id.numberPicker) NumberPicker numberPicker;
	@BindView(R.id.selectBTN)    TextView selectBTN;

	String[] data = new String[]{"A", "A+", "B", "B+", "O", "O+", "AB", "AB+"};


	public static BloodTypeDialog newInstance(Context mCtx, Callback callback) {
		BloodTypeDialog dialog = new BloodTypeDialog();
       dialog.context = mCtx;
       dialog.callback = callback;
		return dialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_blood_type;
	}

	@Override
	public void onViewReady() {
		numberPicker.setMinValue(0);
		numberPicker.setMaxValue(data.length-1);
		numberPicker.setDisplayedValues(data);

	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}


	public interface Callback{
		void getBloodType(String type);
	}

	@OnClick(R.id.selectBTN)
	void selectBTN(){
		int value = numberPicker.getValue();
		for(int i =0; i<data.length; i++){
             if(value == i){
             	callback.getBloodType(data[i]);
			 }
		}
		dismiss();
	}
}
