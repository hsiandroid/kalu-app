package com.highlysucceed.kaluapp.android.fragment.onethousanddays;

import android.util.Log;
import android.widget.TextView;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.activity.First1000DaysActivity;
import com.highlysucceed.kaluapp.android.dialog.BloodTypeDialog;
import com.highlysucceed.kaluapp.android.dialog.CalendarDialog;
import com.highlysucceed.kaluapp.server.request.Auth;
import com.highlysucceed.kaluapp.vendor.android.base.BaseFragment;
import com.highlysucceed.kaluapp.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class GettingStartedFragment extends BaseFragment implements CalendarDialog.DateTimePickerListener, BloodTypeDialog.Callback {
    public static final String TAG = GettingStartedFragment.class.getName().toString();
    private First1000DaysActivity activity;

    @BindView(R.id.bloodTypeET) TextView bloodTypeET;
    @BindView(R.id.mensPeriodTXT) TextView mensPeriodTXT;

    public static GettingStartedFragment newInstance() {
        GettingStartedFragment fragment = new GettingStartedFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_getting_started;
    }

    @Override
    public void onViewReady() {
        activity = (First1000DaysActivity) getContext();
        activity.setTitle("Getting Started");
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }
    @OnClick(R.id.saveBTN)
    void saveBTN(){
        activity.openTodayFragment();
    }
    @OnClick(R.id.mensPeriodTXT)
    void onBirthdayClick(){
        CalendarDialog.newInstance(this, mensPeriodTXT.getText().toString()).show(getChildFragmentManager(), TAG);
    }

   @OnClick(R.id.bloodTypeET)
   void onBloodType(){
       BloodTypeDialog.newInstance(activity,this).show(getChildFragmentManager(), TAG);
   }
    @Override
    public void forDisplay(String date) {
            mensPeriodTXT.setText(date);
    }

    @Override
    public void getBloodType(String type) {
        bloodTypeET.setText(type);
    }
}
