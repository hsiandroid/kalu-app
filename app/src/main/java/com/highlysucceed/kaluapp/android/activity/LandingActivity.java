package com.highlysucceed.kaluapp.android.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Config;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.LoggingBehavior;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.fragment.landing.LoginFragment;
import com.highlysucceed.kaluapp.android.fragment.landing.SplashFragment;
import com.highlysucceed.kaluapp.android.fragment.register.SignUpFragment;
import com.highlysucceed.kaluapp.android.route.RouteActivity;
import com.highlysucceed.kaluapp.data.model.api.UserModel;
import com.highlysucceed.kaluapp.data.preference.UserData;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import icepick.State;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class LandingActivity extends RouteActivity implements FacebookCallback<LoginResult>,
        GraphRequest.GraphJSONObjectCallback, GoogleApiClient.OnConnectionFailedListener {
    public static final String TAG = LandingActivity.class.getName().toString();

    private FBCallback fbCallback;
    private GmailCallback gmailCallback;
    private CallbackManager callbackManager;
    private static final int REQ_CODE = 123;
    private GoogleSignInClient mGoogleSignInClient;
    private   GoogleApiClient  googleApiClient;

    private static final  int RC_SIGN_IN = 1;




    @State String accessToken;


    @Override
    public int onLayoutSet() {
        return R.layout.activity_landing;
    }

    @Override
    public void onViewReady() {


//        try {
//            PackageInfo info = getPackageManager().getPackageInfo("com.highlysucceed.kaluapp", PackageManager.GET_SIGNATURES);
//            for (Signature signature : info.signatures) {
//                MessageDigest md = MessageDigest.getInstance("SHA");
//                md.update(signature.toByteArray());
//                Log.e("MY KEY HASH:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
//            }
//        } catch (PackageManager.NameNotFoundException e) {
//
//        } catch (NoSuchAlgorithmException e) {
//        }
        initFirebaseToken();
        initFacebook();
        initGoogle();
    }

    private void initGoogle() {
        String serverClientID = getString(R.string.google_server_client);
        GoogleSignInOptions gso =  new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(serverClientID)
                .requestEmail()
                .build();
        googleApiClient=new GoogleApiClient.Builder(this)
                .enableAutoManage(this,this)
                .addApi(Auth.GOOGLE_SIGN_IN_API,gso)
                .build();
    }

    private void initFirebaseToken() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this, instanceIdResult -> {
            String newToken = instanceIdResult.getToken();
            UserData.insert(UserData.REGID, newToken);
            Log.d("FCM", "Instance ID: " + newToken);
        });
    }

    public void attemptFBLogin(FBCallback fbCallback){
        this.fbCallback = fbCallback;
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
    }
    public void attemptGMLogin(GmailCallback gmailCallback){
        this.gmailCallback = gmailCallback;
        signIn();
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "login":
                openLoginFragment();
                break;
            default:
                openSplashFragment();
                break;
        }
    }

    public void openLoginFragment(){
        switchFragment(LoginFragment.newInstance());
    }
//    public void openSignUpFragment(){ switchFragment(SignUpFragment.newInstance()); }
    public void openSplashFragment(){ switchFragment(SplashFragment.newInstance()); }

    public void initFacebook(){
        FacebookSdk.sdkInitialize(getApplicationContext());
        FacebookSdk.setIsDebugEnabled(true);
        FacebookSdk.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, this);
    }

    private void signIn() {
        Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(intent,REQ_CODE);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==REQ_CODE){
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
            else {
                if (callbackManager != null) {
                    callbackManager.onActivityResult(requestCode, resultCode, data);
                }
            }

    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask){
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            String token = account.getIdToken();
            String  id = account.getId();
            String name = account.getDisplayName();
            String email = account.getEmail();

//            UserModel userItem = new UserModel();
//            userItem.accessToken = token;
//            userItem.googleID = id;
//            userItem.name = name;
//            userItem.email = email;

//            if(gmailCallback != null){
//                gmailCallback.successGmail(userItem);
//            }

            Log.e("LoginGM", ">>>GM token: " + token);
            Log.e("LoginGM", ">>>email: " +name);

        } catch (ApiException e) {
            Log.w(TAG, "Exception Sign in" + e.getMessage());
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());

        }
    }

    public String getAccessToken(){
        if(accessToken == null){
            return "";
        }
        return accessToken;
    }

    @Override
    public void onSuccess(LoginResult loginResult) {
        accessToken = AccessToken.getCurrentAccessToken().getToken();
        Log.d("fbTOKEN", accessToken);
        GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), this);
        Bundle parameters = new Bundle();
        parameters.putString("fields", "email, gender");
        graphRequest.setParameters(parameters);
        graphRequest.executeAsync();
        fbCallback.fbtokenResult(accessToken);
    }

    @Override
    public void onCancel() {
        if(fbCallback != null){
            fbCallback.failed("cancel");
        }
    }

    @Override
    public void onError(FacebookException error) {
        if(fbCallback != null){
            fbCallback.failed(error.toString());
        }
    }

    @Override
    public void onCompleted(JSONObject object, GraphResponse response) {
        Profile profile = Profile.getCurrentProfile();
        UserModel userItem = new UserModel();

        Log.d("res_json", object.toString());
        Log.d("res_obj", response.toString());

        if ( profile != null ) {
            userItem.fbID = profile.getId();
            userItem.name = profile.getName();

            Log.e("EEEEEE", profile.getName());
            Log.e("EEEEEE", profile.getId());

            if(object != null){
                try {
                    userItem.email = object.getString("email");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            if(fbCallback != null){
                fbCallback.success(userItem);
            }

        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e("connectionFailed", ">>>Failed: " + connectionResult);
    }


    public interface FBCallback{
        void success(UserModel userModel);
        void failed(String message);
        void fbtokenResult(String token);
    }
    public interface GmailCallback{
        void gmailTokenResult(String token);
    }
}
