package com.highlysucceed.kaluapp.android.activity;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.fragment.DefaultFragment;
import com.highlysucceed.kaluapp.android.fragment.onethousanddays.First1000DaysFragment;
import com.highlysucceed.kaluapp.android.fragment.onethousanddays.GettingStartedFragment;
import com.highlysucceed.kaluapp.android.fragment.onethousanddays.TodayFragment;
import com.highlysucceed.kaluapp.android.infectious_covid.PersonalInfoFragment;
import com.highlysucceed.kaluapp.android.route.RouteActivity;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class First1000DaysActivity extends RouteActivity {
    public static final String TAG = First1000DaysActivity.class.getName().toString();

    @Override
    public int onLayoutSet() {
        return R.layout.activity_1000days;
    }

    @Override
    public void onViewReady() {

    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "1stdays":
                 openFirst1000DaysFragment();
                break;

//            default:
//                openDefaultFragment();
//                break;
        }
    }

    public void openDefaultFragment(){
        switchFragment(DefaultFragment.newInstance());
    }
    public void openFirst1000DaysFragment(){
        switchFragment(First1000DaysFragment.newInstance());
    }
    public void openGettingStartedFragment(){ switchFragment(GettingStartedFragment.newInstance()); }
    public void openTodayFragment(){ switchFragment(TodayFragment.newInstance()); }

}
