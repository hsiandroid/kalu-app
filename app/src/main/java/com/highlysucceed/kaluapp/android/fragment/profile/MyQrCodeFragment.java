package com.highlysucceed.kaluapp.android.fragment.profile;

import android.graphics.Bitmap;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.WriterException;
import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.activity.ProfileActivity;
import com.highlysucceed.kaluapp.data.preference.UserData;
import com.highlysucceed.kaluapp.server.request.Auth;
import com.highlysucceed.kaluapp.vendor.android.base.BaseFragment;
import com.highlysucceed.kaluapp.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class MyQrCodeFragment extends BaseFragment {
    public static final String TAG = MyQrCodeFragment.class.getName().toString();

    private ProfileActivity activity;
    @BindView(R.id.qrView)  ImageView qrView;
    @BindView(R.id.idTXT)  TextView idTXT;


    public static MyQrCodeFragment newInstance() {
        MyQrCodeFragment fragment = new MyQrCodeFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_back_id;
    }

    @Override
    public void onViewReady() {
        activity = (ProfileActivity) getContext();
        idTXT.setText(UserData.getUserModel().qrCode);
        generateQR();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }

    @OnClick(R.id.viewBTN)
    void  viewBTN(){
        activity.startProfileActivity("digital");
        activity.onBackPressed();
    }

    private void generateQR() {
        QRGEncoder encoder = new QRGEncoder(UserData.getUserModel().qrCode,null, QRGContents.Type.TEXT,150);
        try {
            Bitmap bitmap = encoder.encodeAsBitmap();
            qrView.setImageBitmap(bitmap);
        } catch (WriterException e){
            Log.e(TAG,e.toString());
        }
    }
}
