package com.highlysucceed.kaluapp.android.infectious_covid;

import android.app.Person;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.activity.PersonalInfoActivity;
import com.highlysucceed.kaluapp.server.request.Auth;
import com.highlysucceed.kaluapp.vendor.android.base.BaseFragment;
import com.highlysucceed.kaluapp.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class HistorySignsFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = HistorySignsFragment.class.getName().toString();
    PersonalInfoActivity activity = new PersonalInfoActivity();

    @BindView(R.id.travelHistoryET)             EditText travelHistoryET;
    @BindView(R.id.exposureHistoryET)           EditText exposureHistoryET;
    @BindView(R.id.onsetET)                     EditText onsetET;
    @BindView(R.id.coughBTN)                    CheckBox coughBTN;
    @BindView(R.id.feverBTN)                    CheckBox feverBTN;
    @BindView(R.id.musclePainBTN)               CheckBox musclePainBTN;
    @BindView(R.id.weaknessBTN)                 CheckBox weaknessBTN;
    @BindView(R.id.smellBTN)                    CheckBox smellBTN;
    @BindView(R.id.tasteBTN)                    CheckBox tasteBTN;
    @BindView(R.id.diarrheaBTN)                 CheckBox diarrheaBTN;
    @BindView(R.id.continueBTN)                 Button continueBTN;

    public static HistorySignsFragment newInstance() {
        HistorySignsFragment fragment = new HistorySignsFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_history_signs;
    }

    @Override
    public void onViewReady() {
        activity = (PersonalInfoActivity) getContext();
        activity.setTitle("Book an Appointment");

        continueBTN.setOnClickListener(this);

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.continueBTN:
                activity.openPersonDetailsFragment();
                break;
        }
    }
}
