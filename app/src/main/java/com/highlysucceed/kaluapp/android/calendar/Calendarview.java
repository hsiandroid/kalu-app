package com.highlysucceed.kaluapp.android.calendar;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import com.highlysucceed.kaluapp.R;

import java.util.ArrayList;
import java.util.Date;


public class Calendarview extends  LinearLayout {

    public Calendarview(Context context)
    {
        super(context);
        initControl(context);
    }

    /**
     * Load component XML layout
     */
    private void initControl(Context context)
    {
        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        inflater.inflate(R.layout.calendar_layout, this);


    }

}


