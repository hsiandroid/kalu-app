package com.highlysucceed.kaluapp.android.dialog;

import android.content.Context;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.activity.BookingActivity;
import com.highlysucceed.kaluapp.android.adapter.AccreditorDialogRecyclerViewAdapter;
import com.highlysucceed.kaluapp.android.adapter.ClinicAccridetorRecyclerViewAdapter;
import com.highlysucceed.kaluapp.android.adapter.ServicesRecyclerViewAdapter;
import com.highlysucceed.kaluapp.data.model.api.AccridetorModel;
import com.highlysucceed.kaluapp.vendor.android.base.BaseDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class HMOAccreditedDialog extends BaseDialog {
	public static final String TAG = HMOAccreditedDialog.class.getName().toString();

	private AccreditorDialogRecyclerViewAdapter accreditorDialogRecyclerViewAdapter;
	private LinearLayoutManager linearLayoutManager;
	private List<AccridetorModel> accridetorModelList =  new ArrayList<>();
	private BookingActivity activity;
	private Context context;
	private String  databaseEvent;
	@BindView(R.id.recyclerList)   RecyclerView recyclerList;


	public static HMOAccreditedDialog newInstance(Context ctx, BookingActivity activity, List<AccridetorModel> data, String detail) {
		HMOAccreditedDialog dialog = new HMOAccreditedDialog();
		dialog.context = ctx;
		dialog.activity =activity;
		dialog.accridetorModelList = data;
		dialog.databaseEvent = detail;
		return dialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_hmo;
	}

	@Override
	public void onViewReady() {
		setUpListView();
	}

	private void setUpListView() {
		accreditorDialogRecyclerViewAdapter = new AccreditorDialogRecyclerViewAdapter(getContext());
		linearLayoutManager = new LinearLayoutManager(getContext());
		recyclerList.setLayoutManager(linearLayoutManager);
		recyclerList.setAdapter(accreditorDialogRecyclerViewAdapter);

		if(databaseEvent.equals("new")){
			accreditorDialogRecyclerViewAdapter.addNewData(accridetorModelList);
		}
		else if(databaseEvent.equals("refresh")){
			accreditorDialogRecyclerViewAdapter.setNewData(accridetorModelList);
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}

	@OnClick(R.id.closeBTN)
	void closeBTN(){
		this.dismiss();
	}
}
