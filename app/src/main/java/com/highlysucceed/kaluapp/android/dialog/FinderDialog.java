package com.highlysucceed.kaluapp.android.dialog;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.TextView;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.activity.BookingActivity;
import com.highlysucceed.kaluapp.data.model.api.OccupationModel;
import com.highlysucceed.kaluapp.vendor.android.base.BaseDialog;
import com.highlysucceed.kaluapp.vendor.android.java.Log;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class FinderDialog extends BaseDialog implements ProviderDialog.Callback, CalendarInquiryDialog.DateTimePickerListener {
	public static final String TAG = FinderDialog.class.getName().toString();

	private BookingActivity  activity;
	private Context context;
	private Callback callback;
	private String [] listofOccupation;
	List<OccupationModel> occupationModelArrayList = new ArrayList<>();

	@BindView(R.id.providerEDT)   EditText providerEDT;
	@BindView(R.id.locationEDT)   EditText locationEDT;
	@BindView(R.id.dateTXT)       EditText dateTXT;
	@BindView(R.id.insuranceEDT)  EditText insuranceEDT;
	String occupationId= "";


	public static FinderDialog newInstance(BookingActivity activity, Context context, Callback callback, List<OccupationModel> occupationModelList) {
		FinderDialog dialog = new FinderDialog();
		dialog.activity = activity;
		dialog.context = context;
		dialog.callback = callback;
		dialog.occupationModelArrayList = occupationModelList;
		return dialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_finder;
	}

	@Override
	public void onViewReady() {

	}


	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}

	@Override
	public void providerCallback(ProviderDialog dialog, String id, String name) {
		dialog.dismiss();
		occupationId = id;
		providerEDT.setText(name);
	}

	@Override
	public void forDisplay(String date) {
          dateTXT.setText(date);
	}

	public interface Callback{
		void finderResult(FinderDialog dialog, String id, String loc, String date, String insurance);
	}

    @OnClick(R.id.providerEDT)
	void providerBTN(){
		ProviderDialog.newInstance(context, this, occupationModelArrayList).show(getChildFragmentManager(), TAG);
	}
	@OnClick(R.id.dateTXT)
	void dateTXT(){
		CalendarInquiryDialog.newInstance(this, dateTXT.getText().toString().trim()).show(getChildFragmentManager(), TAG);
	}

	@OnClick(R.id.findBTN)
	void findBTN(){
		callback.finderResult(this, occupationId, locationEDT.getText().toString().trim(), dateTXT.getText().toString().trim(), insuranceEDT.getText().toString().trim());
	}
	@OnClick(R.id.closeBTN)
	void closeBTN(){
		this.dismiss();
	}
}
