package com.highlysucceed.kaluapp.android.infectious_covid;

import android.widget.TextView;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.activity.PersonalInfoActivity;
import com.highlysucceed.kaluapp.data.model.api.InfoModel;
import com.highlysucceed.kaluapp.data.preference.UserData;
import com.highlysucceed.kaluapp.server.request.Infectious;
import com.highlysucceed.kaluapp.vendor.android.base.BaseFragment;
import com.highlysucceed.kaluapp.vendor.server.request.APIRequest;
import com.highlysucceed.kaluapp.vendor.server.transformer.CollectionTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class CredentialsFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {
    public static final String TAG = CredentialsFragment.class.getName().toString();

    @BindView(R.id.pndTXT)             TextView pndTXT;
    @BindView(R.id.pcrDateTXT)         TextView pcrDateTXT;
    @BindView(R.id.negativePcrTXT)     TextView negativePcrTXT;
    @BindView(R.id.positivePcrTXT)     TextView positivePcrTXT;
    @BindView(R.id.presultTXT)         TextView presultTXT;
    @BindView(R.id.mdTXT)              TextView mdTXT;
    @BindView(R.id.eeTXT)              TextView eeTXT;
    @BindView(R.id.homeSRL)            SwipeRefreshLayout homeSRL;
    private PersonalInfoActivity activity;
    private APIRequest apiRequest;

    public static CredentialsFragment newInstance() {
        CredentialsFragment fragment = new CredentialsFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_credentials;
    }

    @Override
    public void onViewReady() {
        activity = (PersonalInfoActivity) getContext();
        activity.setTitle("Credentials");
        homeSRL.setOnRefreshListener(this);
    }
    @Override
    public void onResume() {
        super.onResume();
        onRefresh();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Infectious.LoginResponse response){
        CollectionTransformer<InfoModel> transformer = response.getData(CollectionTransformer.class);
        if(transformer.status){

            if (!transformer.data.get(0).CREDENTIALS.pcrNotDone.equals("")) {
                pndTXT.setText(transformer.data.get(0).CREDENTIALS.pcrNotDone);
            }else {
                pndTXT.setText("--");
            }

            if (!transformer.data.get(0).CREDENTIALS.date.equals("")) {
                pcrDateTXT.setText(transformer.data.get(0).CREDENTIALS.date);
            }else {
                pcrDateTXT.setText("--");
            }

            if (!transformer.data.get(0).CREDENTIALS.pcrNotDone.equals("")) {
                pndTXT.setText(transformer.data.get(0).CREDENTIALS.pcrNotDone);
            }else {
                pndTXT.setText("--");
            }

            if (!transformer.data.get(0).CREDENTIALS.negative.equals("")) {
                negativePcrTXT.setText(transformer.data.get(0).CREDENTIALS.negative);
            }else {
                negativePcrTXT.setText("--");
            }

            if (!transformer.data.get(0).CREDENTIALS.positive.equals("")) {
                positivePcrTXT.setText(transformer.data.get(0).CREDENTIALS.positive);
            }else {
                positivePcrTXT.setText("--");
            }

            if (!transformer.data.get(0).CREDENTIALS.pendingResult.equals("")) {
                presultTXT.setText(transformer.data.get(0).CREDENTIALS.pendingResult);
            }else {
                presultTXT.setText("--");
            }

            if (!transformer.data.get(0).CREDENTIALS.nameMD.equals("")) {
                mdTXT.setText(transformer.data.get(0).CREDENTIALS.nameMD);
            }else {
                mdTXT.setText("--");
            }

            if (!transformer.data.get(0).CREDENTIALS.nameEE.equals("")) {
                eeTXT.setText(transformer.data.get(0).CREDENTIALS.nameEE);
            }else {
                eeTXT.setText("--");
            }


        }
    }

    @Override
    public void onRefresh() {
          refreshList();
    }
    public void refreshList(){
        apiRequest = Infectious.getDefault().getProfileDetails(activity, String.valueOf(UserData.getUserModel().userId), homeSRL);
        apiRequest.first();
    }
}
