package com.highlysucceed.kaluapp.android.fragment.clinic;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.activity.BookingActivity;
import com.highlysucceed.kaluapp.android.adapter.ClinicListRecyclerViewAdapter;
import com.highlysucceed.kaluapp.android.adapter.HospitalRecyclerViewAdapter;
import com.highlysucceed.kaluapp.android.adapter.NearbyListRecyclerViewAdapter;
import com.highlysucceed.kaluapp.android.dialog.FinderDialog;
import com.highlysucceed.kaluapp.config.Keys;
import com.highlysucceed.kaluapp.data.model.api.ClinicModel;
import com.highlysucceed.kaluapp.data.model.api.FinderModel;
import com.highlysucceed.kaluapp.data.model.api.HolderModel;
import com.highlysucceed.kaluapp.data.model.api.HospitalModel;
import com.highlysucceed.kaluapp.data.model.api.NearbyModel;
import com.highlysucceed.kaluapp.data.model.api.OccupationModel;
import com.highlysucceed.kaluapp.server.request.Clinic;
import com.highlysucceed.kaluapp.server.request.Practitioner;
import com.highlysucceed.kaluapp.vendor.android.base.BaseFragment;
import com.highlysucceed.kaluapp.vendor.android.java.ToastMessage;
import com.highlysucceed.kaluapp.vendor.server.request.APIRequest;
import com.highlysucceed.kaluapp.vendor.server.transformer.CollectionTransformer;
import com.highlysucceed.kaluapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.security.Key;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ClinicFragment extends BaseFragment implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, ClinicListRecyclerViewAdapter.ClinicListener, HospitalRecyclerViewAdapter.Callback,  FinderDialog.Callback {
    public static final String TAG = ClinicFragment.class.getName().toString();


    private HospitalRecyclerViewAdapter hospitalRecyclerViewAdapter;
    private NearbyListRecyclerViewAdapter nearbyListRecyclerViewAdapter;
    private LinearLayoutManager linearLayoutManager;
    private ClinicListRecyclerViewAdapter clinicListRecyclerViewAdapter;
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private SupportMapFragment mapFragment;
    private int PROXIMITY_RADIUS = 10000;
    private Marker mCurrLocationMarker;
    private BookingActivity activity;
    private Location mLastLocation;
    private GoogleMap mMap, showMap;
    private boolean locationEnabled = false;
    private  List<FinderModel> modelList = new ArrayList<>();
    private  List<OccupationModel> occupationModelList = new ArrayList<>();
    private  FinderDialog dialog;
    private  List<HolderModel> holderModelList = new ArrayList<>();
    private APIRequest apiRequest;


    @BindView(R.id.recyclerAllList)        RecyclerView recyclerAllList;
    @BindView(R.id.recyclerSearchClinic)   RecyclerView recyclerSearchClinic;
    @BindView(R.id.titleheaderVIEW)        View titleheaderVIEW;
    @BindView(R.id.searchHeaderVIEW)       View searchHeaderVIEW;
    @BindView(R.id.mainLayout)             View mainLayout;
    @BindView(R.id.searchList)             View searchList;
    @BindView(R.id.activityTitleTXT)       TextView activityTitleTXT;
    @BindView(R.id.allBTN)                 TextView allBTN;
    @BindView(R.id.nearbyBTN)                TextView nearbyBTN;
    @BindView(R.id.backButton)             ImageView backButton;
    @BindView(R.id.removeBTN)              ImageView removeBTN;
    @BindView(R.id.searchEDT)              EditText searchEDT;
    @BindView(R.id.progress_bar)           ProgressBar progress_bar;
    @BindView(R.id.shimmerFrameLayout)     ShimmerFrameLayout shimmerFrameLayout;

    private String occupationId = "", location="",  insurance="",date="";



    public static ClinicFragment newInstance() {
        ClinicFragment fragment = new ClinicFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_clinic;
    }

    @Override
    public void onViewReady() {
        activity = (BookingActivity) getContext();
        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.fragmentMap);
        mapFragment.getMapAsync(this);
        shimmerFrameLayout.startShimmer();
        setUpListView();
        setUpSearchList();
        checkLocationPermission();
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }


    @Override
    public void onResume() {
        super.onResume();
        activityTitleTXT.setText("Clinic");
        initAllClinic();
//        Practitioner.getDefault().getOccupationList(activity);
    }

    private void initAllClinic() {
        apiRequest = Clinic.getDefault().AllClinic(activity);
        apiRequest.setPerPage(5);
        apiRequest.first();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        //Initialize Google Play Services
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(activity,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }


    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(activity)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();

    }

    @Override
    public void onConnected(Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(activity,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(activity,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }


    public static Boolean isLocationEnabled(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
// This is new method provided in API 28
            LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            return lm.isLocationEnabled();
        } else {
// This is Deprecated in API 28
            int mode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE,
                    Settings.Secure.LOCATION_MODE_OFF);
            return (mode != Settings.Secure.LOCATION_MODE_OFF);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();

        }
        //Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
        mCurrLocationMarker = mMap.addMarker(markerOptions);

        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(11));

        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Subscribe
    public void onResponse(Clinic.ALlClinicReponse response) {
        CollectionTransformer<ClinicModel> transformer = response.getData(CollectionTransformer.class);
        if (transformer.status) {
            shimmerFrameLayout.setVisibility(View.GONE);
            recyclerAllList.setVisibility(View.VISIBLE);
            if(response.isNext()){
                hospitalRecyclerViewAdapter.addNewData(transformer.data);
            }
            else{
                hospitalRecyclerViewAdapter.setNewData(transformer.data);
            }
        }
    }
    @Subscribe
    public void onResponse(Clinic.ALlClinicDetailsReponse response) {
        CollectionTransformer<ClinicModel> transformer = response.getData(CollectionTransformer.class);
        if (transformer.status) {
            shimmerFrameLayout.setVisibility(View.GONE);
            recyclerAllList.setVisibility(View.VISIBLE);
            if(response.isNext()){
                AllNearby(transformer.data);
                hospitalRecyclerViewAdapter.addNewData(transformer.data);
            }
            else{
                AllNearby(transformer.data);
                hospitalRecyclerViewAdapter.setNewData(transformer.data);
            }
        }
    }



    @Subscribe
    public void onResponse(Clinic.SearchResponse response) {
        CollectionTransformer<ClinicModel> transformer = response.getData(CollectionTransformer.class);
        if (transformer.status) {
            if (response.isNext()) {
                progress_bar.setVisibility(View.VISIBLE);
                recyclerSearchClinic.setVisibility(View.VISIBLE);
                clinicListRecyclerViewAdapter.addNewData(transformer.data);
            } else {
                progress_bar.setVisibility(View.VISIBLE);
                recyclerSearchClinic.setVisibility(View.VISIBLE);
                clinicListRecyclerViewAdapter.setNewData(transformer.data);
            }
        }
    }


    @Subscribe
    public void onResponse(Practitioner.OccupationList response){
        CollectionTransformer<OccupationModel> transformer = response.getData(CollectionTransformer.class);
        if(transformer.status){
                for(int i=0; i<transformer.data.size(); i++){
                    OccupationModel occupationModel = new OccupationModel();
                    occupationModel.name = transformer.data.get(i).name;
                    occupationModelList.add(occupationModel);
                }
        }
    }

    @Subscribe
    public void onResponse(Practitioner.FinderResultResponse response){
        CollectionTransformer<FinderModel> transformer = response.getData(CollectionTransformer.class);
        if(transformer.status){
            dialog.dismiss();
            activity.startFinderActivity("finder", occupationId, location, date, insurance);
        }
        else {
            ToastMessage.show(getContext(), "No Doctor Available", ToastMessage.Status.FAILED);
        }
    }

    private void setUpListView() {
        allBTN.setTextColor(ContextCompat.getColor(activity, R.color.white));
        allBTN.setBackgroundResource(0);
        allBTN.setBackgroundResource(R.drawable.bg_blue_rounded_corners);

        nearbyBTN.setTextColor(ContextCompat.getColor(activity, R.color.black50));
        nearbyBTN.setBackgroundResource(0);
        nearbyBTN.setBackgroundResource(R.drawable.bg_gray_rounded_corners);

        hospitalRecyclerViewAdapter = new HospitalRecyclerViewAdapter(getContext(), this);
        linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerAllList.setLayoutManager(linearLayoutManager);
        recyclerAllList.setAdapter(hospitalRecyclerViewAdapter);
    }

    private void setUpSearchList() {
        clinicListRecyclerViewAdapter = new ClinicListRecyclerViewAdapter(getContext(), this);
        linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerSearchClinic.setLayoutManager(linearLayoutManager);
        recyclerSearchClinic.setAdapter(clinicListRecyclerViewAdapter);
    }

    public void AllNearby(List<ClinicModel> data) {

        for (int i = 0; i < data.size(); i++) {
            double lat = Double.parseDouble(data.get(i).latitude);
            double lng = Double.parseDouble(data.get(i).longitude);
            String placename = data.get(i).name;
            String id = String.valueOf(data.get(i).id);
            LatLng latLng = new LatLng(lat, lng);

            Marker marker = mMap.addMarker(new MarkerOptions().position(latLng).title(placename).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)).snippet("Book Now"));

                mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                    @Override
                    public void onInfoWindowClick(Marker marker) {
                           activity.openClinicDetails(id);

                    }
                });

            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(11));
        }
    }

    @OnClick(R.id.nearbyBTN)
    void nearbyBTN() {
            if (isLocationEnabled(activity)) {
                String Lat = String.valueOf(mLastLocation.getLatitude());
                String Lng = String.valueOf(mLastLocation.getLongitude());
                mMap.clear();

                nearbyBTN.setTextColor(ContextCompat.getColor(activity, R.color.white));
                nearbyBTN.setBackgroundResource(0);
                nearbyBTN.setBackgroundResource(R.drawable.bg_blue_rounded_corners);
                allBTN.setTextColor(ContextCompat.getColor(activity, R.color.black50));
                allBTN.setBackgroundResource(0);
                allBTN.setBackgroundResource(R.drawable.bg_gray_rounded_corners);

                shimmerFrameLayout.setVisibility(View.VISIBLE);
                shimmerFrameLayout.startShimmer();
                recyclerAllList.setVisibility(View.GONE);

                apiRequest = Clinic.getDefault().AllClinicDetails(activity);
                apiRequest.addParameter(Keys.LAT, Lat);
                apiRequest.addParameter(Keys.LONG, Lng);
                apiRequest.setPerPage(5);
                apiRequest.first();


            } else {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);
                // Setting Dialog Title
                alertDialog.setTitle("Allow 'Kalu' to access this device's location?");

                // Setting Dialog Message
                alertDialog.setMessage("This app will only have access to the location while you're using the app.");

                // On pressing Settings button
                alertDialog.setPositiveButton(
                        "Allow",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(
                                        Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(intent);
                            }
                        });
                alertDialog.setNegativeButton("Deny", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        ;
                    }
                });

                alertDialog.show();
            }

    }

    @OnClick(R.id.allBTN)
    void allBTN(){
        mMap.clear();
        allBTN.setTextColor(ContextCompat.getColor(activity, R.color.white));
        allBTN.setBackgroundResource(0);
        allBTN.setBackgroundResource(R.drawable.bg_blue_rounded_corners);

        nearbyBTN.setTextColor(ContextCompat.getColor(activity, R.color.black50));
        nearbyBTN.setBackgroundResource(0);
        nearbyBTN.setBackgroundResource(R.drawable.bg_gray_rounded_corners);
        holderModelList.clear();
        shimmerFrameLayout.setVisibility(View.VISIBLE);
        shimmerFrameLayout.startShimmer();
        initAllClinic();
    }

    @OnClick(R.id.searchBTN)
    void searchBTN() {
//        titleheaderVIEW.setVisibility(View.GONE);
//        mainLayout.setVisibility(View.GONE);
//        searchHeaderVIEW.setVisibility(View.VISIBLE);
//        searchList.setVisibility(View.VISIBLE);
//        Clinic.getDefault().getAllClinic(activity);
//        textSearch();
         FinderDialog.newInstance(activity, getContext(), this, occupationModelList).show(getChildFragmentManager(), TAG);

    }


    @OnClick(R.id.backButton)
    void backButton() {
        activity.closeKeyboard();
        searchHeaderVIEW.setVisibility(View.GONE);
        mainLayout.setVisibility(View.VISIBLE);
        titleheaderVIEW.setVisibility(View.VISIBLE);
        searchList.setVisibility(View.GONE);
    }



    @Override
    public void onSuccess(String id) {
        activity.openClinicDetails(id);
    }



    @Override
    public void setType(String id) {
            activity.openClinicDetails(id);
    }

    @Override
    public void setContactNumber(String number) {
        try {
            String s = number.substring(1);
            Intent intent = new Intent(Intent.ACTION_DIAL);
            String cNum = "+63" + s;
            intent.setData(Uri.parse("tel:" + cNum));
            startActivity(intent);
        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
        }
    }

    @OnClick(R.id.activityBackBTN)
    void activityBackBTN() {
        activity.onBackPressed();
    }


    @Override
    public void finderResult(FinderDialog dialog, String id, String loc, String date, String insurance) {
        this.dialog = dialog;
        this.occupationId = id;
        this.location = loc;
        this.date = date;
        this.insurance = insurance;
        Practitioner.getDefault().getProvidersAvailability(activity, id, loc, date, insurance);
    }
}
