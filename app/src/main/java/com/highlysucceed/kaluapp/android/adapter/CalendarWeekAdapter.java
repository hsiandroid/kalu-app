package com.highlysucceed.kaluapp.android.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.highlysucceed.kaluapp.R;

public class CalendarWeekAdapter extends BaseAdapter {
    private Context context;
    private String [] cDays;
    LayoutInflater inflter;
    private TextView daysTXT;

    public CalendarWeekAdapter(Context mContext , String [] days) {
        this.context = mContext;
        this.cDays = days;
        inflter = (LayoutInflater.from(mContext));
    }

    @Override
    public int getCount() {
        return cDays.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if(view == null){
            daysTXT = new TextView(context);
            daysTXT.setLayoutParams(new GridView.LayoutParams(65, 65));
            daysTXT.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL);
            daysTXT.setTextColor(ContextCompat.getColor(context, R.color.white));
            daysTXT.setPadding(2, 2, 2, 2);

        }
        else {
            daysTXT = (TextView) view;
        }
        daysTXT.setText(cDays[i]);

        return daysTXT;
    }


}
