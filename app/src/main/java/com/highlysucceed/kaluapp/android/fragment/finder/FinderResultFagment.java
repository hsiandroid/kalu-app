package com.highlysucceed.kaluapp.android.fragment.finder;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.android.activity.BookingActivity;
import com.highlysucceed.kaluapp.android.activity.FinderActivity;
import com.highlysucceed.kaluapp.android.adapter.ClinicRecommendedRecyclerViewAdapter;
import com.highlysucceed.kaluapp.android.adapter.ExpertiseListRecyclerViewAdapter;
import com.highlysucceed.kaluapp.android.adapter.FinderResultRecyclerViewAdapter;
import com.highlysucceed.kaluapp.data.model.api.DataModel;
import com.highlysucceed.kaluapp.data.model.api.FinderModel;
import com.highlysucceed.kaluapp.server.request.Auth;
import com.highlysucceed.kaluapp.server.request.Clinic;
import com.highlysucceed.kaluapp.server.request.Practitioner;
import com.highlysucceed.kaluapp.vendor.android.base.BaseFragment;
import com.highlysucceed.kaluapp.vendor.android.java.ToastMessage;
import com.highlysucceed.kaluapp.vendor.server.request.APIRequest;
import com.highlysucceed.kaluapp.vendor.server.transformer.BaseTransformer;
import com.highlysucceed.kaluapp.vendor.server.transformer.CollectionTransformer;
import com.highlysucceed.kaluapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class FinderResultFagment extends BaseFragment implements FinderResultRecyclerViewAdapter.Callback, SwipeRefreshLayout.OnRefreshListener {
    public static final String TAG = FinderResultFagment.class.getName().toString();

    private FinderResultRecyclerViewAdapter finderResultRecyclerViewAdapter;
    private LinearLayoutManager linearLayoutManager;
    private FinderActivity activity;
    private APIRequest apiRequest;
    @BindView(R.id.homeSRL)   SwipeRefreshLayout homeSRL;

    @BindView(R.id.recyclerFinder)       RecyclerView recyclerFinder;
    String occupationID="", location="", date="", hmo="";


    public static FinderResultFagment newInstance(String id, String location, String date, String hmo) {
        FinderResultFagment fragment = new FinderResultFagment();
        fragment.occupationID = id;
        fragment.location = location;
        fragment.date = date;
        fragment.hmo = hmo;
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_finder_result;
    }

    @Override
    public void onViewReady() {
        activity  = (FinderActivity) getContext();
        activity.setTitle("Provider Details");
         setData();
        homeSRL.setOnRefreshListener(this);
    }

    private void setData() {
        finderResultRecyclerViewAdapter = new FinderResultRecyclerViewAdapter(getContext(), this);
        linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerFinder.setLayoutManager(linearLayoutManager);
        recyclerFinder.setAdapter(finderResultRecyclerViewAdapter);

    }

    @Override
    public void onResume() {
        super.onResume();

        onRefresh();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Practitioner.ResultResponse response){
        CollectionTransformer<FinderModel> transformer = response.getData(CollectionTransformer.class);
        if(transformer.status){
             if(response.isNext()){
                 finderResultRecyclerViewAdapter.addNewData(transformer.data);
             }
             else {
                 finderResultRecyclerViewAdapter.setNewData(transformer.data);
             }
        }
        else {
            ToastMessage.show(getContext(), transformer.msg, ToastMessage.Status.FAILED);
        }
    }


    @Override
    public void onItemClick(String clinicID, String providerID, String timeStart, String timeEnd) {
        activity.openFastBookingFragment(clinicID,providerID,date,timeStart,timeEnd);
    }

    @Override
    public void onRefresh() {
        refreshList();
    }
    public void refreshList(){
        apiRequest =  Practitioner.getDefault().providersAvailabilty(activity, occupationID, location, date, hmo, homeSRL);
        apiRequest.first();

    }
}
