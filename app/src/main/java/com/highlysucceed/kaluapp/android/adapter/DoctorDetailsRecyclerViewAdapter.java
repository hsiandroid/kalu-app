package com.highlysucceed.kaluapp.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.highlysucceed.kaluapp.R;
import com.highlysucceed.kaluapp.data.model.api.DoctorModel;
import com.highlysucceed.kaluapp.data.model.api.ProviderModel;
import com.highlysucceed.kaluapp.vendor.android.base.BaseRecylerViewAdapter;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class DoctorDetailsRecyclerViewAdapter extends BaseRecylerViewAdapter<DoctorDetailsRecyclerViewAdapter.ViewHolder, ProviderModel> {

    private DoctorListener listener;

    public DoctorDetailsRecyclerViewAdapter(Context context, DoctorListener listener1) {
        super(context);
        this.listener = listener1;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_doctor_details));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
//        holder.doctorIMG.setImageResource(getItem(position).getImage());;
        holder.doctorName.setText(holder.getItem().name);
        holder.doctorDetails.setText(holder.getItem().expertise);

        if(!holder.getItem().profilePicture.fullPath.equals("")) {
            Glide.with(getContext())
                    .load(holder.getItem().profilePicture.fullPath)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.ic_no_image)
                            .error(R.drawable.ic_no_image))
                    .into(holder.doctorIMG);

        }
        else{
            Glide.with(getContext())
                    .load(R.drawable.ic_no_image)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.ic_no_image)
                            .error(R.drawable.ic_no_image))
                    .into(holder.doctorIMG);
        }


    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder implements View.OnClickListener {

        @BindView(R.id.doctorIMG)   ImageView doctorIMG;
        @BindView(R.id.doctorName)  TextView doctorName;
        @BindView(R.id.doctorDetails) TextView doctorDetails;

        public ViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
        }

        public ProviderModel getItem() {
            return (ProviderModel) super.getItem();
        }

        @Override
        public void onClick(View v) {
            listener.doctorId(String.valueOf(getItem().id));
        }
    }

    public void setClickListener(DoctorListener clickListener) {
        this.listener = clickListener;
    }

    public interface DoctorListener{
        void doctorId( String id);
    }

}
