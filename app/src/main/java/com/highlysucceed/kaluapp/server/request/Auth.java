package com.highlysucceed.kaluapp.server.request;

import android.content.Context;

import com.highlysucceed.kaluapp.config.Keys;
import com.highlysucceed.kaluapp.config.Url;
import com.highlysucceed.kaluapp.data.model.api.ClinicModel;
import com.highlysucceed.kaluapp.data.model.api.InfoModel;
import com.highlysucceed.kaluapp.data.model.api.SampleModel;
import com.highlysucceed.kaluapp.data.model.api.UserModel;
import com.highlysucceed.kaluapp.data.preference.UserData;
import com.highlysucceed.kaluapp.vendor.server.request.APIRequest;
import com.highlysucceed.kaluapp.vendor.server.request.APIResponse;
import com.highlysucceed.kaluapp.vendor.server.transformer.BaseTransformer;
import com.highlysucceed.kaluapp.vendor.server.transformer.CollectionTransformer;
import com.highlysucceed.kaluapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by Labyalo on 8/3/2017.
 */

public class Auth {

    public static Auth getDefault(){
        return new Auth();
    }

    public void login(Context context, String email, String password) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserModel>>(context) {
            @Override
            public Call<SingleTransformer<UserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.getLogin(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new LoginResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.EMAIL, email)
                .addParameter(Keys.PASSWORD, password)
                .showDefaultProgressDialog("Logging in...")
                .execute();

    }
    public void fbLogin(Context context, String accessToken) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserModel>>(context) {
            @Override
            public Call<SingleTransformer<UserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.getFBLogin(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new FBResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.ACCESS_TOKEN, accessToken)
                .showDefaultProgressDialog("Logging in...")
                .execute();
    }

    public void logout(Context context) {
        APIRequest apiRequest = new APIRequest<BaseTransformer>(context) {
            @Override
            public Call<BaseTransformer> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestBaseTransformer(Url.getLogout(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new LogoutResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .showDefaultProgressDialog("Logging out...")
                .execute();
    }

    public APIRequest register(Context context) {
        APIRequest apiRequest = new APIRequest<BaseTransformer>(context) {
            @Override
            public Call<BaseTransformer> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestBaseTransformer(Url.getRegister(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new RegisterResponse(this));
            }
        };

        apiRequest
                .showDefaultProgressDialog("Register...");

        return apiRequest;
    }
    public APIRequest preRegister(Context context) {
        APIRequest apiRequest = new APIRequest<BaseTransformer>(context) {
            @Override
            public Call<BaseTransformer> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestBaseTransformer(Url.preRegister(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new SingleResponse(this));
            }
        };

        apiRequest
                .showDefaultProgressDialog("Validating...");

        return apiRequest;
    }

    public void checkLogin(Context context) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserModel>>(context) {
            @Override
            public Call<SingleTransformer<UserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.checkLogin(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new CheckResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .execute();
    }

    public void refreshToken(Context context) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserModel>>(context) {
            @Override
            public Call<SingleTransformer<UserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.getRefreshToken(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new RefreshTokenResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .execute();

    }

    public void update_avatar(Context context, File file, String user_id) {
        APIRequest apiRequest = new APIRequest<BaseTransformer>(context) {
            @Override
            public Call<BaseTransformer> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestBaseTransformer(Url.updateAvatar(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new SingleResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.FILE, file)
                .addParameter(Keys.USER_ID, user_id)
                .showDefaultProgressDialog("Updating Avatar...")
                .execute();

    }

    public APIRequest updatePassword(Context context) {
        APIRequest apiRequest = new APIRequest<BaseTransformer>(context) {
            @Override
            public Call<BaseTransformer> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestBaseTransformer(Url.updatePassword(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new PasswordResponse(this));
            }
        };

        apiRequest
                .showDefaultProgressDialog("Updating ...");

        return apiRequest;


    }

    public APIRequest updateProfile(Context context) {
        APIRequest apiRequest = new APIRequest<BaseTransformer>(context) {
            @Override
            public Call<BaseTransformer> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestBaseTransformer(Url.editProfile(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new UpdateResponse(this));
            }
        };

        apiRequest
                .showDefaultProgressDialog("Updating ...");

        return apiRequest;
    }

    public void getProfileDetails(Context context) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<InfoModel>>(context) {
            @Override
            public Call<CollectionTransformer<InfoModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestprofileDetails(Url.getProfileDetails(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new ProfileDetailsResponse(this));
            }
        };
            apiRequest.
                    addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                    .showDefaultProgressDialog("Loading ...")
                    .execute();



    }




    public static void facebook(SampleModel userModel) {

    }


    public interface RequestService {
        @Multipart
        @POST("{p}")
        Call<SingleTransformer<UserModel>> requestSingleTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<BaseTransformer> requestBaseTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<ClinicModel>> hospitalList(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<InfoModel>> requestprofileDetails(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);


    }


    public class LoginResponse extends APIResponse<SingleTransformer<UserModel>> {
        public LoginResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
    public class ProfileDetailsResponse extends APIResponse<CollectionTransformer<InfoModel>> {
        public ProfileDetailsResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
    public class LogoutResponse extends APIResponse<BaseTransformer> {
        public LogoutResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
    public class RegisterResponse extends APIResponse<BaseTransformer> {
        public RegisterResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
    public class SingleResponse extends APIResponse<BaseTransformer> {
        public SingleResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class UpdateResponse extends APIResponse<BaseTransformer> {
        public UpdateResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
    public class PasswordResponse extends APIResponse<BaseTransformer> {
        public PasswordResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
    public class HospitalList extends APIResponse<CollectionTransformer<ClinicModel>> {
        public HospitalList(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
    public class CheckResponse extends APIResponse<SingleTransformer<UserModel>> {
        public CheckResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
    public class RefreshTokenResponse extends APIResponse<SingleTransformer<UserModel>> {
        public RefreshTokenResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
    public class FBResponse extends APIResponse<SingleTransformer<UserModel>> {
        public FBResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
}
