package com.highlysucceed.kaluapp.server.request;

import android.content.Context;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.highlysucceed.kaluapp.config.Keys;
import com.highlysucceed.kaluapp.config.Url;
import com.highlysucceed.kaluapp.data.model.api.ClinicModel;
import com.highlysucceed.kaluapp.data.model.api.ClinicsModel;
import com.highlysucceed.kaluapp.data.model.api.FinderModel;
import com.highlysucceed.kaluapp.data.model.api.OccupationModel;
import com.highlysucceed.kaluapp.data.model.api.PractitionerModel;
import com.highlysucceed.kaluapp.data.model.api.UserModel;
import com.highlysucceed.kaluapp.vendor.server.request.APIRequest;
import com.highlysucceed.kaluapp.vendor.server.request.APIResponse;
import com.highlysucceed.kaluapp.vendor.server.transformer.CollectionTransformer;
import com.highlysucceed.kaluapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public class Practitioner {

    public static Practitioner getDefault(){
        return new Practitioner();
    }


    public void getOccupationList(Context context) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<OccupationModel>>(context) {
            @Override
            public Call<CollectionTransformer<OccupationModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestCollectionTransformer(Url.getOccupationList(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new OccupationList(this));
            }
        };
        apiRequest
                .execute();


    }

    public APIRequest getClinicList(Context context, SwipeRefreshLayout refreshLayout) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<ClinicModel>>(context) {
            @Override
            public Call<SingleTransformer<ClinicModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestClinicListTransformer(Url.getClinicList(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new ClinicList(this));
            }
        };

        apiRequest
                .setSwipeRefreshLayout(refreshLayout)
                .showSwipeRefreshLayout(true);
                return apiRequest;


    }
    public void getProviderList(Context context, String id) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<PractitionerModel>>(context) {
            @Override
            public Call<CollectionTransformer<PractitionerModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestPractitionerListTransformer(Url.getProviderList(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new PractitionerList(this));
            }
        };

        apiRequest
                .addParameter(Keys.OCCUPATION_ID, id)
                .showDefaultProgressDialog("Loading . . . ")
                .execute();


    }
    public void getProviderLists(Context context, String id) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<PractitionerModel>>(context) {
            @Override
            public Call<CollectionTransformer<PractitionerModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestPractitionerListTransformer(Url.getProviderList(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new PractitionersList(this));
            }
        };

        apiRequest
                .addParameter(Keys.OCCUPATION_ID, id)
                .showDefaultProgressDialog("Loading . . . ")
                .execute();


    }
    public void getProvidersAvailability(Context context, String id, String loc, String date, String insurance) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<FinderModel>>(context) {
            @Override
            public Call<CollectionTransformer<FinderModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requesFinderTransformer(Url.getProviderAvailability(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new FinderResultResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.OCCUPATION_ID, id)
                .addParameter(Keys.LOCATION, loc)
                .addParameter(Keys.DATE, date)
                .addParameter(Keys.INSURANCE, insurance)
                .showDefaultProgressDialog("Loading . . . ")
                .execute();


    }

    public APIRequest providersAvailabilty(Context context, String id, String loc, String date, String insurance, SwipeRefreshLayout swipeRefreshLayout) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<FinderModel>>(context) {
            @Override
            public Call<CollectionTransformer<FinderModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.getProviderAvailability(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new ResultResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.DATE, date)
                .addParameter(Keys.LOCATION, loc)
                .addParameter(Keys.INSURANCE, insurance)
                .addParameter(Keys.OCCUPATION_ID, id)
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .showSwipeRefreshLayout(true);
        return apiRequest;


    }
    public interface RequestService {
        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<OccupationModel>> requestCollectionTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<SingleTransformer<ClinicModel>> requestClinicListTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);
        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<PractitionerModel>> requestPractitionerListTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);
        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<FinderModel>> requesFinderTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<FinderModel>> requestSingleTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

    }


    public class OccupationList extends APIResponse<CollectionTransformer<OccupationModel>> {
        public OccupationList(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
    public class ClinicList extends APIResponse<CollectionTransformer<ClinicsModel>> {
        public ClinicList(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
    public class PractitionerList extends APIResponse<CollectionTransformer<PractitionerModel>> {
        public PractitionerList(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
    public class PractitionersList extends APIResponse<CollectionTransformer<PractitionerModel>> {
        public PractitionersList(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
    public class FinderResultResponse extends APIResponse<CollectionTransformer<FinderModel>> {
        public FinderResultResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
    public class ResultResponse extends APIResponse<CollectionTransformer<FinderModel>> {
        public ResultResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
}
