package com.highlysucceed.kaluapp.server.request;

import android.content.Context;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.highlysucceed.kaluapp.config.Keys;
import com.highlysucceed.kaluapp.config.Url;
import com.highlysucceed.kaluapp.data.model.api.AccridetorModel;
import com.highlysucceed.kaluapp.data.model.api.ClinicDetailsModel;
import com.highlysucceed.kaluapp.data.model.api.ClinicModel;
import com.highlysucceed.kaluapp.data.model.api.DoctorModel;
import com.highlysucceed.kaluapp.data.model.api.FeturedPhotoModel;
import com.highlysucceed.kaluapp.data.model.api.HospitalModel;
import com.highlysucceed.kaluapp.data.model.api.NearbyModel;
import com.highlysucceed.kaluapp.data.model.api.ProviderDetailsModel;
import com.highlysucceed.kaluapp.data.model.api.ProviderModel;
import com.highlysucceed.kaluapp.data.model.api.ScheduleModel;
import com.highlysucceed.kaluapp.data.model.api.ServiceModel;
import com.highlysucceed.kaluapp.data.model.api.SlotModel;
import com.highlysucceed.kaluapp.data.model.api.UserModel;
import com.highlysucceed.kaluapp.data.preference.UserData;
import com.highlysucceed.kaluapp.vendor.server.request.APIRequest;
import com.highlysucceed.kaluapp.vendor.server.request.APIResponse;
import com.highlysucceed.kaluapp.vendor.server.transformer.BaseTransformer;
import com.highlysucceed.kaluapp.vendor.server.transformer.CollectionTransformer;
import com.highlysucceed.kaluapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public class Clinic {

    public static Clinic getDefault() {
        return new Clinic();
    }


    public void getClinicDetails(Context context, String id,  SwipeRefreshLayout swipeRefreshLayout) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<ClinicDetailsModel>>(context) {
            @Override
            public Call<SingleTransformer<ClinicDetailsModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestClinicDetails(Url.clinicList(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new ClinicDetailsReponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.INCLUDE, "provider,coverphoto,featured_photo,clinic")
                .addParameter(Keys.CLINIC_ID, id)
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .showSwipeRefreshLayout(true)
                .execute();

    }
    public void getDoctorsList(Context context, String keyword,  SwipeRefreshLayout swipeRefreshLayout) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<ProviderModel>>(context) {
            @Override
            public Call<CollectionTransformer<ProviderModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestProviderTransformer(Url.getProvidersList(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new DoctorListResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.KEYWORD, keyword)
                .setPerPage(5)
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .showSwipeRefreshLayout(true)
                .execute();

    }

    public APIRequest AllClinic(Context context) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<ClinicModel>>(context) {
            @Override
            public Call<CollectionTransformer<ClinicModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSearch(Url.getClinicList(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new ALlClinicReponse(this));
            }
        };
        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.INCLUDE, "clinic");
      return  apiRequest;

    }
    public APIRequest AllClinicDetails(Context context) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<ClinicModel>>(context) {
            @Override
            public Call<CollectionTransformer<ClinicModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSearch(Url.getClinicList(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new ALlClinicReponse(this));
            }
        };
        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.INCLUDE, "clinic");
        return  apiRequest;

    }



    public APIRequest getProviderDetails(Context context, String id, SwipeRefreshLayout refreshLayout) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<ProviderDetailsModel>>(context) {
            @Override
            public Call<SingleTransformer<ProviderDetailsModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestProvider(Url.getProviderDetails(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new ProviderDetailsResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.INCLUDE, "clinic,services,schedule,clinic_photos")
                .addParameter(Keys.PROVIDER_ID, id)
                .setSwipeRefreshLayout(refreshLayout)
                .showSwipeRefreshLayout(true);
        return apiRequest;


    }
    public void getServiceList(Context context, String id) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<ServiceModel>>(context) {
            @Override
            public Call<CollectionTransformer<ServiceModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestServiceList(Url.getserviceList(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new ServiceResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.PROVIDER_ID, id)
                .execute();

    }
    public APIRequest getHospitalDetails(Context context, String id, SwipeRefreshLayout swipeRefreshLayout) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<HospitalModel>>(context) {
            @Override
            public Call<CollectionTransformer<HospitalModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestHospitalList(Url.getHospitalDetails(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new HosptalDetailsReponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.HOSPITAL_ID, id)
                .addParameter(Keys.INCLUDE, "clinics")
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .showSwipeRefreshLayout(true);
        return apiRequest;

    }
    public void getCliniclist(Context context, String id) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<HospitalModel>>(context) {
            @Override
            public Call<CollectionTransformer<HospitalModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestHospitalList(Url.getHospitalDetails(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new HospitalReponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.HOSPITAL_ID, id)
                .addParameter(Keys.INCLUDE, "clinics")
                .showDefaultProgressDialog("Loading ...")
                .execute();
    }

    public void getFeaturedHospital(Context context, String id) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<FeturedPhotoModel>>(context) {
            @Override
            public Call<CollectionTransformer<FeturedPhotoModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestFeturedModel(Url.getHospitalPhoto(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new FeaturedPhotoResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.HOSPITAL_ID, id)
//                .showDefaultProgressDialog("Loading ...")
                .execute();

    }
    public void getFeaturedPhoto(Context context, String id) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<FeturedPhotoModel>>(context) {
            @Override
            public Call<CollectionTransformer<FeturedPhotoModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestFeturedModel(Url.getHospitalPhoto(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new PhotoResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.HOSPITAL_ID, id)
                .showDefaultProgressDialog("Loading ...")
                .execute();

    }


    public APIRequest  bookAppointment(Context context) {
        APIRequest apiRequest = new APIRequest<BaseTransformer>(context) {
            @Override
            public Call<BaseTransformer> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestBaseTransformer(Url.bookAppointment(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new BookingResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .showDefaultProgressDialog("Booking ...");
        return apiRequest;

    }

    public void  getProviderSchedule(Context context, String id, String date) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<SlotModel>>(context) {
            @Override
            public Call<CollectionTransformer<SlotModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestScheduleTransformer(Url.getProviderSchedule(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new SlotResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.PROVIDER_ID, id)
                .addParameter(Keys.DATE, date)
                .execute();

    }
    public APIRequest  getClinicAccreditation(Context context, String id) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<AccridetorModel>>(context) {
            @Override
            public Call<CollectionTransformer<AccridetorModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestListAccridetor(Url.getAccreditation(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new AccreditorResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.CLINIC_ID, id);
        return apiRequest;

    }



    public interface RequestService {
        @Multipart
        @POST("{p}")
        Call<BaseTransformer> requestBaseTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);
        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<SlotModel>> requestScheduleTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);
        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<HospitalModel>> requestHospitalList(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part java.util.List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<SingleTransformer<ClinicDetailsModel>> requestClinicDetails(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part java.util.List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<ClinicModel>> requestSearch(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part java.util.List<MultipartBody.Part> part);
        @Multipart
        @POST("{p}")
        Call<SingleTransformer<ProviderDetailsModel>> requestProvider(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part java.util.List<MultipartBody.Part> part);
        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<ProviderModel>> requestProviderTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part java.util.List<MultipartBody.Part> part);
        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<ServiceModel>> requestServiceList(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part java.util.List<MultipartBody.Part> part);
        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<FeturedPhotoModel>> requestFeturedModel(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part java.util.List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<AccridetorModel>> requestListAccridetor(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part java.util.List<MultipartBody.Part> part);

    }



    public class ALlClinicReponse extends APIResponse<CollectionTransformer<ClinicModel>> {
        public ALlClinicReponse(APIRequest apiRequest) { super(apiRequest); }
    }
    public class ALlClinicDetailsReponse extends APIResponse<CollectionTransformer<ClinicModel>> {
        public ALlClinicDetailsReponse(APIRequest apiRequest) { super(apiRequest); }
    }
    public class ClinicDetailsReponse extends APIResponse<SingleTransformer<ClinicDetailsModel>> {
        public ClinicDetailsReponse(APIRequest apiRequest) { super(apiRequest); }
    }
    public class DoctorListResponse extends APIResponse<CollectionTransformer<ProviderModel>> {
        public DoctorListResponse(APIRequest apiRequest) { super(apiRequest); }
    }
    public class ProviderDetailsResponse extends APIResponse<SingleTransformer<ProviderDetailsModel>> {
        public ProviderDetailsResponse(APIRequest apiRequest) { super(apiRequest); }
    }
    public class SearchResponse extends APIResponse<CollectionTransformer<ClinicModel>> {
        public SearchResponse(APIRequest apiRequest) { super(apiRequest); }
    }
    public class ServiceResponse extends APIResponse<CollectionTransformer<ServiceModel>> {
        public ServiceResponse(APIRequest apiRequest) { super(apiRequest); }
    }
    public class BookingResponse extends APIResponse<BaseTransformer> {
        public BookingResponse(APIRequest apiRequest) { super(apiRequest); }
    }
    public class HosptalDetailsReponse extends APIResponse<CollectionTransformer<HospitalModel>> {
        public HosptalDetailsReponse(APIRequest apiRequest) { super(apiRequest); }
    }
    public class HospitalReponse extends APIResponse<CollectionTransformer<HospitalModel>> {
        public HospitalReponse(APIRequest apiRequest) { super(apiRequest); }
    }
    public class FeaturedPhotoResponse extends APIResponse<CollectionTransformer<FeturedPhotoModel>> {
        public FeaturedPhotoResponse(APIRequest apiRequest) { super(apiRequest); }
    }
    public class PhotoResponse extends APIResponse<CollectionTransformer<FeturedPhotoModel>> {
        public PhotoResponse(APIRequest apiRequest) { super(apiRequest); }
    }
    public class SlotResponse extends APIResponse<CollectionTransformer<SlotModel>> {
        public SlotResponse(APIRequest apiRequest) { super(apiRequest); }
    }
    public class AccreditorResponse extends APIResponse<CollectionTransformer<AccridetorModel>> {
        public AccreditorResponse(APIRequest apiRequest) { super(apiRequest); }
    }
}
