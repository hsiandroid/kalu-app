package com.highlysucceed.kaluapp.server.request;

import android.content.Context;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.highlysucceed.kaluapp.config.Keys;
import com.highlysucceed.kaluapp.config.Url;
import com.highlysucceed.kaluapp.data.model.api.InfoModel;
import com.highlysucceed.kaluapp.data.model.api.UserModel;
import com.highlysucceed.kaluapp.data.preference.UserData;
import com.highlysucceed.kaluapp.vendor.server.request.APIRequest;
import com.highlysucceed.kaluapp.vendor.server.request.APIResponse;
import com.highlysucceed.kaluapp.vendor.server.transformer.CollectionTransformer;
import com.highlysucceed.kaluapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public class Infectious {
    public static Infectious getDefault(){
        return new Infectious();
    }


    public APIRequest getProfileDetails(Context context, String id,  SwipeRefreshLayout swipeRefreshLayout) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<InfoModel>>(context) {
            @Override
            public Call<CollectionTransformer<InfoModel>> onCreateCall() {
                return getRetrofit().create(Auth.RequestService.class).requestprofileDetails(Url.getProfileDetails(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new LoginResponse(this));
            }
        };
        apiRequest.
                addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.USER_ID, id)
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .showSwipeRefreshLayout(true);
        return apiRequest;
    }

    public interface RequestService {
        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<InfoModel>> requestSingleTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);
    }


        public class LoginResponse extends APIResponse<CollectionTransformer<InfoModel>> {
        public LoginResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

}
