package com.highlysucceed.kaluapp.data.model.api;

import com.highlysucceed.kaluapp.vendor.android.base.AndroidModel;

public class CenterModel extends AndroidModel {

    public int image;
    public String name;
    public String address;

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public CenterModel convertFromJson(String json) {
        return convertFromJson(json, CenterModel.class);
    }

}
