package com.highlysucceed.kaluapp.data.model.api;

import androidx.databinding.BaseObservable;

import com.google.gson.annotations.SerializedName;
import com.highlysucceed.kaluapp.vendor.android.base.AndroidModel;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class CoverPhotoModel extends AndroidModel {


    @SerializedName("clinic_id")
    public int clinicId;
    @SerializedName("hospital_id")
    public Object hospitalId;
    @SerializedName("thumbnail")
    public ThumbnailBean thumbnail;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public CoverPhotoModel convertFromJson(String json) {
        return convertFromJson(json, CoverPhotoModel.class);
    }


    public static class ThumbnailBean extends BaseObservable {
        @SerializedName("path")
        public String path;
        @SerializedName("filename")
        public String filename;
        @SerializedName("directory")
        public String directory;
        @SerializedName("full_path")
        public String fullPath;
        @SerializedName("thumb_path")
        public String thumbPath;
    }
}
