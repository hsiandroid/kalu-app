package com.highlysucceed.kaluapp.data.model.api;

import com.google.gson.annotations.SerializedName;
import com.highlysucceed.kaluapp.vendor.android.base.AndroidModel;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ClinicsModel extends AndroidModel {


    @SerializedName("hospital_id")
    public int hospitalId;
    @SerializedName("name")
    public String name;
    @SerializedName("address")
    public String address;
    @SerializedName("email")
    public String email;
    @SerializedName("contact_number")
    public String contactNumber;
    @SerializedName("clinic_start_time")
    public String clinicStartTime;
    @SerializedName("clinic_end_time")
    public String clinicEndTime;
    @SerializedName("about_us")
    public String aboutUs;
    @SerializedName("date_created")
    public DateCreatedBean dateCreated;
    @SerializedName("last_modified")
    public LastModifiedBean lastModified;
    @SerializedName("thumbnail")
    public ThumbnailBean thumbnail;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public ClinicsModel convertFromJson(String json) {
        return convertFromJson(json, ClinicsModel.class);
    }

    public static class DateCreatedBean  {
        @SerializedName("date_db")
        public String dateDb;
        @SerializedName("month_year")
        public String monthYear;
        @SerializedName("time_passed")
        public String timePassed;
        @SerializedName("timestamp")
        public String timestamp;
    }

    public static class LastModifiedBean {
        @SerializedName("date_db")
        public String dateDb;
        @SerializedName("month_year")
        public String monthYear;
        @SerializedName("time_passed")
        public String timePassed;
        @SerializedName("timestamp")
        public String timestamp;
    }

    public static class ThumbnailBean  {
        @SerializedName("path")
        public String path = " ";
        @SerializedName("filename")
        public String filename = " ";
        @SerializedName("directory")
        public String directory = "";
        @SerializedName("full_path")
        public String fullPath = "";
        @SerializedName("thumb_path")
        public String thumbPath ="";
    }
}
