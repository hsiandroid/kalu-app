package com.highlysucceed.kaluapp.data.model.api;

import com.google.gson.annotations.SerializedName;
import com.highlysucceed.kaluapp.vendor.android.base.AndroidModel;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class UserModel extends AndroidModel {

    @SerializedName("user_id")
    public int userId;

    @SerializedName("fb_id")
    public String fbID;
    @SerializedName("access_token")
    public String accessToken;
    @SerializedName("name")
    public String name;
    @SerializedName("qr_code")
    public String qrCode;
    @SerializedName("firstname")
    public String firstname;
    @SerializedName("lastname")
    public String lastname;
    @SerializedName("email")
    public String email;
    @SerializedName("address")
    public String address;
    @SerializedName("contact_number")
    public String contactNumber;
    @SerializedName("birthdate")
    public String birthdate;
    @SerializedName("blood_type")
    public String bloodType;
    @SerializedName("height")
    public String height;
    @SerializedName("weight")
    public String weight;
    @SerializedName("date_created")
    public DateCreatedBean dateCreated;
    @SerializedName("avatar")
    public AvatarBean avatar;


    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public UserModel convertFromJson(String json) {
        return convertFromJson(json, UserModel.class);
    }


    public static class DateCreatedBean  {
        @SerializedName("date_db")
        public String dateDb;
        @SerializedName("month_year")
        public String monthYear;
        @SerializedName("time_passed")
        public String timePassed;
        @SerializedName("timestamp")
        public String timestamp;
    }

    public static class AvatarBean  {
        @SerializedName("path")
        public String path;
        @SerializedName("filename")
        public String filename;
        @SerializedName("directory")
        public String directory;
        @SerializedName("full_path")
        public String fullPath;
        @SerializedName("thumb_path")
        public String thumbPath;
    }
}
