package com.highlysucceed.kaluapp.data.model.api;

import com.highlysucceed.kaluapp.vendor.android.base.AndroidModel;

public class AccreditorModel extends AndroidModel {

    public int image;
    public String name;


    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public AccreditorModel convertFromJson(String json) {
        return convertFromJson(json, AccreditorModel.class);
    }

}
