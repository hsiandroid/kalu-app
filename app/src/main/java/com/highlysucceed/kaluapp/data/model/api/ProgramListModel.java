package com.highlysucceed.kaluapp.data.model.api;


import com.highlysucceed.kaluapp.vendor.android.base.AndroidModel;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ProgramListModel extends AndroidModel {

    public int id;
    public String name;
    public String address;
    public String email;
    public String number;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public ProgramListModel convertFromJson(String json) {
        return convertFromJson(json, ProgramListModel.class);
    }
}
