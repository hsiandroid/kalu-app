package com.highlysucceed.kaluapp.data.model.api;


import com.highlysucceed.kaluapp.vendor.android.base.AndroidModel;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class DaysModel extends AndroidModel {

    public String dayName;
    public String day;



    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public DaysModel convertFromJson(String json) {
        return convertFromJson(json, DaysModel.class);
    }
}
