package com.highlysucceed.kaluapp.data.model.api;

import com.google.gson.annotations.SerializedName;
import com.highlysucceed.kaluapp.vendor.android.base.AndroidModel;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ScheduleModel extends AndroidModel {


    @SerializedName("day")
    public String day;
    @SerializedName("start_time")
    public String startTime;
    @SerializedName("end_time")
    public String endTime;
    @SerializedName("clinic_name")
    public String clinicName;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public ScheduleModel convertFromJson(String json) {
        return convertFromJson(json, ScheduleModel.class);
    }
}
