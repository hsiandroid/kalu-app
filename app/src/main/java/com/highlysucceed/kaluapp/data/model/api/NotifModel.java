package com.highlysucceed.kaluapp.data.model.api;


import com.highlysucceed.kaluapp.vendor.android.base.AndroidModel;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class NotifModel extends AndroidModel {

    public int id;
    public String title;
    public String description;


    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public NotifModel convertFromJson(String json) {
        return convertFromJson(json, NotifModel.class);
    }
}
