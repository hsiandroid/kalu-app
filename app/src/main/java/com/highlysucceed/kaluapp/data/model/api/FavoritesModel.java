package com.highlysucceed.kaluapp.data.model.api;


import com.highlysucceed.kaluapp.vendor.android.base.AndroidModel;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class FavoritesModel extends AndroidModel {

    public int id;
    public String avatar;
    public String name;


    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public FavoritesModel convertFromJson(String json) {
        return convertFromJson(json, FavoritesModel.class);
    }
}
