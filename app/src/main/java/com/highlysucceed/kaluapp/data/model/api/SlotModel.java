package com.highlysucceed.kaluapp.data.model.api;

import com.google.gson.annotations.SerializedName;
import com.highlysucceed.kaluapp.vendor.android.base.AndroidModel;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class SlotModel extends AndroidModel {


    @SerializedName("time")
    public String time;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public SlotModel convertFromJson(String json) {
        return convertFromJson(json, SlotModel.class);
    }
}
