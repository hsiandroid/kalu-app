package com.highlysucceed.kaluapp.data.model.api;

import com.google.gson.annotations.SerializedName;
import com.highlysucceed.kaluapp.vendor.android.base.AndroidModel;

import java.util.List;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ErrorModel extends AndroidModel {


    @SerializedName("email")
    public List<String> email;
    @SerializedName("password")
    public List<String> password;
    @SerializedName("firstname")
    public List<String> firstname;
    @SerializedName("lastname")
    public List<String> lastname;
    @SerializedName("address")
    public List<String> address;
    @SerializedName("contact_number")
    public List<String> contactNumber;
    @SerializedName("birthdate")
    public List<String> birthdate;
    @SerializedName("blood_type")
    public List<String> bloodType;
    @SerializedName("height")
    public List<String> height;
    @SerializedName("weight")
    public List<String> weight;
    @SerializedName("date")
    public List<String> date;


    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public ErrorModel convertFromJson(String json) {
        return convertFromJson(json, ErrorModel.class);
    }
}
