package com.highlysucceed.kaluapp.data.model.api;

import com.google.gson.annotations.SerializedName;
import com.highlysucceed.kaluapp.vendor.android.base.AndroidModel;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class InfoModel extends AndroidModel {


    @SerializedName("name")
    public String name;
    @SerializedName("prefix")
    public String prefix;
    @SerializedName("fname")
    public String fname;
    @SerializedName("lname")
    public String lname;
    @SerializedName("mname")
    public String mname;
    @SerializedName("gender")
    public String gender;
    @SerializedName("age")
    public String age;
    @SerializedName("suffix")
    public String suffix;
    @SerializedName("PERSONAL_INFORMATION")
    public PERSONALINFORMATIONBean PERSONALINFORMATION;
    @SerializedName("CONCISE_CLINICAL_HISTORY")
    public CONCISECLINICALHISTORYBean CONCISECLINICALHISTORY;
    @SerializedName("SIGNS_AND_SYMPTOMS")
    public SIGNSANDSYMPTOMSBean SIGNSANDSYMPTOMS;
    @SerializedName("ANTIGEN_TEST_RESULT")
    public ANTIGENTESTRESULTBean ANTIGENTESTRESULT;
    @SerializedName("CREDENTIALS")
    public CREDENTIALSBean CREDENTIALS;
    @SerializedName("date_created")
    public DateCreatedBean dateCreated;
    @SerializedName("last_modified")
    public LastModifiedBean lastModified;
    @SerializedName("profile_picture")
    public ProfilePictureBean profilePicture;
    @SerializedName("cover_photo")
    public CoverPhotoBean coverPhoto;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public InfoModel convertFromJson(String json) {
        return convertFromJson(json, InfoModel.class);
    }


    public static class PERSONALINFORMATIONBean{
        @SerializedName("phone_number")
        public String phoneNumber;
        @SerializedName("email")
        public String email;
        @SerializedName("address")
        public String address;
        @SerializedName("birthdate")
        public String birthdate;
        @SerializedName("age")
        public String age;
        @SerializedName("gender")
        public String gender;
        @SerializedName("contact_number")
        public String contactNumber;
        @SerializedName("status")
        public String status;
        @SerializedName("type")
        public String type;
        @SerializedName("blood_type")
        public String bloodType;
        @SerializedName("height")
        public String height;
        @SerializedName("weight")
        public String weight;
        @SerializedName("systolic")
        public String systolic;
        @SerializedName("diastolic")
        public String diastolic;
        @SerializedName("celsius")
        public String celsius;
    }

    public static class CONCISECLINICALHISTORYBean {
        @SerializedName("travel_history")
        public String travelHistory;
        @SerializedName("exposure_history")
        public String exposureHistory;
        @SerializedName("initial_onset_of_symptoms")
        public String initialOnsetOfSymptoms;
        @SerializedName("date_of_resolution_of_symptoms")
        public String dateOfResolutionOfSymptoms;
    }

    public static class SIGNSANDSYMPTOMSBean {
        @SerializedName("dry_cough")
        public String dryCough;
        @SerializedName("fever")
        public String fever;
        @SerializedName("muscle_pain")
        public String musclePain;
        @SerializedName("weakness")
        public String weakness;
        @SerializedName("decreased_sense_of_smell")
        public String decreasedSenseOfSmell;
        @SerializedName("decreased_sense_of_taste")
        public String decreasedSenseOfTaste;
        @SerializedName("diarrhea")
        public String diarrhea;
        @SerializedName("difficulty_of_breathing")
        public String difficultyOfBreathing;
        @SerializedName("temperature")
        public String temperature;
        @SerializedName("note_observations")
        public String noteObservations;
    }

    public static class ANTIGENTESTRESULTBean {
        @SerializedName("date_of_test_done")
        public String dateOfTestDone;
        @SerializedName("negative")
        public String negative;
        @SerializedName("positive")
        public String positive;
        @SerializedName("pcr_done")
        public String pcrDone;
    }

    public static class CREDENTIALSBean {
        @SerializedName("pcr_not_done")
        public String pcrNotDone;
        @SerializedName("date")
        public String date;
        @SerializedName("negative")
        public String negative;
        @SerializedName("positive")
        public String positive;
        @SerializedName("pending_result")
        public String pendingResult;
        @SerializedName("name_MD")
        public String nameMD;
        @SerializedName("name_EE")
        public String nameEE;
    }

    public static class DateCreatedBean {
        @SerializedName("date_db")
        public String dateDb;
        @SerializedName("month_year")
        public String monthYear;
        @SerializedName("time_passed")
        public String timePassed;
        @SerializedName("timestamp")
        public String timestamp;
    }

    public static class LastModifiedBean  {
        @SerializedName("date_db")
        public String dateDb;
        @SerializedName("month_year")
        public String monthYear;
        @SerializedName("time_passed")
        public String timePassed;
        @SerializedName("timestamp")
        public String timestamp;
    }

    public static class ProfilePictureBean {
        @SerializedName("path")
        public String path;
        @SerializedName("filename")
        public String filename;
        @SerializedName("directory")
        public String directory;
        @SerializedName("full_path")
        public String fullPath;
        @SerializedName("thumb_path")
        public String thumbPath;
    }

    public static class CoverPhotoBean {
        @SerializedName("path")
        public String path;
        @SerializedName("filename")
        public String filename;
        @SerializedName("directory")
        public String directory;
        @SerializedName("full_path")
        public String fullPath;
        @SerializedName("thumb_path")
        public String thumbPath;
    }
}
