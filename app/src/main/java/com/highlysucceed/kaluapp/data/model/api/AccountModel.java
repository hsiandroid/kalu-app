package com.highlysucceed.kaluapp.data.model.api;

import com.google.gson.annotations.SerializedName;
import com.highlysucceed.kaluapp.vendor.android.base.AndroidModel;

import java.io.Serializable;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class AccountModel extends AndroidModel {

    @SerializedName("email")
    public String email;
    @SerializedName("password")
    public String password;
    @SerializedName("confirm_password")
    public String confirm_password;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public AccountModel convertFromJson(String json) {
        return convertFromJson(json, AccountModel.class);
    }


}
