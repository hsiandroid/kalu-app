package com.highlysucceed.kaluapp.data.model.api;

import com.google.gson.annotations.SerializedName;
import com.highlysucceed.kaluapp.vendor.android.base.AndroidModel;

public class DoctorModel extends AndroidModel {


    @SerializedName("prefix")
    public String prefix;
    @SerializedName("expertise")
    public String expertise;
    @SerializedName("name")
    public String name;
    @SerializedName("suffix")
    public String suffix;
    @SerializedName("username")
    public String username;
    @SerializedName("email")
    public String email;
    @SerializedName("contact_number")
    public String contactNumber;
    @SerializedName("birthdate")
    public String birthdate;
    @SerializedName("gender")
    public String gender;
    @SerializedName("civil_status")
    public String civilStatus;
    @SerializedName("about")
    public String about;
    @SerializedName("address")
    public String address;
    @SerializedName("date_created")
    public DateCreatedBean dateCreated;
    @SerializedName("last_modified")
    public LastModifiedBean lastModified;
    @SerializedName("profile_picture")
    public ProfilePictureBean profilePicture;
    @SerializedName("cover_photo")
    public CoverPhotoBean coverPhoto;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public DoctorModel convertFromJson(String json) {
        return convertFromJson(json, DoctorModel.class);
    }


    public static class DateCreatedBean {
        @SerializedName("date_db")
        public String dateDb;
        @SerializedName("month_year")
        public String monthYear;
        @SerializedName("time_passed")
        public String timePassed;
        @SerializedName("timestamp")
        public String timestamp;
    }

    public static class LastModifiedBean {
        @SerializedName("date_db")
        public String dateDb;
        @SerializedName("month_year")
        public String monthYear;
        @SerializedName("time_passed")
        public String timePassed;
        @SerializedName("timestamp")
        public String timestamp;
    }

    public static class ProfilePictureBean  {
        @SerializedName("path")
        public String path;
        @SerializedName("filename")
        public String filename;
        @SerializedName("directory")
        public String directory;
        @SerializedName("full_path")
        public String fullPath;
        @SerializedName("thumb_path")
        public String thumbPath;
    }

    public static class CoverPhotoBean {
        @SerializedName("path")
        public String path;
        @SerializedName("filename")
        public String filename;
        @SerializedName("directory")
        public String directory;
        @SerializedName("full_path")
        public String fullPath;
        @SerializedName("thumb_path")
        public String thumbPath;
    }
}
