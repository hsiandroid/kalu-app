package com.highlysucceed.kaluapp.data.model.api;

import androidx.databinding.BaseObservable;

import com.google.gson.annotations.SerializedName;
import com.highlysucceed.kaluapp.vendor.android.base.AndroidModel;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ProviderModel extends AndroidModel {


    @SerializedName("user_id")
    public int userId;
    @SerializedName("prefix")
    public String prefix;
    @SerializedName("name")
    public String name;
    @SerializedName("suffix")
    public String suffix;
    @SerializedName("expertise")
    public String expertise;
    @SerializedName("username")
    public String username;
    @SerializedName("email")
    public String email;
    @SerializedName("contact_number")
    public String contactNumber;
    @SerializedName("birthdate")
    public String birthdate;
    @SerializedName("gender")
    public String gender;
    @SerializedName("civil_status")
    public String civilStatus;
    @SerializedName("about")
    public String about;
    @SerializedName("address")
    public String address;
    @SerializedName("date_created")
    public DateCreatedBean dateCreated;
    @SerializedName("last_modified")
    public LastModifiedBean lastModified;
    @SerializedName("profile_picture")
    public ProfilePictureBean profilePicture;
    @SerializedName("cover_photo")
    public CoverPhotoBean coverPhoto;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public ProviderModel convertFromJson(String json) {
        return convertFromJson(json, ProviderModel.class);
    }


    public static class DateCreatedBean extends BaseObservable {
        @SerializedName("date_db")
        public String dateDb;
        @SerializedName("month_year")
        public String monthYear;
        @SerializedName("time_passed")
        public String timePassed;
        @SerializedName("timestamp")
        public String timestamp;
    }

    public static class LastModifiedBean extends BaseObservable {
        @SerializedName("date_db")
        public String dateDb;
        @SerializedName("month_year")
        public String monthYear;
        @SerializedName("time_passed")
        public String timePassed;
        @SerializedName("timestamp")
        public String timestamp;
    }

    public static class ProfilePictureBean extends BaseObservable {
        @SerializedName("path")
        public String path;
        @SerializedName("filename")
        public String filename;
        @SerializedName("directory")
        public String directory;
        @SerializedName("full_path")
        public String fullPath;
        @SerializedName("thumb_path")
        public String thumbPath;
    }

    public static class CoverPhotoBean extends BaseObservable {
        @SerializedName("path")
        public String path;
        @SerializedName("filename")
        public String filename;
        @SerializedName("directory")
        public String directory;
        @SerializedName("full_path")
        public String fullPath;
        @SerializedName("thumb_path")
        public String thumbPath;
    }
}
