package com.highlysucceed.kaluapp.data.model.api;

import com.google.gson.annotations.SerializedName;
import com.highlysucceed.kaluapp.vendor.android.base.AndroidModel;

import java.util.List;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class NearbyModel extends AndroidModel {

    @SerializedName("nearby_clinics")
    public List<NearbyClinicModel> nearbyClinics;
    @SerializedName("nearby_hospitals")
    public List<NeabyHospitalModel> nearbyHospitals;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public NearbyModel convertFromJson(String json) {
        return convertFromJson(json, NearbyModel.class);
    }

}
