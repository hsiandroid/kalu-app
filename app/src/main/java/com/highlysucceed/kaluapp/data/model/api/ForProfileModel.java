package com.highlysucceed.kaluapp.data.model.api;

import com.google.gson.annotations.SerializedName;
import com.highlysucceed.kaluapp.vendor.android.base.AndroidModel;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ForProfileModel extends AndroidModel {

    @SerializedName("name")
    public int image;
    public String name;
    public String info;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public ForProfileModel convertFromJson(String json) {
        return convertFromJson(json, ForProfileModel.class);
    }
}
