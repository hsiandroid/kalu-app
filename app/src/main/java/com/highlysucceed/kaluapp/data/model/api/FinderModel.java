package com.highlysucceed.kaluapp.data.model.api;

import com.google.gson.annotations.SerializedName;
import com.highlysucceed.kaluapp.vendor.android.base.AndroidModel;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class FinderModel extends AndroidModel {


    @SerializedName("start_time")
    public String startTime;
    @SerializedName("end_time")
    public String endTime;
    @SerializedName("provider_id")
    public int providerId;
    @SerializedName("provider_name")
    public String providerName;
    @SerializedName("Clinic")
    public String Clinic;
    @SerializedName("Occupation")
    public String Occupation;
    @SerializedName("clinic_id")
    public int clinicId;
    @SerializedName("profile_picture_full_path")
    public String profilePictureFullPath;
    @SerializedName("profile_picture_thumb_path")
    public String profilePictureThumbPath;
    @SerializedName("cover_photo_full_path")
    public Object coverPhotoFullPath;
    @SerializedName("cover_photo_thumb_path")
    public Object coverPhotoThumbPath;


    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public FinderModel convertFromJson(String json) {
        return convertFromJson(json, FinderModel.class);
    }
}
