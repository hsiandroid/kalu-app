package com.highlysucceed.kaluapp.data.model.api;


import com.google.gson.annotations.SerializedName;
import com.highlysucceed.kaluapp.vendor.android.base.AndroidModel;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ServiceModel extends AndroidModel {


    @SerializedName("name")
    public String name;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public ServiceModel convertFromJson(String json) {
        return convertFromJson(json, ServiceModel.class);
    }
}
