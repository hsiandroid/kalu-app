package com.highlysucceed.kaluapp.data.model.api;

import com.google.gson.annotations.SerializedName;
import com.highlysucceed.kaluapp.vendor.android.base.AndroidModel;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class DataModel extends AndroidModel {

    public String Id;
    public String loc;
    public String date;
    public String insurance;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public DataModel convertFromJson(String json) {
        return convertFromJson(json, DataModel.class);
    }
}
