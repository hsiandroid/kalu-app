package com.highlysucceed.kaluapp.data.model.api;

import com.google.gson.annotations.SerializedName;
import com.highlysucceed.kaluapp.vendor.android.base.AndroidModel;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class SignUpModel extends AndroidModel {

    @SerializedName("firstname")
    public String firstname;
    @SerializedName("lastname")
    public String lastname;
    @SerializedName("email")
    public String email;
    @SerializedName("password")
    public String password;
    @SerializedName("confirm_password")
    public String confirm_password;
    @SerializedName("address")
    public String address;
    @SerializedName("contact_number")
    public String contactNumber;
    @SerializedName("gender")
    public String gender;
    @SerializedName("birthdate")
    public String birthdate;
    @SerializedName("blood_type")
    public String bloodType;
    @SerializedName("height")
    public String height;
    @SerializedName("weight")
    public String weight;


    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public SignUpModel convertFromJson(String json) {
        return convertFromJson(json, SignUpModel.class);
    }
}
