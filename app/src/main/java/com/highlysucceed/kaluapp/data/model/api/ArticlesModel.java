package com.highlysucceed.kaluapp.data.model.api;


import com.highlysucceed.kaluapp.vendor.android.base.AndroidModel;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ArticlesModel extends AndroidModel {

    public int id;
    public String title;
    public int like;
    public int share;


    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public ArticlesModel convertFromJson(String json) {
        return convertFromJson(json, ArticlesModel.class);
    }
}
