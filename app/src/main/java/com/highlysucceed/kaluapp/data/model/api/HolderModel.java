package com.highlysucceed.kaluapp.data.model.api;

import com.google.gson.annotations.SerializedName;
import com.highlysucceed.kaluapp.vendor.android.base.AndroidModel;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class HolderModel extends AndroidModel {

    public String id;
    public String name;
    public String lat;
    public String lng;
    public String type;
    public String address;
    public String contact_number;
    public String full_path;


    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public HolderModel convertFromJson(String json) {
        return convertFromJson(json, HolderModel.class);
    }
}
