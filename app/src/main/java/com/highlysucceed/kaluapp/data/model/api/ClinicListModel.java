package com.highlysucceed.kaluapp.data.model.api;

import com.google.gson.annotations.SerializedName;
import com.highlysucceed.kaluapp.vendor.android.base.AndroidModel;

import java.util.List;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ClinicListModel extends AndroidModel {


    @SerializedName("hospital_id")
    public int hospitalId;
    @SerializedName("name")
    public String name;
    @SerializedName("address")
    public String address;
    @SerializedName("email")
    public String email;
    @SerializedName("contact_number")
    public String contactNumber;
    @SerializedName("clinic_start_time")
    public String clinicStartTime;
    @SerializedName("clinic_end_time")
    public String clinicEndTime;
    @SerializedName("about_us")
    public String aboutUs;
    @SerializedName("date_created")
    public DateCreatedBean dateCreated;
    @SerializedName("last_modified")
    public LastModifiedBean lastModified;
    @SerializedName("thumbnail")
    public ThumbnailBean thumbnail;
    @SerializedName("provider")
    public List<ProviderModel> provider;
    @SerializedName("coverphoto")
    public List<CoverPhotoModel> coverphotoList;
    @SerializedName("featured_photo")
    public FeaturedPhotoBean featuredPhoto;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public ClinicListModel convertFromJson(String json) {
        return convertFromJson(json, ClinicListModel.class);
    }


    public static class DateCreatedBean {
        @SerializedName("date_db")
        public String dateDb;
        @SerializedName("month_year")
        public String monthYear;
        @SerializedName("time_passed")
        public String timePassed;
        @SerializedName("timestamp")
        public String timestamp;
    }

    public static class LastModifiedBean  {
        @SerializedName("date_db")
        public String dateDb;
        @SerializedName("month_year")
        public String monthYear;
        @SerializedName("time_passed")
        public String timePassed;
        @SerializedName("timestamp")
        public String timestamp;
    }

    public static class ThumbnailBean{
        @SerializedName("path")
        public String path;
        @SerializedName("filename")
        public String filename;
        @SerializedName("directory")
        public String directory;
        @SerializedName("full_path")
        public String fullPath;
        @SerializedName("thumb_path")
        public String thumbPath;
    }

    public static class ProviderBean {
        @SerializedName("data")
        public List<DataBean> data;

        public static class DataBean {
            @SerializedName("id")
            public int idX;
            @SerializedName("prefix")
            public String prefix;
            @SerializedName("name")
            public String name;
            @SerializedName("suffix")
            public String suffix;
            @SerializedName("username")
            public String username;
            @SerializedName("email")
            public String email;
            @SerializedName("contact_number")
            public String contactNumber;
            @SerializedName("birthdate")
            public String birthdate;
            @SerializedName("gender")
            public String gender;
            @SerializedName("civil_status")
            public String civilStatus;
            @SerializedName("about")
            public String about;
            @SerializedName("address")
            public String address;
            @SerializedName("date_created")
            public DateCreatedBeanX dateCreated;
            @SerializedName("last_modified")
            public LastModifiedBeanX lastModified;
            @SerializedName("thumbnail")
            public ThumbnailBeanX thumbnail;

            public static class DateCreatedBeanX{
                @SerializedName("date_db")
                public String dateDb;
                @SerializedName("month_year")
                public String monthYear;
                @SerializedName("time_passed")
                public String timePassed;
                @SerializedName("timestamp")
                public String timestamp;
            }

            public static class LastModifiedBeanX {
                @SerializedName("date_db")
                public String dateDb;
                @SerializedName("month_year")
                public String monthYear;
                @SerializedName("time_passed")
                public String timePassed;
                @SerializedName("timestamp")
                public String timestamp;
            }

            public static class ThumbnailBeanX {
                @SerializedName("path")
                public String path;
                @SerializedName("filename")
                public String filename;
                @SerializedName("directory")
                public String directory;
                @SerializedName("full_path")
                public String fullPath;
                @SerializedName("thumb_path")
                public String thumbPath;
            }
        }
    }

    public static class CoverphotoBean  {
        @SerializedName("data")
        public List<DataBeanX> data;

        public static class DataBeanX  {
            @SerializedName("id")
            public int idX;
            @SerializedName("clinic_id")
            public int clinicId;
            @SerializedName("hospital_id")
            public Object hospitalId;
            @SerializedName("thumbnail")
            public ThumbnailBeanXX thumbnail;

            public static class ThumbnailBeanXX {
                @SerializedName("path")
                public String path;
                @SerializedName("filename")
                public String filename;
                @SerializedName("directory")
                public String directory;
                @SerializedName("full_path")
                public String fullPath;
                @SerializedName("thumb_path")
                public String thumbPath;
            }
        }
    }

    public static class FeaturedPhotoBean  {
        @SerializedName("data")
        public List<DataBeanXX> data;

        public static class DataBeanXX  {
            @SerializedName("id")
            public int idX;
            @SerializedName("clinic_id")
            public int clinicId;
            @SerializedName("hospital_id")
            public Object hospitalId;
            @SerializedName("thumbnail")
            public ThumbnailBeanXXX thumbnail;

            public static class ThumbnailBeanXXX  {
                @SerializedName("path")
                public String path;
                @SerializedName("filename")
                public String filename;
                @SerializedName("directory")
                public String directory;
                @SerializedName("full_path")
                public String fullPath;
                @SerializedName("thumb_path")
                public String thumbPath;
            }
        }
    }
}
