package com.highlysucceed.kaluapp.config;

import com.highlysucceed.kaluapp.vendor.android.java.security.SecurityLayer;

/**
 * Created by Labyalo on 8/3/2017.
 */

public class Url extends SecurityLayer {
    public static final String PRODUCTION_URL = decrypt("http://internal.kalu.highlysucceed.com/");
    public static final String DEBUG_URL = decrypt("http://internal.kalu.highlysucceed.com/");

    public static final String APP = App.production ? PRODUCTION_URL : DEBUG_URL;



    public static final String getRefreshToken() {return "/api/auth/refresh-token.json";}
    public static final String checkLogin() {return "/api/auth/check-login.json";}
    public static final String getLogin() {return "/api/auth/login.json";}
    public static final String getLogout() {return "/api/auth/logout.json";}
    public static final String getRegister() {return "/api/auth/register.json";}
    public static final String preRegister() {return "/api/auth/pre-register.json";}
    public static final String getFBLogin() {return "/api/auth/facebook.json";}


    public static final String editProfile() {return "/api/profile/edit-profile.json";}
    public static final String updateAvatar() {return "/api/profile/edit-avatar.json";}
    public static final String updatePassword() {return "/api/profile/edit-password.json";}
    public static final String getProfileDetails() {return "/api/profile/show.json";}


    public static final String clinicList() {return "/api/clinic/show.json";}
    public static final String getClinicList() {return "/api/clinic/show-all.json";}
    public static final String getProvidersList() {return "/api/provider/all.json";}

    public static final String getOccupationList() {return "/api/occupation/all.json";}


    public static final String getProviderDetails() {return "/api/provider/show.json";}
    public static final String bookAppointment() {return "/api/appointment/create.json";}
    public static final String getserviceList() {return "/api/provider/fetch-service.json";}
    public static final String getHospitalDetails() {return "/api/hospital/fetch-hospital-by-id.json";}
    public static final String getHospitalPhoto() {return "/api/hospital/fetch-featured-photos-by-hospital-id.json";}

    public static final String getProviderList() {return "/api/provider/fetch_provider_by_occupation.json";}
    public static final String getProviderSchedule() {return "/api/provider/schedule-slot.json";}
    public static final String getProviderAvailability() {return "/api/appointment/search_availability.json";}
    public static final String getAccreditation() {return "/api/clinic/fetch-accreditations.json";}
}
