package com.highlysucceed.kaluapp.config;

import com.google.gson.annotations.SerializedName;

import java.io.File;

/**
 * Created by Labyalo on 8/7/2017.
 */

public class Keys {
    public static final String DEVICE_ID = "device_id";
    public static final String DEVICE_PLATFORM = "device_name";
    public static final String DEVICE_PLATFORM_VERSION = "device_platform_version";
    public static final String DEVICE_BRAND = "device_brand";
    public static final String DEVICE_BRAND_MODEL = "device_brand_model";
    public static final String DEVICE_REG_ID = "device_reg_id";
    public static final String ACCESS_TOKEN = "access_token";
    public static final String PAGE = "page";
    public static final String PER_PAGE = "per_page";
    public static final String USER_ID = "user_id";
    public static final String USERNAME = "username";
    public static final String FIRSTNAME = "firstname";
    public static final String LASTNAME ="lastname";
    public static final String CURRENT_PASSWORD = "current_password";
    public static final String PASSWORD = "password";
    public static final String CONFIRM_PASSWORD = "password_confirmation";
    public static final String EMAIL = "email";
    public static final String GENDER = "gender";
    public static final String ADDRESS = "address";
    public static final String CONTACT_NUMBER = "contact_number";
    public static final String BIRTHDATE = "birthdate";
    public static final String BLOOD_TYPE = "blood_type";
    public static final String HEIGHT = "height";
    public static final String WEIGHT = "weight";
    public static final String ALLERGIES = "allergies";
    public static final String MEDICAL_HISTORY = "medical_history";
    public static final String MEDICARD_NUMBER = "medicard_number";
    public static final String MEDICARD_REFERENCE = "medicard_reference";
    public static final String MEDICARD_EXPIRY = "medicard_expiry";
    public static final String CURRENT_MEDICATION = "current_medication";
    public static final String CLINIC_ID = "clinic_id";
    public static final String INCLUDE = "include";
    public static final String PROGRESS = "progress";
    public static final String  KEYWORD = "keyword";
    public static final String  PROVIDER_ID = "provider_id";
    public static final String  SERVICE_ID = "service_id";
    public static final String  DATE = "date";
    public static final String  START_TIME = "start_time";
    public static final String  APPOINTMENT_TYPE = "appointment_type";
    public static final String  HOSPITAL_ID = "hospital_id";
    public static final String  OCCUPATION_ID = "occupation_id";
    public static final String  LAT = "lat";
    public static final String  LONG = "long";
    public static final String  LOCATION = "location";
    public static final String  INSURANCE = "insurance";
    public static final String  FILE = "file";
    public static final String  NOTE = "note";

}
